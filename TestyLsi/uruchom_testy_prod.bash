#!/bin/bash
# Id czynnego naboru do testow
NABOR=43
# Id wniosku z czynnego naboru do testow
WNIOSEK=7597
# Lokalizacja testow
TESTY_PATH='/home/marcin_wojciechowski/Documents/TestyLsi'
uruchom_testy_prod(){
# uruchom testy na prod
        echo -e "\e[4;32m Uruchomienie testów produkcji na serwerze $SERWER przeglądaka Firefox \e[0m"
        PATH="$PATH:$TESTY_PATH"      # dodaje do PATH sciezke do webdriverow
        pybot -v IdWniosku:$WNIOSEK -v IdNaboru:$NABOR -v BROWSER:firefox -v BrowserMode:Grid -v HOMEPAGE:$SERWER -v DaneBazy:"database='lsi1420', user='$uzytk', password='$haslo', host='srv-lsi1420-psql.parp.gov.pl', port=5432" -d $TESTY_PATH/TestResultProd --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania --removekeywords name:Resource.UtworzZmiennaDaneBazyZYml $TESTY_PATH/GitLab
        echo -e "\e[4;32m Zakończono testy $INSTANCJA na przeglądarce $BROWSER \e[0m"
}

echo -e "\e[4;31m Zamierzasz uruchomić testy produkcji na serwerze $SERWER przeglądaka Firefox \e[0m"
read -r -p "Nazwa użytkownika posiadającego prawa odczytu na bazie produkcyjnej: " uzytk
read -s -p "Hasło: " haslo
echo 
echo -e "Wybierz serwer do testów: 1 - www1; 2 - www2; 3 - www3"
read -r -p "Twój wybór? [1/2/3] " response
case $response in
    1)
        SERWER='http://srv-lsi1420-www1.parp.gov.pl'
        ;;
    2)
        SERWER='http://srv-lsi1420-www2.parp.gov.pl'
        ;;
    3)
        SERWER='http://srv-lsi1420-www3.parp.gov.pl'
        ;;
    *)
        exit 0
        ;;
esac
case $response in
    1|2|3)
        uruchom_testy_prod
        ;;
    *)
        exit 0
        ;;
esac