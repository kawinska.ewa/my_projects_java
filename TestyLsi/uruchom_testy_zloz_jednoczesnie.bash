#!/bin/bash
# Instancja do testowania
INSTANCJA='https://lsi1420.test.parp.gov.pl'
# Lokalizacja testow
TESTY_PATH='/srv/www/lsi1420-test/robot/TestyLsi'
# Lokalizacja parameters.yml
PARAM_PATH='app/config/parameters.yml' # wykorzystać $PWD
timestamp() {
        date +"%Y-%m-%d %T" -d "+3 minutes" # data + 3 min
}
uruchom_testy() {
# uruchom testy
        echo -e "\e[4;32m Uruchomienie testów $INSTANCJA na przeglądarce $BROWSER \e[0m"
        PATH="$PATH:$TESTY_PATH"      # dodaje do PATH sciezke do webdriverow
	for WNIOSEK_ZLOZ in {1873..1882}
	do
        	pybot --console none -t ZlozWniosek -v IdWnioskuDoZlozenia:$WNIOSEK_ZLOZ -v DataDoZlozeniaWniosku:$data_do_zlozenia -E space:_ -v BROWSER:$BROWSER -v BrowserMode:Grid -v HOMEPAGE:$INSTANCJA -v ParametersSciezka:$PARAM_PATH -d /srv/www/lsi1420-test/web/robot/Test --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania --removekeywords name:Resource.UtworzZmiennaDaneBazyZYml $TESTY_PATH/GitLab &
	done
#	WNIOSEK_ZLOZ=1764
#	pybot -t ZlozWniosek -v IdWnioskuDoZlozenia:$WNIOSEK_ZLOZ -v DataDoZlozeniaWniosku:$data_do_zlozenia -E space:_ -v BROWSER:$BROWSER -v BrowserMode:Grid -v HOMEPAGE:$INSTANCJA -v ParametersSciezka:$PARAM_PATH -d /srv/www/lsi1420-test/web/robot/Test --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania --removekeywords name:Resource.UtworzZmiennaDaneBazyZYml $TESTY_PATH/GitLab &
        echo -e "\e[4;32m Testy są wykonywane w tle i zakończą się ok. $(date +"%Y-%m-%d_%T" -d "+4 minutes") \e[0m"
}

uruchom_testy_firefox(){
# uruchom testy na firefox
        BROWSER='firefox'
}

uruchom_testy_chrome(){
# uruchom testy na chrome
        BROWSER='chrome'
}

uruchom_testy_ie(){
# uruchom testy na ie
        BROWSER='ie'
}

echo -e "\e[4;31m Zamierzasz uruchomić testy na $INSTANCJA \e[0m"
echo -e "Wybierz przeglądarkę: 1 - Firefox; 2 - Chrome; 3 - Internet Explorer"
read -r -p "Twój wybór? [1/2/3] " response
case $response in
    1)
        uruchom_testy_firefox
        ;;
    2)
        uruchom_testy_chrome
        ;;
    3)
        uruchom_testy_ie
        ;;
    *)
        exit 0
        ;;
esac
case $response in
    1|2|3)
        data_do_zlozenia=$(date +"%Y-%m-%d_%T" -d "+3 minutes") # data + 3 min
        echo -e "\e[4;32m Wnioski zostaną złożone: $data_do_zlozenia \e[0m"
        uruchom_testy
        ;;
    *)
        exit 0
        ;;
esac
