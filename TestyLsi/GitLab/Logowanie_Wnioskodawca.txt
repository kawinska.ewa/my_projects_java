*** Settings ***
Suite Teardown    Close Browser
Force Tags        test    lab    prod
Library           Selenium2Library
Resource          ../Library/LogowanieUI.txt    # Logowanie elementy interface
Resource          ../Library/Dane_do_logowania.txt    # Dane do poprawnego logowania
Resource          ../Library/Resource.txt    # User keywords

*** Variables ***

*** Test Cases ***
Logowanie Poprawne
    [Documentation]    *Zaloguj i sprawdz rezultat*
    ...    Krok 1: Otwórz przegladarkę
    ...    Krok 2: Czekaj, aż przycisk Zaloguj będzie widoczny
    ...    Krok 3: Wpisz Nazwę użytkownika
    ...    Krok 4: Wpisz Hasło
    ...    Krok 5: Kliknij przycisk Zaloguj
    ...    Krok 6: Czekaj, aż będzie widoczne menu Wnioski
    [Tags]    smoke
    Otworz Przegladarke na Stronie Logowania
    Zaloguj    ${Login}    ${Password}
    Logowanie Powiodlo Sie

Wylogowanie
    [Documentation]    *Wyloguj* Krok 1: Sprawdź, czy widoczne jest menu Więcej opcji
    ...    Krok 2: Kliknij menu Więcej opcji
    ...    Krok 3: Sprawdź, czy widoczny jest link Wyloguj
    ...    Krok 4: Kliknij menu Wyloguj
    ...    Krok 5: Na stronie powinien być przycisk Zaloguj
    ...
    ...    Testowanie tego przypadku wymaga wcześniejszego *Zalogowania*
    [Tags]    smoke
    Wyloguj
