#!/bin/bash
# Instancja do testowania
INSTANCJA='https://lsi1420.test.parp.gov.pl'
# Id czynnego naboru do testow
NABOR=71
# Id wniosku z czynnego naboru do testow
WNIOSEK=1561
# Id wniosku z czynnego naboru do złożenia
WNIOSEK_ZLOZ=1764
# Lokalizacja testow
TESTY_PATH='/home/marcin_wojciechowski/Documents/TestyLsi'
# Lokalizacja parameters.yml
PARAM_PATH='/home/marcin_wojciechowski/Documents/TestyLsi/parameters.yml' # wykorzystać $PWD
uruchom_testy(){
# uruchom testy
        echo -e "\e[4;32m Uruchomienie testów $INSTANCJA na przeglądarce $BROWSER \e[0m"
        PATH="$PATH:$TESTY_PATH"      # dodaje do PATH sciezke do webdriverow
        pybot -v IdWniosku:$WNIOSEK -v IdNaboru:$NABOR -v IdWnioskuDoZlozenia:$WNIOSEK_ZLOZ -v BROWSER:$BROWSER -v BrowserMode:Grid -v HOMEPAGE:$INSTANCJA -d $TESTY_PATH/TestResult --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania --removekeywords name:Resource.UtworzZmiennaDaneBazyZYml $TESTY_PATH/GitLab
        echo -e "\e[4;32m Zakończono testy $INSTANCJA na przeglądarce $BROWSER \e[0m"
}

uruchom_testy_firefox(){
# uruchom testy na firefox
        BROWSER='firefox'
}

uruchom_testy_chrome(){
# uruchom testy na chrome
        BROWSER='chrome'
}

uruchom_testy_ie(){
# uruchom testy na ie
        BROWSER='ie'
}

echo -e "\e[4;31m Zamierzasz uruchomić testy na $INSTANCJA \e[0m"
echo -e "Wybierz przeglądarkę: 1 - Firefox; 2 - Chrome; 3 - Internet Explorer"
read -r -p "Twój wybór? [1/2/3] " response
case $response in
    1)
        uruchom_testy_firefox
        ;;
    2)
        uruchom_testy_chrome
        ;;
    3)
        uruchom_testy_ie
        ;;
    *)
        exit 0
        ;;
esac
case $response in
    1|2|3)
        uruchom_testy
        ;;
    *)
        exit 0
        ;;
esac