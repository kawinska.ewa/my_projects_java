*** Settings ***
Documentation     *W związku z koniecznością ukrycia hasła pakiet należy uruchamiać z prarametrami --outputdir TestResult --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania*
Suite Setup       Otworz Przegladarke Na Stronie Logowania
Suite Teardown    Close Browser
Test Setup        Idz Do Strony Logowania
Test Teardown     Wyloguj
Default Tags      Wniosek nabor id156
Library           Selenium2Library    run_on_failure=Nothing
Library           FakerLibrary    locale=pl_PL
Library           String
Resource          ../Library/LogowanieUI.robot    # Logowanie elementy interface
Resource          ../Library/Dane_do_logowania.robot   # Dane do poprawnego logowania
Resource          ../Library/Resource.robot    # User keywords
Resource          ../Library/Wniosek/Akceptacja_regulaminu.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Informacje_ogolne_o_projekcie.robot    # sekcja wniosku
Resource          ../Library/ElementyWnioskuUI.robot    # zmienine sekcji wniosku
Resource          ../Library/ElementyWniosku.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Wnioskodawca_informacje_ogolne.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Wnioskodawca_informacje_ogolneUI.robot    # zmienine sekcji wniosku
Resource          ../Library/Wniosek/Wnioskodawca_adres_korespondencyjny.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Wnioskodawca_adres_korespondencyjnyUI.robot    # zmienine sekcji wniosku
Resource          ../Library/Wniosek/Klasyfikacja_projektu.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Klasyfikacja_projektuUI.robot    # zmienne sekcji wniosku
Resource          ../Library/Wniosek/Osoba_do_kontaktow_roboczych.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Osoba_do_kontaktow_roboczychUI.robot    # zmienne sekcji wniosku
Resource          ../Library/Wniosek/Wnioskodawca_adres_korespondencyjnyUI.robot    # zmienine sekcji wniosku
Resource          ../Library/Wniosek/Wnioskodawca_adres_korespondencyjny.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Miejsce_realizacji_projektu.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Wskazniki.robot    # część sekcji wniosku
Resource          ../Library/Wniosek/Harmonogram_rzeczowo-finansowy.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Zrodla_finansowania_wydatkow.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Zrodla_finansowania_wydatkowUI.robot    # zmienne sekcji wniosku
Resource          ../Library/Wniosek/Otrzymana_Pomoc.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Otrzymana_PomocUI.robot    # zmienine sekcji wniosku
Resource          ../Library/Wniosek/Oswiadczenia.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Akcje_wniosku.robot    # walidacja, złozenie, wydruk wniosku
Resource          ../Library/Wniosek/Zalaczniki.robot    # sekcja wniosku
Resource          ../Library/WalidacjaUI.robot    # zmienne sekcji wniosku
Resource          ../Library/Wniosek/Informacje_dotyczace_KKK.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Informacje_dotyczace_KKKUI.robot    # zmienine sekcji wniosku
Resource          ../Library/Wniosek/Szczegolowy_opis_projektu.robot    # sekcja wniosku
Resource          ../Library/Wniosek/Szczegolowy_opis_projektuUI.robot    # zmienine sekcji wniosku

*** Test Cases ***
Utworz wniosek
    [Documentation]    Utworzenie wniosku o dofinansowanie w naborze POPW.01.01.01 (id konkursu 156). Id utworzonego wniosku przekazywane będzie do kolejnych przypadków testowych.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Logowanie Powiodlo Sie
    Wybierz Nabor    156

Wypelnij informacje ogolne platforma testowa
    [Documentation]    Wypełniana jest część wniosku "Informacje ogólne platforma startowa" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Tytul projektu    100
    Krotki opis projektu    100
    Wybierz branze  14
    Wybierz platforme startowa
    Zapisz Dane
    Otworz Okno Walidacji
    Walidacja nie powinna wyswietlac    Nazwa pomyslu: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Skrocony opis pomyslu: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Wybierz branze: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Proszę wpisać przynajmniej jedno słowo kluczowe.
    Walidacja nie powinna wyswietlac    Platforma startowa: To pole jest obowiązkowe
    Zamknij Okno Walidacji

Wypelnij informacje ogolne o pomyslodawcy
    [Documentation]    Wypełniana jest część wniosku "POMYSLODAWCA Ogolne" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Imie i nazwisko pomyslodawcy
    Pesel pomyslodawcy
    Zapisz Dane
    Otworz Okno Walidacji
    Walidacja nie powinna wyswietlac    Imie i nazwisko: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    PESEL: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Nieprawidłowy numer PESEL
    Zamknij Okno Walidacji

Wypelnij dane adresowe pomyslodawcy
    [Documentation]    Wypełniana jest część wniosku "Adres pomyslodawcy" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Dodaj adres pomyslodawcy
    Adres pomyslodawcy
    Zapisz Dane
    Otworz Okno Walidacji
    Walidacja nie powinna wyswietlac    Partner nr 1, Adres siedziby Partnera - Kraj: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1, Adres siedziby Partnera - Województwo: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1 Adres siedziby Partnera - Powiat: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1 Adres siedziby Partnera - Gmina: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1 Adres siedziby Partnera - Nr budynku: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1 Adres siedziby Partnera - Kod pocztowy: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Partner nr 1 Adres siedziby Partnera - Miejscowość: To pole jest obowiązkowe
    Zamknij Okno Walidacji

Wypelnij informacje o pomysle
    [Documentation]    Wypełniana jest część wniosku "Informacje o pomysle" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Opis innowacyjnego pomyslu  100
    Opis problemow ktore rozwiazuje produkt potrzeby rynkowej  50
    Opis rozwoju przedsiewziecia w perspektywie 2 lat oraz 5 lat  50
    Glowne zrodla przychodow  50
    Opis konkurencji  75
    Charakterystyka niszy rynkowej lub konkretnego segmentu klientow  50
    Opis etapu rozwoju pomyslu -poziom gotowosci technologicznej  50
    Opis planowanych kosztow wprowadzenia produktu na rynek wraz z ich orientacyjna wysokoscia  50
    Opis zasobow niezbednych do wdrozenia pomyslu  50
    Glowne bariery i ograniczenia powodujacy aktualny brak realizacji przedsiewziecia  50
    Zapisz Dane

Wypelnij informacje o zespole realizujacym pomysl
    [Documentation]    Wypełniana jest część wniosku "Zespol realizujacy pomysl" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Dodaj czlonka realizujacego pomysl
    Dane czlonka zespolu realizujacego pomysl
    Zapisz Dane
    Otworz Okno Walidacji
    Walidacja nie powinna wyswietlac    Konsorcjant nr 1, Nazwa: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Konsorcjant nr 1, Rola w projekcie: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Konsorcjant nr 1, Doświadczenie zawodowe: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Konsorcjant nr 1, Adres siedziby - Telefon: To pole jest obowiązkowe
    Walidacja nie powinna wyswietlac    Konsorcjant nr 1, Adres siedziby - Adres e-mail: Nieprawidłowy adres e-mail
    Zamknij Okno Walidacji

Oswiadczenia
    [Documentation]    Wypełniana jest część wniosku "Oswiadczenia" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Zaznacz Losowo Oswiadczenia
    Wypelnij Podstawe Prawna  100
    Zapisz Dane

Wypelnij zalaczniki
    [Documentation]    Wypełniana jest część wniosku "Załączniki" i wykonywana walidacja.
    [Setup]    Idz Do Strony Logowania
    Zaloguj    ${Login}    ${Password}
    Edytuj Wniosek    ${IdUtworzonegoWniosku}
    Przejdz do edycji zalacznikow
    Dodaj plik    1    4
    Dodaj plik    2    4
    Wroc do edycji wniosku
    Otworz Okno Walidacji
    Walidacja nie powinna wyswietlac    Nie podłączono wymaganej liczby załączników typu: Tabele finansowe
    Click Element    ${WalidacjaPierwszyLink Xpath}
    Zamknij Okno Walidacji
