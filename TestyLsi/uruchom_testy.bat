echo off
REM Instancja do testowania
SET instancja=https://lsi1420.test.parp.gov.pl
REM Wybierz baze
IF "%instancja%"=="https://lsi1420.test.parp.gov.pl" SET baza=lsi1420_test
IF "%instancja%"=="https://lsi1420-lab.parp.gov.pl" SET baza=lsi1420
IF "%instancja%"=="https://lsi1420-maw.parp.gov.pl" SET baza=lsi1420_test_maw
REM Id czynnego naboru do testow
SET nabor=71
REM Id wniosku z czynnego naboru do testow
SET wniosek=1561
REM Id wniosku z czynnego naboru do z�o�enia
SET wniosek_zloz=1764
REM Katalog z testami
SET katalog=C:\Python27\TestyLsi
REM Idz do katalogu z testami
cd /d %katalog%
CLS
ECHO.
ECHO Nazwa uzytkownika posiadajacego prawa odczytu na bazie %baza%:
SET /P uzytk=
ECHO Haslo:
SET /P haslo=
:MENU
REM Czyszczenie ekranu
CLS
ECHO.
ECHO ..........................................................
ECHO Nacisnij 1, 2 lub 3, zeby wybrac zadanie lub 4 zeby wyjsc.
ECHO ..........................................................
ECHO.
ECHO 1 - Testuj %instancja% na Firefox
ECHO 2 - Testuj %instancja% na Chrome
ECHO 3 - Testuj %instancja% na Internet Explorer
ECHO 4 - Wyjdz
ECHO.
SET /P M=Wpisz 1, 2, 3, lub 4 i nacisnij ENTER: 
IF %M%==1 GOTO BROWSER1
IF %M%==2 GOTO BROWSER2
IF %M%==3 GOTO BROWSER3
IF %M%==4 GOTO WYJSCIE
:URUCHOM
start pybot -v BROWSER:%browser% -v IdWniosku:%wniosek% -v IdWnioskuDoZlozenia:%wniosek_zloz% -v IdNaboru:%nabor% -v HOMEPAGE:%instancja% -v DaneBazy:"database='%baza%', user='%uzytk%', password='%haslo%', host='test-lsi1420-psql.parp.gov.pl', port=5432" -d TestResult --timestamp GitLab/
GOTO MENU
:BROWSER1
SET browser=firefox
GOTO URUCHOM
:BROWSER2
SET browser=chrome
GOTO URUCHOM
:BROWSER3
SET browser=ie
GOTO URUCHOM
:WYJSCIE
cd /d %sciezka%
GOTO EOF