echo off
REM Id czynnego naboru do testow
SET nabor=43
REM Id wniosku z czynnego naboru do testow
SET wniosek=7597
REM Katalog z testami
SET katalog=C:\Python27\TestyLsi
REM Sciezki do serwerow www
SET www1=http://srv-lsi1420-www1.parp.gov.pl
SET www2=http://srv-lsi1420-www2.parp.gov.pl
SET www3=http://srv-lsi1420-www3.parp.gov.pl
REM Idz do katalogu z testami
cd /d %katalog%
CLS
ECHO.
ECHO Nazwa uzytkownika posiadajacego prawa odczytu na bazie produkcyjnej:
SET /P uzytk=
ECHO Haslo:
SET /P haslo=
:MENU
REM Czyszczenie ekranu
CLS
ECHO.
ECHO ..........................................................
ECHO Nacisnij 1, 2 lub 3, zeby wybrac zadanie lub 4 zeby wyjsc.
ECHO ..........................................................
ECHO.
ECHO 1 - Testuj zapis na www1
ECHO 2 - Testuj zapis na www2
ECHO 3 - Testuj zapis na www3
ECHO 4 - Wyjdz
ECHO.
SET /P M=Wpisz 1, 2, 3, lub 4 i nacisnij ENTER: 
IF %M%==1 GOTO WWW1
IF %M%==2 GOTO WWW2
IF %M%==3 GOTO WWW3
IF %M%==4 GOTO WYJSCIE
:URUCHOM
start pybot -v IdWniosku:%wniosek% -v IdNaboru:%nabor% -v HOMEPAGE:%www% -v DaneBazy:"database='lsi1420', user='%uzytk%', password='%haslo%', host='srv-lsi1420-psql.parp.gov.pl', port=5432" -d TestResultProd --timestamp GitLab/
GOTO MENU
:WWW1
SET www=%www1%
GOTO URUCHOM
:WWW2
SET www=%www2%
GOTO URUCHOM
:WWW3
SET www=%www3%
GOTO URUCHOM
:WYJSCIE
cd /d %sciezka%
GOTO EOF