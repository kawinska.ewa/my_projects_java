*** Variables ***
${LogotypLogoPOIR XPath}    //img[@src='/media/logo_poir.jpg']
${LogotypLogoPOPW XPath}    //img[@src='/media/logo_popw.jpg']
${LogotypLogoPARP XPath}    //img[@src='/media/logo_parp.jpg']
${LogotypLogoEFRR XPath}    //img[@src='/media/logo_ue_regionalne.jpg']
${AkcjeWnioskuSprawdzPoprawnosc Id}    id=akcja_waliduj_wniosek
${AkcjeWnioskuWydrukPDF Id}    id=akcja_drukuj_wniosek
${AkcjeWnioskuZapisz Id}    id=akcja_zapisz_wniosek
${AkcjeWnioskuZloz Id}    id=akcja_zloz_wniosek
${AkcjeWnioskuPrzyciskNiebieskiKlasa}    btn-primary
${AkcjeWnioskuPrzyciskZielonyKlasa}    btn-success
${OIOPNumerWnioskuPoprzedniEtapLabel}    Nr wniosku o dofinansowanie w ramach I Etapu działania    # Nr wniosku o dofinansowanie w ramach I Etapu działania
${OIOPKodWnioskuPoprzedniEtapLabel}    Kod wniosku z poprzedniego etapu w ramach I Etapu działania    # Kod wniosku z poprzedniego etapu
${OIOPTytulProjektuLabel}    Tytuł projektu    # Tytuł projektu
${OIOPKrotkiOpisProjektuLabel}    Krótki opis projektu    # Krótki opis projektu
${OIOPRozszerzonyOpisProjektuLabel}    Rozszerzony opis projektu    # Rozszerzony opis projektu
${OIOPCelProjektuLabel}    Cel projektu    # Cel projektu
${OIOPOpisProcesuProjektowegoLabel}    Opis procesu projektowego    # Opis procesu projektowego
${OIOPOpisDzialanWdrozeniowychLabel}    Opis i uzasadnienie działań wdrożeniowych    # Opis i uzasadnienie działań wdrożeniowych
${OIOPSlowaKluczoweLabel}    Słowa kluczowe    # Słowa kluczowe
${OIOPSlowaKluczoweButton XPath}    //button[@data-bind='click: addSlowo']    # Słowa kluczowe przycisk XPath
${OIOPZnaczenieProjektuLabel}    Znaczenie projektu dla dalszego rozwoju firmy    # Znaczenie projektu dla dalszego rozwoju firmy
${OIOPOczekiwaneEfektyLabel}    Oczekiwane efekty projektu    # Oczekiwane efekty projektu
${OIOPOczekiwaneEfekty10000Label}    Oczekiwane efekty projektu    # Oczekiwane efekty projektu (10000 znaków)
${OIOPProjektDotyczyInnowcjiProduktowejLabel}    Projekt dotyczy innowacji produktowej    # Projekt dotyczy innowacji produktowej
${OIOPPoziomInnowacyjnosciLabel}    Poziom innowacyjności produktu    # Poziom innowacyjności produktu
${OIOPOpisInnowacjiLabel}    Opis innowacji wdrażanej w ramach projektu    # Opis innowacji wdrażanej w ramach projektu
${OIOPOkresRealizacjiProjektuPoczatekLabel}    Okres realizacji projektu <od>    # Okres realizacji projektu (początek)
${OIOPOkresRealizacjiProjektuKoniecLabel}    Okres realizacji projektu <do>    # Okres realizacji projektu (koniec)
${OIOPInstrumentyFinansoweLabel}    Instrumenty finansowe    # Instrumenty finansowe
${OIOPKategorieDrogiLabel}    Kategoria drogi    # Kategorie drogi
${OIOPOkresWdrozeniaInnowacjiPoczatekLabel}    Okres wdrożenia innowacji <od>    # Okres wdrożenia innowacji (początek)
${OIOPOkresWdrozeniaInnowacjiKoniecLabel}    Okres wdrożenia innowacji <do>    # Okres wdrożenia innowacji (koniec)
${OIOPKodWnioskuPoprzedniEtap Id}    id=ogolne_projekt_kodWnioskuPoprzedniEtap    # Kod wniosku z poprzedniego etapu w ramach I Etapu działania
${OIOPNumerWnioskuPoprzedniEtap Id}    id=ogolne_projekt_numerWnioskuPoprzedniEtap    # Nr wniosku o dofinansowanie w ramach I Etapu działania
${OIOPTytulProjektu Id}    id=ogolne_projekt_tytulProjektu    # Tytuł projektu
${OIOPKrotkiOpisProjektu Id}    id=ogolne_projekt_krotkiOpisProjektu    # Krótki opis projektu
${OIOPCelProjektu Id}    id=ogolne_projekt_celProjektu    # Cel projektu
${OIOPZgodnoscProjektuZDzialalnosciaWnioskodawcy Id}    id=ogolne_projekt_zgodnoscProjektuZDzalalnosciaWnioskodawcy    # Zgodność projektu z działalnością Wnioskodawcy
${OIOPOpisProcesuProjektowego Id}    id=ogolne_projekt_opisProcesuProjektowego    # Opis procesu projektowego
${OIOPOpisDzialanWdrozeniowych Id}    id=ogolne_projekt_opisDzialanWdrozeniowych    # Opis i uzasadnienie działań wdrożeniowych
${OIOPOczekiwaneEfekty Id}    id=ogolne_projekt_oczekiwaneEfekty    # Oczekiwane efekty projektu
${OIOPProjektDotyczyInnowcjiProduktowej Id}    id=ogolne_projekt_projektDotyczyInnowcjiProduktowej    # Projekt dotyczy innowacji produktowej
${OIOPPoziomInnowacyjnosci Id}    id=ogolne_projekt_poziomInnowacyjnosci    # Poziom innowacyjności produktu
${OIOPOpisInnowacji Id}    id=ogolne_projekt_opisInnowacji    # Opis innowacji wdrażanej w ramach projektu
${OIOPProjektDotyczyInnowcjiInnejNizProduktowa Id}    id=ogolne_projekt_dotyczyInnowacjiInnejNizProduktowa    # Projekt dotyczy innowacji innej niż produktowa
${OIOPOpisInnowcjiInnejNizProduktowa Id}    id=ogolne_projekt_opisInnowacjiInnejNizProduktowa    # Opis innowacji (innej niż produktowa) wdrażanej w ramach projektu
${OIOPOkresRealizacjiProjektuPoczatek Id}    id=ogolne_projekt_okresRealizacjiProjektuPoczatek    # Okres realizacji projektu (początek)
${OIOPOkresRealizacjiProjektuKoniec Id}    id=ogolne_projekt_okresRealizacjiProjektuKoniec    # Okres realizacji projektu (koniec)
${OIOPTytulProjektuLen}    1000    # Tytuł projektu dopuszczalna liczba znaków
${OIOPKrotkiOpisProjektuLen}    2000    # Krótki opis projektu dopuszczalna liczba znaków
${OIOPCelProjektuLen}    1000    # Cel projektu dopuszczalna liczba znaków
${OIOPTytulProjektuBazaTabela}    wnioski.wnioski_ogolne_projekt    # Tabela z polem Tytuł projektu
${OIOPTytulProjektuBazaPole}    tytul_projektu    # Nazwa pola z bazy dla Tytuł projektu
${OIOPKrotkiOpisProjektuTabela}    wnioski.wnioski_ogolne_projekt    # Tabela z polem Krótki opis projektu
${OIOPKrotkiOpisProjektuPole}    krotki_opis_projektu    # Nazwa pola z bazy dla Krótki opis projektu
${OIOPCelProjektuTabela}    wnioski.wnioski_ogolne_projekt    # Tabela z polem Cel projektu
${OIOPCelProjektuPole}    cel_projektu    # Nazwa pola z bazy dla Cel projektu
${OIOPOkresRealizacjiProjektuPoczatekMin}    2013-01-01
${OIOPOkresRealizacjiProjektuKoniecMin}    2013-12-31
${OIOPOkresRealizacjiProjektuPoczatekMax}    2024-12-31
${OIOPOkresRealizacjiProjektuKoniecMax}    2024-01-01
${OIOPSlowaKluczowePrzyciskDodaj XPath}    //button[@data-bind='click: addSlowo']
${OIOPSlowaKluczowePrzyciskUsun XPath}    //button[@data-bind='click: removeSlowo']
${OIOPSlowaKluczowePoleTekstowe Id}    //*[@id='ogolne_projekt_slowaKluczowe_x_nazwa']
${OIOPSlowaKluczowePoleTekstowe XPath}    //*[contains(@id,'ogolne_projekt_slowaKluczowe_') and contains(@id,'_nazwa')]
${OIOPSlowaKluczoweLen}    255
${OIOPSlowaKluczoweLiczba}    5
${OIOPSlowaKluczowePrefix}    Slowo
${OIOPDziedzinaProjektu Id}    id=ogolne_projekt_dziedzinyProjektu    # Dziedzina projektu
${OIOPDziedzinaProjektu XPath}    //*[@id='ogolne_projekt_dziedzinyProjektu']    # Dziedzina projektu
${OIOPBranzaPomyslu XPath}        //*[@id='ogolne_projekt_branza']  #Branza pomyslu
${OIOPPlatformaStartowa XPath}    //*[@id='ogolne_projekt_platformaStartowa']  #Platformy startowe
${WcaOPesel Id}  id=wnioskodawca_ogolne_pesel  #Pesel pomyslodawcy
${WcaONazwaLabel}    Nazwa Wnioskodawcy    # Nazwa wnioskodawcy
${WcaONazwaSkroconaLabel}    //*[@id='modal']//*[@value='nazwaSkrocona']    # Nazwa wnioskodawcy - skrócona
${WcaOStatusLabel}    Status Wnioskodawcy    # Status wnioskodawcy
${WcaOStatusWithDuzyLabel}    //*[@id='modal']//*[@value='statusWithDuzy']    # Status wnioskodawcy zawierający włączoną opcję "Duży"
${WcaOStatusWithNieDotyczyLabel}    //*[@id='modal']//*[@value='statusWithNieDotyczy']    # Status wnioskodawcy zawierający opcję "Nie dotyczy" z brakiem możliwości wybrania czegokolwiek innego
${WcaOStatusMikroMalyLabel}    //*[@id='modal']//*[@value='statusMikroMaly']    # Status wnioskodawcy zawierający włączone opcję "Mikro" i "Mały"
${WcaODataRozpoczeciaDzialalnosciLabel}    Data rozpoczęcia działalności zgodnie z dokumentem rejestrowym    # Data rozpoczęcia działalności
${WcaOFormaPrawnaLabel}    Forma prawna Wnioskodawcy    # Forma prawna wnioskodawcy
${WcaOFormaWlasnosciLabel}    Forma własności    # Forma własności
${WcaONipLabel}    NIP Wnioskodawcy    # NIP
${WcaORegonLabel}    REGON    # REGON
${WcaOKrsLabel}    Numer w Krajowym Rejestrze Sądowym    # KRS
${WcaOPeselLabel}    PESEL    # PESEL
${WcaOInnyRejestrLabel}    //*[@id='modal']//*[@value='innyRejestr']    # Inny rejestr
${WcaOPkdLabel}    Numer kodu PKD przeważającej działalności Wnioskodawcy    # Numer PKD przeważającej działalności wnioskodawcy
${WcaOMozliwoscOdzyskaniaVatLabel}    Możliwość odzyskania VAT    # Możliwość odzyskania VAT
${WcaOUzasadnienieBrakuOdzyskaniaVATLabel}    Uzasadnienie braku możliwości odzyskania VAT    # Uzasadnienie braku możliwości odzyskania VAT
${WcaOUdzialWydatkowNaDzialanoscBRLabel}    //*[@id='modal']//*[@value='udzialWydatkowNaDzialanoscBR']    # Udział wydatków na działalność B+R w działalności gospodarczej Wnioskodawcy w ciągu ostatnich, zamkniętych 3 lat obrachunkowych (%)
${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBRLabel}    //*[@id='modal']//*[@value='metodologiaOkreslaniaWydatkowNaDzialalnoscBR']    # Metodologia określania wysokości wydatków na działalność
${WcaOIloscPatentyLabel}    //*[@id='modal']//*[@value='iloscPatenty']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Patenty
${WcaOIloscPrawoOchronneNaWzorPrzemyslowyLabel}    //*[@id='modal']//*[@value='iloscPrawoOchronneNaWzorPrzemyslowy']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Prawo ochronne na wzór użytkowy
${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowegoLabel}    //*[@id='modal']//*[@value='iloscPrawoZRejestracjiWzoruPrzemyslowego']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Prawo z rejestracji wzoru przemysłowego:
${WcaORodzajOsrodkaInnowacjiLabel}    //*[@id='modal']//*[@value='rodzajOsrodkaInnowacji']    # Rodzaj ośrodka innowacji
${WcaOSiedzibaLabel}    Adres siedziby/miejsce zamieszkania wnioskodawcy    # Adres siedziby/miejsca zamieszkania Wnioskodawcy
${WcaOAdresKrajPlLabel}    Kraj    # Kraj - Polska
${WcaOAdresWojewodztwoLabel}    Województwo    # Województwo
${WcaOAdresPowiatLabel}    Powiat    # Powiat
${WcaOAdresGminaLabel}    Gmina    # Gmina
${WcaOAdresKrajSf2Label}    //*[@id='modal']//*[@value='adres.krajSf2']    # Kraj zagranica
${WcaOAdresKodPocztowyLabel}    Kod pocztowy    # Kod pocztowy
${WcaOAdresPocztaLabel}    Poczta    # Poczta
${WcaOAdresMiejscowoscLabel}    //*[@id='modal']//*[@value='adres.miejscowosc']    # (NIE UZYWAĆ) Miejscowość
${WcaOAdresMiejscowoscMirIdLabel}    Miejscowość    # Miejscowość - lista rozwijalna
${WcaOAdresMiejscowoscZagranicaLabel}    //*[@id='modal']//*[@value='adres.miejscowoscZagranica']    # Miejscowość zagranica
${WcaOAdresUlicaLabel}    //*[@id='modal']//*[@value='adres.ulica']    # (NIE UŻYWAĆ) Ulica
${WcaOAdresUlicaMirIdLabel}    Ulica    # Ulica - lista rozwijalna
${WcaOAdresNrBudynkuLabel}    Nr budynku    # Nr budynku
${WcaOAdresNrLokaluLabel}    Nr lokalu    # Nr lokalu
${WcaOAdresUlicaZagranicaLabel}    //*[@id='modal']//*[@value='adres.ulicaZagranica']    # Ulica zagranica
${WcaOAdresTelefonLabel}    Telefon    # Telefon
${WcaOAdresFaksLabel}    Fax    # Faks
${WcaOAdresEmailLabel}    Adres e-mail    # E-mail
${WcaOAdresStronaWwwLabel}    Adres strony WWW    # Adres WWW
${WcaOHistoriaWnioskodawcyLabel}    //*[@id='modal']//*[@value='historiaWnioskodawcy']    # Historia wnioskodawcy oraz przedmiot działalności w kontekście projektu
${WcaOMiejsceNaRynkuLabel}    //*[@id='modal']//*[@value='miejsceNaRynku']    # Miejsce na rynku
${WcaOCharakterystykaRynkuLabel}    //*[@id='modal']//*[@value='charakterystykaRynku']    # Charakterystyka rynku
${WcaOOczekiwaniaPotrzebyKlientowLabel}    //*[@id='modal']//*[@value='oczekiwaniaPotrzebyKlientow']    # Oczekiwania i potrzeby klientów
${WcaOCharakterPopytuLabel}    //*[@id='modal']//*[@value='charakterPopytu']    # Charakter popytu
${WcaOWielkoscZatrudnieniaLabel}    //*[@id='modal']//*[@value='wielkoscZatrudnienia']    # Wielkość zatrudnienia
${WcaOPrzychodyZeSprzedazyOstatniRokLabel}    Przychody ze sprzedaży w ostatnim zamkniętym roku obrotowym:    # Przychody ze sprzedaży w ostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagranicznaLabel}    //*[@id='modal']//*[@value='przychodyZeSprzedazyOstatniRokSprzedazZagraniczna']    # Przychody ze sprzedaży w ostatnim zamkniętym roku obrotowym, w tym przychody ze sprzedaży zagranicznej
${WcaOProcentZeSprzedazyOstatniRokSprzedazZagranicznaLabel}    //*[@id='modal']//*[@value='procentZeSprzedazyOstatniRokSprzedazZagraniczna']    # Przychody ze sprzedaży zagranicznej w ostatnim zamkniętym roku obrotowym jako % sumy ze sprzedaży w ostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyPrzedostatniRokLabel}    Przychody ze sprzedaży w przedostatnim zamkniętym roku obrotowym:    # Przychody ze sprzedaży w przedostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRokLabel}    Przychody ze sprzedaży w roku obrotowym poprzedzającym przedostatni zamknięty rok obrotowy:    # Przychody ze sprzedaży w roku obrotowym poprzedzającym przedostatni zamknięty rok obrotowy
${WcaOOpisProwadzonejDzialalnosciLabel}    //*[@id='modal']//*[@value='opisProwadzonejDzialalnosci']    # Opis prowadzonej działalności
${WcaOOfertaWnioskodawcyLabel}    //*[@id='modal']//*[@value='ofertaWnioskodawcy']    # Oferta wnioskodawcy
${WcaOOczekiwaniaIPotrzebyOdbiorcowLabel}    //*[@id='modal']//*[@value='oczekiwaniaIPotrzebyOdbiorcow']    # Oczekawnia i potrzeby odbiorców
${WcaODoswiadczenieWeWzornictwieLabel}    //*[@id='modal']//*[@value='doswiadczenieWeWzornictwie']    # Doświadczenie we wzornictwie
${WcaOWspolnicyLabel}    //*[@id='modal']//*[@value='wspolnicy']    # Wspólnicy
${WcaOWspolnicyUlicaLabel}    //*[@id='modal']//*[@value='wspolnicy.ulica']    # Ulica (wspólnicy)
${WcaOWspolnicyNrBudynkuLabel}    //*[@id='modal']//*[@value='wspolnicy.nrBudynku']    # Nr budynku (wspólnicy)
${WcaOWspolnicyNrLokaluLabel}    //*[@id='modal']//*[@value='wspolnicy.nrLokalu']    # Nr lokalu (wspólnicy)
${WcaOWspolnicyKodPocztowyLabel}    //*[@id='modal']//*[@value='wspolnicy.kodPocztowy']    # Kod pocztowy (wspólnicy)
${WcaOWspolnicyPocztaLabel}    //*[@id='modal']//*[@value='wspolnicy.poczta']    # Poczta (wspólnicy)
${WcaOWspolnicyMiejscowoscLabel}    //*[@id='modal']//*[@value='wspolnicy.miejscowosc']    # Miejscowość (wspólnicy)
${WcaOWspolnicyTelefonLabel}    //*[@id='modal']//*[@value='wspolnicy.telefon']    # Telefon (wspólnicy)
${WcaOWspolnicyFaksLabel}    //*[@id='modal']//*[@value='wspolnicy.faks']    # Faks (wspólnicy)
${WcaOWspolnicyEmailLabel}    //*[@id='modal']//*[@value='wspolnicy.email']    # E-mail (wspólnicy)
${WcaOWspolnicyStronaWwwLabel}    //*[@id='modal']//*[@value='wspolnicy.stronaWww']    # Adres WWW (wspólnicy)
${WcaOProduktyPodlegajaceInternacjonalizacjiLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji']    # Produkty, które zdaniem Wnioskodawcy mogą podlegać internacjonalizacji
${WcaOProduktyPodlegajaceInternacjonalizacjiNazwaLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji.nazwa']    # Nazwa i charakterystyka produktu
${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji.kodCn']    # Kod CN wyrobu
${WcaOProduktyPodlegajaceInternacjonalizacjiCzyDotyczyKodCnLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji.czyDotyczyKodCn']    # Kod CN wyrobu - pole czy dotyczy
${WcaOProduktyPodlegajaceInternacjonalizacjiInformacjeLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji.informacje']    # Informacje na temat sprzedaży, wytwarzania i rozwoju produktu
${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiLabel}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji.rynki']    # Rynki (kraje), na których Wnioskodawca prowadzi lub prowadził sprzedaż produktu
${WcaORynkiZagraniczneLabel}    //*[@id='modal']//*[@value='rynkiZagraniczne']    # Rynki zagraniczne (kraje), na których Wnioskodawca prowadzi sprzedaż pozostałych produktów
${WcaOCzyDotyczyRynkiZagraniczneLabel}    //*[@id='modal']//*[@value='czyDotyczyRynkiZagraniczne']    # Rynki zagraniczne (kraje), na których Wnioskodawca prowadzi sprzedaż pozostałych produktów - pole czy dotyczy
${WcaOAdresKrajPl Id}    id=wnioskodawca_ogolne_siedziba_krajPl    # Kraj - Polska
${WcaOAdresWojewodztwo Id}    id=select2-wnioskodawca_ogolne_siedziba_wojewodztwo-container    # Województwo
${WcaOAdresPowiat Id}    id=select2-wnioskodawca_ogolne_siedziba_powiat-container    # Powiat
${WcaOAdresGmina Id}    id=select2-wnioskodawca_ogolne_siedziba_gmina-container    # Gmina
${WcaOAdresKodPocztowy Id}    id=wnioskodawca_ogolne_siedziba_kodPocztowy    # Kod pocztowy
${WcaOAdresPoczta Id}    id=wnioskodawca_ogolne_siedziba_poczta    # Poczta
${WcaOAdresMiejscowoscMirId Id}    id=select2-wnioskodawca_ogolne_siedziba_miejscowoscMirId-container    # Miejscowość - lista rozwijalna
${WcaOAdresUlicaMirId Id}    id=select2-wnioskodawca_ogolne_siedziba_ulicaMirId-container    # Ulica - lista rozwijalna
${WcaOAdresNrBudynku Id}    id=wnioskodawca_ogolne_siedziba_nrBudynku    # Nr budynku
${WcaOAdresNrLokalu Id}    id=wnioskodawca_ogolne_siedziba_nrLokalu    # Nr lokalu
${KrajPolskaLabel}    Polska    # Label dla listy rozwijanej kraj, pozycja Polska
${KrajPolskaLabelPusty}    Proszę wybrać    # Label dla listy rozwijanej kraj, pozycja Proszę wybrać
${ZapytanieWojewodztwo}    SELECT mir_nazwa FROM slowniki.jednostki_geo WHERE kod_nts_pelny LIKE '_.__' ORDER BY mir_kod_nts;    # Zwraca listę nazw województw
${ZapytaniePowiat}    SELECT powiat.mir_nazwa FROM slowniki.jednostki_geo woj JOIN slowniki.jednostki_geo powiat ON left(powiat.kod_nts_pelny, 4) = woj.kod_nts_pelny WHERE woj.mir_nazwa = '{Wojewodztwo}' AND powiat.kod_nts_pelny LIKE '_.__.__.__' ORDER BY powiat.mir_nazwa;    # Zwraca listę powiatów dla wskazanego województwa
${ZapytanieGmina}    SELECT gmina.mir_nazwa FROM slowniki.jednostki_geo woj JOIN slowniki.jednostki_geo powiat ON left(powiat.kod_nts_pelny, 4) = woj.kod_nts_pelny JOIN slowniki.jednostki_geo gmina ON left(gmina.kod_nts_pelny, 10) = powiat.kod_nts_pelny WHERE woj.mir_nazwa = '{Wojewodztwo}' AND powiat.mir_nazwa = '{Powiat}' AND powiat.kod_nts_pelny LIKE '_.__.__.__' AND gmina.kod_nts_pelny LIKE '_.__.__.__.__' ORDER BY gmina.mir_nazwa;    # Zwraca listę gmin dla wskazanego powiatu
${ZapytanieMiejscowosc}    SELECT miejscowosci.msc_nazwa || ' (' || CASE miejscowosci.msc_rodzaj_gm WHEN '1' THEN 'gmina miejska' WHEN '2' THEN 'gmina wiejska' WHEN '3' THEN 'gmina miejsko-wiejska' WHEN '4' THEN 'miasto w gminie miejsko-wiejskiej' WHEN '5' THEN 'obszar wiejski w gminie miejsko-wiejskiej' WHEN '8' THEN 'dzielnica Warszawy' WHEN '9' THEN 'delegatura lub dzielnica innych gmin miejskich' END || ')' nazwa FROM slowniki.jednostki_geo woj JOIN slowniki.jednostki_geo powiat ON left(powiat.kod_nts_pelny, 4) = woj.kod_nts_pelny JOIN slowniki.jednostki_geo gmina ON left(gmina.kod_nts_pelny, 10) = powiat.kod_nts_pelny JOIN slowniki.miejscowosci ON miejscowosci.msc_jge_id = gmina.mir_id WHERE woj.mir_nazwa = '{Wojewodztwo}' AND powiat.mir_nazwa = '{Powiat}' AND gmina.mir_nazwa ='{Gmina}' AND powiat.kod_nts_pelny LIKE '_.__.__.__' AND gmina.kod_nts_pelny LIKE '_.__.__.__.__' ORDER BY miejscowosci.msc_nazwa;    # Zwraca listę miejscowości dla wskazanej gminy (w formacie w jakim lista jest wyświetlana w LSI)
${ZapytanieUlica}    SELECT ulice.ulc_nazwa FROM slowniki.jednostki_geo woj JOIN slowniki.jednostki_geo powiat ON left(powiat.kod_nts_pelny, 4) = woj.kod_nts_pelny JOIN slowniki.jednostki_geo gmina ON left(gmina.kod_nts_pelny, 10) = powiat.kod_nts_pelny JOIN slowniki.miejscowosci ON miejscowosci.msc_jge_id = gmina.mir_id JOIN slowniki.ulice ON ulice.ulc_msc_id = miejscowosci.msc_id WHERE woj.mir_nazwa = '{Wojewodztwo}' AND powiat.mir_nazwa = '{Powiat}' AND gmina.mir_nazwa ='{Gmina}' AND miejscowosci.msc_nazwa || ' (' || CASE miejscowosci.msc_rodzaj_gm WHEN '1' THEN 'gmina miejska' WHEN '2' THEN 'gmina wiejska' WHEN '3' THEN 'gmina miejsko-wiejska' WHEN '4' THEN 'miasto w gminie miejsko-wiejskiej' WHEN '5' THEN 'obszar wiejski w gminie miejsko-wiejskiej' WHEN '8' THEN 'dzielnica Warszawy' WHEN '9' THEN 'delegatura lub dzielnica innych gmin miejskich' END || ')' = '{Miejscowosc}' AND powiat.kod_nts_pelny LIKE '_.__.__.__' AND gmina.kod_nts_pelny LIKE '_.__.__.__.__' ORDER BY ulice.ulc_nazwa;    # Zwraca listę ulic dla wskazanej miejscowości
${WcaOAdresNrBudynkuLen}    10    # Nr budynku liczba znaków
${WcaOAdresNrLokaluLen}    10    # Nr lokalu liczba znaków
${WcaOAdresKodPocztowyLen}    6    # Kod pocztowy liczba znaków
${WcaOAdresPocztaLen}    50    # Poczta liczba znaków
${PoleSzukajWLiscie XPath}    //html/body/span/span/span[1]/input    # Pole tekstowe szukaj w liście wyboru
${WcaDodajCzlonkaRealPomysl}  //button[@data-bind='click: addKonsorcjant']  # Przycisk dodaj w sekcji Zespol realizujacy pomysl