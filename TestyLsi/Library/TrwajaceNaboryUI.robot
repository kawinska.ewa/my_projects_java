*** Variables ***
${AdresNaboru XPath}    //a[@href="/wniosek/utworz/IdNaboru"]    # Adres przycisku "Utwórz wniosek" dla wybranego Id naboru
${AdresWnioskuEdycja}    /wniosek/edytuj/
${AdresWnioskuPodglad}    /wniosek/pokaz/
${AdresListaKonkursow}    /wniosek/wyswietlListeKonkursow
${AdresUsunWniosek}    /wniosek/usun/
${ModalUsun Id}    id=modal
${ModalUsunPrzycisk XPath}    //div[@id='modal']//a[contains(text(),'Usuń wniosek')]    # Przycisk Usuń wniosek
${AlertUtworzono}    Pomyślnie utworzono wniosek    # Treść alertu po utworzeniu wniosku
${AlertUsunieto}    Pomyślnie usunięto wniosek    # Treść alertu po usunięciu wniosku
${AlertSuccess CSS}    //*[@class="alert alert-success alert-dismissible"]    # Styl alertu Pomyślnie utworzono/usunięto wniosek było: 'css=.alert.alert-success'
${ModalUsunH1 CSS}    css=.modal h1    # Styl nagłówka modala Usunięcie wniosku
${ModalUsunH1Tresc}    Usunięcie wniosku    # Treść nagłówka modala Usunięcie wniosku
${FiltrIdWniosku Id}    id=grid_6769656d814e9a9acbc61f559a7b16f6__id__query__from    # Id pola z filtra do wpisania Id wniosku
${FiltrFiltrujPrzycisk XPath}    //*[@id='grid_6769656d814e9a9acbc61f559a7b16f6']//button[text()='Filtruj']    # Przycisk Filtruj z filtra do wpisania
