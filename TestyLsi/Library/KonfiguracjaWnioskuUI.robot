*** Variables ***
${ModalCheckbox XPath}    //*[@id='modal']//*[@type='checkbox']    # Każdy checkbox w modalu z konfiguracją elementu
${ModalNazwa Id}    id=form_label    # Pole Nazwa na modalu z konfiguracją elementu
${LogotypLogoLewe Id}    id=form_image_left
${LogotypLogoSrodkowe Id}    id=form_image_center
${LogotypLogoPrawe Id}    id=form_image_right
${OIOPNumerWnioskuPoprzedniEtap XPath}    //*[@id='modal']//*[@value='numerWnioskuPoprzedniEtap']    # Nr wniosku o dofinansowanie w ramach I Etapu działania
${OIOPKodWnioskuPoprzedniEtap XPath}    //*[@id='modal']//*[@value='kodWnioskuPoprzedniEtap']    # Kod wniosku z poprzedniego etapu
${OIOPUkryjNumerNaboru1Etap XPath}    //*[@id='modal']//*[@value='ukryjNumerNaboru1Etap']    # nie pokazuj pól "Numer naboru w ramach I Etapu działania" oraz "Data otrzymania informacji o zatwierdzeniu strategii wzorniczej"
${OIOPTytulProjektu XPath}    //*[@id='modal']//*[@value='tytulProjektu']    # Tytuł projektu
${OIOPDataZlozeniaWNPKoncowaIEtap XPath}    //*[@id='modal']//*[@value='dataZlozeniaPierwszyEtap']
${OIOPKrotkiOpisProjektu XPath}    //*[@id='modal']//*[@value='krotkiOpisProjektu']    # Krótki opis projektu
${OIOPRozszerzonyOpisProjektu XPath}    //*[@id='modal']//*[@value='rozszerzonyOpisProjektu']    # Rozszerzony opis projektu
${OIOPCelProjektu XPath}    //*[@id='modal']//*[@value='celProjektu']    # Cel projektu
${OIOPOpisProcesuProjektowego XPath}    //*[@id='modal']//*[@value='opisProcesuProjektowego']    # Opis procesu projektowego
${OIOPOpisDzialanWdrozeniowych XPath}    //*[@id='modal']//*[@value='opisDzialanWdrozeniowych']    # Opis i uzasadnienie działań wdrożeniowych
${OIOPSlowaKluczowe XPath}    //*[@id='modal']//*[@value='slowaKluczowe']    # Słowa kluczowe
${OIOPDziedzinyProjektu XPath}    //*[@id='modal']//*[@value='dziedzinyProjektu']    # Dziedziny projektu
${OIOPZnaczenieProjektu XPath}    //*[@id='modal']//*[@value='znaczenieProjektu']    # Znaczenie projektu dla dalszego rozwoju firmy
${OIOPOczekiwaneEfekty XPath}    //*[@id='modal']//*[@value='oczekiwaneEfekty']    # Oczekiwane efekty projektu
${OIOPOczekiwaneEfekty10000 XPath}    //*[@id='modal']//*[@value='oczekiwaneEfekty10000']    # Oczekiwane efekty projektu (10000 znaków)
${OIOPProjektDotyczyInnowcjiProduktowej XPath}    //*[@id='modal']//*[@value='projektDotyczyInnowcjiProduktowej']    # Projekt dotyczy innowacji produktowej
${OIOPPoziomInnowacyjnosci XPath}    //*[@id='modal']//*[@value='poziomInnowacyjnosci']    # Poziom innowacyjności produktu
${OIOPOpisInnowacji XPath}    //*[@id='modal']//*[@value='opisInnowacji']    # Opis innowacji wdrażanej w ramach projektu
${OIOPOkresRealizacjiProjektuPoczatek XPath}    //*[@id='modal']//*[@value='okresRealizacjiProjektuPoczatek']    # Okres realizacji projektu (początek)
${OIOPOkresRealizacjiProjektuKoniec XPath}    //*[@id='modal']//*[@value='okresRealizacjiProjektuKoniec']    # Okres realizacji projektu (koniec)
${OIOPInstrumentyFinansowe XPath}    //*[@id='modal']//*[@value='instrumentyFinansowe']    # Instrumenty finansowe
${OIOPKategorieDrogi XPath}    //*[@id='modal']//*[@value='kategorieDrogi']    # Kategorie drogi
${OIOPOkresWdrozeniaInnowacjiPoczatek XPath}    //*[@id='modal']//*[@value='okresWdrozeniaInnowacjiPoczatek']    # Okres wdrożenia innowacji (początek)
${OIOPOkresWdrozeniaInnowacjiKoniec XPath}    //*[@id='modal']//*[@value='okresWdrozeniaInnowacjiKoniec']    # Okres wdrożenia innowacji (koniec)
${WcaONazwa XPath}    //*[@id='modal']//*[@value='nazwa']    # Nazwa wnioskodawcy
${WcaONazwaSkrocona XPath}    //*[@id='modal']//*[@value='nazwaSkrocona']    # Nazwa wnioskodawcy - skrócona
${WcaOStatus XPath}    //*[@id='modal']//*[@value='status']    # Status wnioskodawcy
${WcaOStatusWithDuzy XPath}    //*[@id='modal']//*[@value='statusWithDuzy']    # Status wnioskodawcy zawierający włączoną opcję "Duży"
${WcaOStatusWithNieDotyczy XPath}    //*[@id='modal']//*[@value='statusWithNieDotyczy']    # Status wnioskodawcy zawierający opcję "Nie dotyczy" z brakiem możliwości wybrania czegokolwiek innego
${WcaOStatusMikroMaly XPath}    //*[@id='modal']//*[@value='statusMikroMaly']    # Status wnioskodawcy zawierający włączone opcję "Mikro" i "Mały"
${WcaODataRozpoczeciaDzialalnosci XPath}    //*[@id='modal']//*[@value='dataRozpoczeciaDzialalnosci']    # Data rozpoczęcia działalności
${WcaOFormaPrawna XPath}    //*[@id='modal']//*[@value='formaPrawna']    # Forma prawna wnioskodawcy
${WcaOFormaWlasnosci XPath}    //*[@id='modal']//*[@value='formaWlasnosci']    # Forma własności
${WcaONip XPath}    //*[@id='modal']//*[@value='nip']    # NIP
${WcaORegon XPath}    //*[@id='modal']//*[@value='regon']    # REGON
${WcaOKrs XPath}    //*[@id='modal']//*[@value='krs']    # KRS
${WcaOPesel XPath}    //*[@id='modal']//*[@value='pesel']    # PESEL
${WcaOInnyRejestr XPath}    //*[@id='modal']//*[@value='innyRejestr']    # Inny rejestr
${WcaOPkd XPath}    //*[@id='modal']//*[@value='pkd']    # Numer PKD przeważającej działalności wnioskodawcy
${WcaOMozliwoscOdzyskaniaVat XPath}    //*[@id='modal']//*[@value='mozliwoscOdzyskaniaVat']    # Możliwość odzyskania VAT
${WcaOUzasadnienieBrakuOdzyskaniaVAT XPath}    //*[@id='modal']//*[@value='uzasadnienieBrakuOdzyskaniaVAT']    # Uzasadnienie braku możliwości odzyskania VAT
${WcaOUdzialWydatkowNaDzialanoscBR XPath}    //*[@id='modal']//*[@value='udzialWydatkowNaDzialanoscBR']    # Udział wydatków na działalność B+R w działalności gospodarczej Wnioskodawcy w ciągu ostatnich, zamkniętych 3 lat obrachunkowych (%)
${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBR XPath}    //*[@id='modal']//*[@value='metodologiaOkreslaniaWydatkowNaDzialalnoscBR']    # Metodologia określania wysokości wydatków na działalność
${WcaOIloscPatenty XPath}    //*[@id='modal']//*[@value='iloscPatenty']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Patenty
${WcaOIloscPrawoOchronneNaWzorPrzemyslowy XPath}    //*[@id='modal']//*[@value='iloscPrawoOchronneNaWzorPrzemyslowy']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Prawo ochronne na wzór użytkowy
${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowego XPath}    //*[@id='modal']//*[@value='iloscPrawoZRejestracjiWzoruPrzemyslowego']    # Liczba posiadanych przez Wnioskodawcę praw ochronnych na dzień składania wniosku o dofinansowanie (szt.): Prawo z rejestracji wzoru przemysłowego:
${WcaORodzajOsrodkaInnowacji XPath}    //*[@id='modal']//*[@value='rodzajOsrodkaInnowacji']    # Rodzaj ośrodka innowacji
${WcaOSiedziba XPath}    //*[@id='modal']//*[@value='siedziba']    # Adres siedziby/miejsca zamieszkania Wnioskodawcy
${WcaOAdresKrajPl XPath}    //*[@id='modal']//*[@value='adres.krajPl']    # Kraj - Polska
${WcaOAdresKrajSf2 XPath}    //*[@id='modal']//*[@value='adres.krajSf2']    # Kraj zagranica
${WcaOAdresKodPocztowy XPath}    //*[@id='modal']//*[@value='adres.kodPocztowy']    # Kod pocztowy
${WcaOAdresPoczta XPath}    //*[@id='modal']//*[@value='adres.poczta']    # Poczta
${WcaOAdresMiejscowosc XPath}    //*[@id='modal']//*[@value='adres.miejscowosc']    # (NIE UZYWAĆ) Miejscowość
${WcaOAdresMiejscowoscMirId XPath}    //*[@id='modal']//*[@value='adres.miejscowoscMirId']    # Miejscowość - lista rozwijalna
${WcaOAdresMiejscowoscZagranica XPath}    //*[@id='modal']//*[@value='adres.miejscowoscZagranica']    # Miejscowość zagranica
${WcaOAdresUlica XPath}    //*[@id='modal']//*[@value='adres.ulica']    # (NIE UŻYWAĆ) Ulica
${WcaOAdresUlicaMirId XPath}    //*[@id='modal']//*[@value='adres.ulicaMirId']    # Ulica - lista rozwijalna
${WcaOAdresNrBudynku XPath}    //*[@id='modal']//*[@value='adres.nrBudynku']    # Nr budynku
${WcaOAdresNrLokalu XPath}    //*[@id='modal']//*[@value='adres.nrLokalu']    # Nr lokalu
${WcaOAdresUlicaZagranica XPath}    //*[@id='modal']//*[@value='adres.ulicaZagranica']    # Ulica zagranica
${WcaOAdresTelefon XPath}    //*[@id='modal']//*[@value='adres.telefon']    # Telefon
${WcaOAdresFaks XPath}    //*[@id='modal']//*[@value='adres.faks']    # Faks
${WcaOAdresEmail XPath}    //*[@id='modal']//*[@value='adres.email']    # E-mail
${WcaOAdresStronaWww XPath}    //*[@id='modal']//*[@value='adres.stronaWww']    # Adres WWW
${WcaOHistoriaWnioskodawcy XPath}    //*[@id='modal']//*[@value='historiaWnioskodawcy']    # Historia wnioskodawcy oraz przedmiot działalności w kontekście projektu
${WcaOMiejsceNaRynku XPath}    //*[@id='modal']//*[@value='miejsceNaRynku']    # Miejsce na rynku
${WcaOCharakterystykaRynku XPath}    //*[@id='modal']//*[@value='charakterystykaRynku']    # Charakterystyka rynku
${WcaOOczekiwaniaPotrzebyKlientow XPath}    //*[@id='modal']//*[@value='oczekiwaniaPotrzebyKlientow']    # Oczekiwania i potrzeby klientów
${WcaOCharakterPopytu XPath}    //*[@id='modal']//*[@value='charakterPopytu']    # Charakter popytu
${WcaOWielkoscZatrudnienia XPath}    //*[@id='modal']//*[@value='wielkoscZatrudnienia']    # Wielkość zatrudnienia
${WcaOPrzychodyZeSprzedazyOstatniRok XPath}    //*[@id='modal']//*[@value='przychodyZeSprzedazyOstatniRok']    # Przychody ze sprzedaży w ostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagraniczna XPath}    //*[@id='modal']//*[@value='przychodyZeSprzedazyOstatniRokSprzedazZagraniczna']    # Przychody ze sprzedaży w ostatnim zamkniętym roku obrotowym, w tym przychody ze sprzedaży zagranicznej
${WcaOProcentZeSprzedazyOstatniRokSprzedazZagraniczna XPath}    //*[@id='modal']//*[@value='procentZeSprzedazyOstatniRokSprzedazZagraniczna']    # Przychody ze sprzedaży zagranicznej w ostatnim zamkniętym roku obrotowym jako % sumy ze sprzedaży w ostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyPrzedostatniRok XPath}    //*[@id='modal']//*[@value='przychodyZeSprzedazyPrzedostatniRok']    # Przychody ze sprzedaży w przedostatnim zamkniętym roku obrotowym
${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok XPath}    //*[@id='modal']//*[@value='przychodyZeSprzedazyPoprzedazjacyPrzedostatniRok']    # Przychody ze sprzedaży w roku obrotowym poprzedzającym przedostatni zamknięty rok obrotowy
${WcaOOpisProwadzonejDzialalnosci XPath}    //*[@id='modal']//*[@value='opisProwadzonejDzialalnosci']    # Opis prowadzonej działalności
${WcaOOfertaWnioskodawcy XPath}    //*[@id='modal']//*[@value='ofertaWnioskodawcy']    # Oferta wnioskodawcy
${WcaOOczekiwaniaIPotrzebyOdbiorcow XPath}    //*[@id='modal']//*[@value='oczekiwaniaIPotrzebyOdbiorcow']    # Oczekawnia i potrzeby odbiorców
${WcaODoswiadczenieWeWzornictwie XPath}    //*[@id='modal']//*[@value='doswiadczenieWeWzornictwie']    # Doświadczenie we wzornictwie
${WcaOWspolnicy XPath}    //*[@id='modal']//*[@value='wspolnicy']    # Wspólnicy
${WcaOWspolnicyUlica XPath}    //*[@id='modal']//*[@value='wspolnicy.ulica']    # Ulica (wspólnicy)
${WcaOWspolnicyNrBudynku XPath}    //*[@id='modal']//*[@value='wspolnicy.nrBudynku']    # Nr budynku (wspólnicy)
${WcaOWspolnicyNrLokalu XPath}    //*[@id='modal']//*[@value='wspolnicy.nrLokalu']    # Nr lokalu (wspólnicy)
${WcaOWspolnicyKodPocztowy XPath}    //*[@id='modal']//*[@value='wspolnicy.kodPocztowy']    # Kod pocztowy (wspólnicy)
${WcaOWspolnicyPoczta XPath}    //*[@id='modal']//*[@value='wspolnicy.poczta']    # Poczta (wspólnicy)
${WcaOWspolnicyMiejscowosc XPath}    //*[@id='modal']//*[@value='wspolnicy.miejscowosc']    # Miejscowość (wspólnicy)
${WcaOWspolnicyTelefon XPath}    //*[@id='modal']//*[@value='wspolnicy.telefon']    # Telefon (wspólnicy)
${WcaOWspolnicyFaks XPath}    //*[@id='modal']//*[@value='wspolnicy.faks']    # Faks (wspólnicy)
${WcaOWspolnicyEmail XPath}    //*[@id='modal']//*[@value='wspolnicy.email']    # E-mail (wspólnicy)
${WcaOWspolnicyStronaWww XPath}    //*[@id='modal']//*[@value='wspolnicy.stronaWww']    # Adres WWW (wspólnicy)
${WcaOProduktyPodlegajaceInternacjonalizacji XPath}    //*[@id='modal']//*[@value='produktyPodlegajaceInternacjonalizacji']    # Produkty, które zdaniem Wnioskodawcy mogą podlegać internacjonalizacji
${WcaOProduktyPodlegajaceInternacjonalizacjiNazwa XPath}    //*[@id='modal']//*[@value='nazwaProduktu']    # Nazwa i charakterystyka produktu
${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnCzyDotyczy XPath}    //*[@id='modal']//*[@value='czyDotyczyKodCn']    # Kod CN wyrobu - pole czy dotyczy
${WcaOProduktyPodlegajaceInternacjonalizacjiKodCn XPath}    //*[@id='modal']//*[@value='kodCn']    # Kod CN wyrobu
${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiuCzyDotyczy XPath}    //*[@id='modal']//*[@value='czyDotyczyKodPKWiU2015Uslugi']    # Kod PKWiU 2015 usługi - pole czy dotyczy
${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiu XPath}    //*[@id='modal']//*[@value='kodPKWiU2015uslugi']    # Kod PKWiU 2015 usługi
${WcaOProduktyPodlegajaceInternacjonalizacjiInformacje XPath}    //*[@id='modal']//*[@value='informacje']    # Informacje na temat sprzedaży, wytwarzania i rozwoju produktu
${WcaOProduktyPodlegajaceInternacjonalizacjiRynki XPath}    //*[@id='modal']//*[@value='rynki']    # Rynki (kraje), na których Wnioskodawca prowadzi lub prowadził sprzedaż produktu
${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiDocelowe XPath}    //*[@id='modal']//*[@value='rynkiDocelowe']    # Rynki docelowe wskazane w modelu biznesowym internacjonalizacji
${WcaORynkiZagraniczne XPath}    //*[@id='modal']//*[@value='rynkiZagraniczne']    # Rynki zagraniczne (kraje), na których Wnioskodawca prowadzi sprzedaż pozostałych produktów
${WcaOCzyDotyczyRynkiZagraniczne XPath}    //*[@id='modal']//*[@value='czyDotyczyRynkiZagraniczne']    # Rynki zagraniczne (kraje), na których Wnioskodawca prowadzi sprzedaż pozostałych produktów - pole czy dotyczy
