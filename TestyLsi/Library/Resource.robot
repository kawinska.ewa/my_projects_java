*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary
Resource          ../Library/LogowanieUI.robot    # Logowanie elementy interface
Resource          ../Library/TrwajaceNaboryUI.robot
Resource          ../Library/WniosekUI.robot
Resource          ../Library/AdministracjaUI.robot
Resource          ../Library/WersjeElementowEdutujUI.robot
Resource          ../Library/KonfiguracjaSrodowiska.robot
Resource          ../Library/Dane_do_logowania.robot

*** Variables ***
${HOMEPAGE}       https://lsi1420-maw.parp.gov.pl    # https://lsi1420-lab.parp.gov.pl LUB https://lsi1420-test.parp.gov.pl LUB https://lsi1420-php7.parp.gov.pl
${BROWSER}        chrome    # Firefox Chrome
${BrowserMode}    Default    # Możliwe wartości to Default, Grid i Headless
${DELAY}          0
${LocalIEDriver}    C:${/}Python27${/}IEDriverServer.exe    # Ścieżka do web drivera IE. Wymagane, żeby testować na IE.

*** Keywords ***
Otworz Przegladarke Na Stronie Logowania
    Run Keyword If    '${BROWSER}'=='ie'    Zmienne Srodowiskowe Dla IE
    Run Keyword If    '${BrowserMode}'=='Default'    Otworz Przegladarke Na Stronie Logowania Default
    Run Keyword If    '${BrowserMode}'=='Grid'    Otworz Przegladarke Na Stronie Logowania Grid
    Run Keyword If    '${BrowserMode}'=='Headless'    Otworz Przegladarke Na Stronie Logowania Headless
    Run Keyword If    '${BrowserMode}'=='Proxy'    Otworz Przegladarke Na Stronie Logowania Proxy
    Set Selenium Speed    ${DELAY}
    Strona Logowania Jest Widoczna
    Ukryj Cookie Info

Otworz Przegladarke Na Stronie Logowania Default
    Open Browser    ${HOMEPAGE}    ${BROWSER}
    Maximize Browser Window

Otworz Przegladarke Na Stronie Logowania Grid
    Open Browser    ${HOMEPAGE}    ${BROWSER}    None    http://10.10.6.110:5555/wd/hub
    Maximize Browser Window

Otworz Przegladarke Na Stronie Logowania Headless
    Start Virtual Display    1920    1080
    Open Browser    ${HOMEPAGE}    ${BROWSER}
    Set Window Size    1920    1080

Otworz Przegladarke Na Stronie Logowania Proxy
    ${proxy}=    Evaluate    sys.modules['selenium.webdriver'].Proxy()    sys, selenium.webdriver
    ${proxy.http_proxy}=    Set Variable    127.0.0.1:8888
    Create Webdriver    Firefox    proxy=${proxy}
    Go To    ${HOMEPAGE}
    Maximize Browser Window

Zmienne Srodowiskowe Dla IE
    [Documentation]    Wymagane do testów na IE, bez tego nie działa
    ...    http://stackoverflow.com/questions/20140493/ie-browser-nor-working-with-robot-framework
    ...    http://heliumhq.com/docs/internet_explorer
    ...    http://www.abodeqa.com/2014/11/26/challenges-to-run-selenium-webdriver-scripts-in-ie-browser/
    ...    W ustawieniach, w karcie Zabezpieczenia trzeba włączyć (albo wyłączyć) tryb chroniony dla wszystkich stref:
    ...    http://www.abodeqa.com/2013/05/25/unexpected-error-launching-internet-explorer-protected-mode-must-be-set-to-the-same-value/
    Set Environment Variable    webdriver.ie.driver    ${LocalIEDriver}

Strona Logowania Jest Widoczna
    Wait Until Element Is Visible    ${LoginButton Id}    30s

Zaloguj
    [Arguments]    ${Login}    ${Password}
    [Documentation]    *Zaloguj i sprawdz rezultat*
    ...    Krok 1: Wpisz Nazwę użytkownika
    ...    Krok 2: Wpisz Hasło
    ...    Krok 3: Kliknij przycisk Zaloguj
    Input Text    ${LoginUserName Id}    ${Login}
    Input Password    ${LoginPassword Id}    ${Password}
    Click Element    ${LoginButton Id}

Wyloguj
    [Documentation]    *Wyloguj* Krok 1: Sprawdź, czy widoczne jest menu Więcej opcji
    ...    Krok 2: Kiedy będzie to możliwe, kliknij menu Więcej opcji
    ...    Krok 3: Sprawdź, czy widoczny jest link Wyloguj
    ...    Krok 4: Kliknij menu Wyloguj
    ...    Krok 5: Na stronie powinien być przycisk Zaloguj
    Set Focus To Element    ${WiecejOpcjiButton Id}
    Wait Until Element Is Visible    ${WiecejOpcjiButton Id}    30s
    Wait Until Keyword Succeeds    5s    1s    Click Element    ${WiecejOpcjiButton Id}
    Wait Until Element Is Visible    ${LogoutLink Id}    5s
    Click Element    ${LogoutLink Id}
    Page Should Contain Element    ${LoginButton Id}

Logowanie Powiodlo Sie
    [Documentation]    Krok 1: Strona powinna zawierać menu Wnioski
    Wait Until Element Is Visible    ${MenuKonkursy Id}    30s

Logowanie Nie Powiodlo Sie
    [Arguments]    ${Login}    ${Password}
    [Documentation]    *Logowanie Nie Powiodlo Sie*: Na stronie widoczny alert "Niepoprawne dane uwierzytelniania"
    ...    Jeżeli Nazwa Użytkownika była pusta to na stronie powinna być widoczna informacja "Nazwa użytkownika jest wymagana" w elemencie //div[@class='label label-danger']
    ...    Jeżeli Hasło było puste to na stronie powinna być widoczna informacja "Hasło jest wymagane" w elemencie //div[@class='label label-danger']
    ...    Jeżeli Nazwa Użytkownika i Hasło były puste to na stronie widoczne są dwa elementy [@class='label label-danger']
    Wait Until Element Is Visible    ${BledneDaneLogowaniaAlert CSS}    30s
    Element Text Should Be    ${BledneDaneLogowaniaAlert CSS}    ${BledneDaneLogowaniaKomunikat}
    Run Keyword If    '${Login}'=='${EMPTY}' and '${Password}'=='${EMPTY}'    Login I Haslo Sa Wymagane
    ...    ELSE IF    '${Login}'=='${EMPTY}'    Login Jest Wymagany
    ...    ELSE IF    '${Password}'=='${EMPTY}'    Haslo Jest Wymagane

Idz Do Strony Logowania
    Go To    ${HOMEPAGE}
    Strona Logowania Jest Widoczna

Login Jest Wymagany
    Page Should Contain Element    ${JestWymaganeAlert CSS}
    Element Should Contain    ${JestWymaganeAlert CSS}    ${LoginJestWymaganyKomunikat}

Haslo Jest Wymagane
    Page Should Contain Element    ${JestWymaganeAlert CSS}
    Element Should Contain    ${JestWymaganeAlert CSS}    ${HasloJestWymaganeKomunikat}

Login I Haslo Sa Wymagane
    Locator Should Match X Times    ${JestWymaganeAlert CSS}    2
    Element Should Contain    ${JestWymaganeAlert CSS}    ${LoginJestWymaganyKomunikat}
    Page Should Contain    ${HasloJestWymaganeKomunikat}

Utworz Wniosek
    [Arguments]    ${AdresNaboru}
    [Documentation]    *Przyjmuje XPatha z adresem naboru Zwraca Id utworzonego wniosku*
    ...    Krok 1: Na stronie powinien być przycisk "Utwórz wniosek" dla konkursu, który został wybrany do testów
    ...    Krok 2: Kliknij przycisk "Utwórz wniosek" dla konkursu, który został wybrany do testów
    ...    Krok 3: Adres strony pownien zawierać fragment "/wniosek/edytuj/"
    ...    Krok 4: Strona powinna zawierać alert "Pomyślnie utworzono wniosek"
    ...    Krok 5: Pobierz adres utworzonego wniosku
    ...    Krok 6: Wytnij z adresu Id utworzonego wniosku
    Page Should Contain Element    ${AdresNaboru}
    Click Element    ${AdresNaboru}
    Location Should Contain    ${AdresWnioskuEdycja}
    Element Should Contain    ${AlertSuccess CSS}    ${AlertUtworzono}
    ${AdresUtworzonegoWniosku}    Get Location
    ${IdUtworzonegoWniosku}    Fetch From Right    ${AdresUtworzonegoWniosku}    /
    [Return]    ${IdUtworzonegoWniosku}

Usun Wniosek
    [Arguments]    ${IdWniosku}
    [Documentation]    Przyjmuje Id wniosku do usunięcia
    ...    Krok 1: Wstaw Id wniosku do adresu usunięcia
    ...    Krok 2: Filtruj listę wniosków dla podanego Id wniosku
    ...    Krok 3: Strona powinna zawierać link do adresu usunięcia
    ...    Krok 4: Kliknij przycisk Usuń
    ...    Krok 5: Czekaj, aż widoczny będzie modal
    ...    Krok 6: Czekaj, aż widoczny bedzie nagłówek w modalu
    ...    Krok 7: Nagłówek w modalu powinien zaierać tekst "Usunięcie wniosku"
    ...    Krok 8: Czekaj, aż widoczny będzie przycisk Usuń wniosek
    ...    Krok 9: Kliknij przycisk Usuń wniosek
    ...    Krok 10: Strona nie powinna zawierać linku do adresu usunięcia
    ...    Krok 11: Łączenie komunikatu z alertu informującego o pomyślnym usunięciu z Id wniosku
    ...    Ktok 12: Alert powinien zawierać komunikat o pomyślnym usunięciu pliku
    ${AdresPrzyciskuUsun}    Catenate    SEPARATOR=    ${AdresUsunWniosek}    ${IdWniosku}
    Filtruj Id Wniosku    ${IdWniosku}
    Wait Until Keyword Succeeds    10s    1s    Page Should Contain Link    ${AdresPrzyciskuUsun}
    Click Link    ${AdresPrzyciskuUsun}
    Wait Until Element Is Visible    ${ModalUsun Id}
    Wait Until Element Is Visible    ${ModalUsunH1 CSS}
    Element Should Contain    ${ModalUsunH1 CSS}    ${ModalUsunH1Tresc}
    Wait Until Element Is Visible    ${ModalUsunPrzycisk XPath}
    Click Element    ${ModalUsunPrzycisk XPath}
    Page Should Not Contain Link    ${AdresPrzyciskuUsun}
    ${AlertUsunieto}    Catenate    ${AlertUsunieto}    ${IdWniosku}
    Element Should Contain    ${AlertSuccess CSS}    ${AlertUsunieto}

Idz Do Strony Lista Konkursow
    ${adres}    Catenate    SEPARATOR=    ${HOMEPAGE}    ${AdresListaKonkursow}
    Go To    ${adres}

Edytuj Wniosek
    [Arguments]    ${IdWniosku}
    [Documentation]    Przyjmuje Id wniosku do edycji
    ...    Krok 1: Wstaw Id wniosku do adresu edycji
    ...    Krok 2: Filtruj listę wniosków dla podanego Id wniosku
    ...    Krok 3: Strona powinna zawierać link do adresu edycji
    ...    Krok 4: Kliknij przycisk Edycja
    ...    Krok 5: Adres strony pownien zawierać wskazany fragment
    ${AdresWnioskuEdycja}    Catenate    SEPARATOR=    ${AdresWnioskuEdycja}    ${IdWniosku}
    Filtruj Id Wniosku    ${IdWniosku}
    Wait Until Keyword Succeeds    20s    1s    Page Should Contain Link    ${AdresWnioskuEdycja}
    Click Link    ${AdresWnioskuEdycja}
    Wait Until Keyword Succeeds    20s    1s    Location Should Contain    ${AdresWnioskuEdycja}

Java Script Dziala
    [Documentation]    Na stronie edycji wniosku:
    ...    Krok 1: Czekaj, aż widoczne będzie menu nawigacyjne po lewej
    ...    Krok 2: Pobierz współrzędną Y położenia strony
    ...    Krok 3: Kliknij ostatni link w menu nawigacyjnym
    ...    Krok 4: Czekaj 2 sekundy
    ...    Krok 5: Pobierz współrzędną Y położenia strony
    ...    Krok 6: Obie pobrane współrzedne Y powinny być różne od siebie
    ...    Krok 7: Kliknij ostatni link w menu nawigacyjnym
    Wait Until Element Is Visible    ${MenuNawigacja Id}
    ${Y}    Execute JavaScript    return window.pageYOffset
    Comment    ${MNOstatniLink Y}    Get Horizontal Position    ${MNOstatniLink XPath}
    Comment    ${NavigatorMenu_width}    ${NavigatorMenu_height}    Get Element Size    //*[@id="navigator_menu"]
    Comment    Execute Javascript    window.scroll(0,-${MNOstatniLink Y} + 100)    # przesuwa stronę o 100 px ponad ostatniąpozycję w menu
    Comment    Execute Javascript    document.getElementById('${MNOstatniLink XPath}').scrollIntoView();
    Comment    Execute javascript    document.body.style.zoom="30%"    \    # FIX_IT Element is not clickable at point pomimo ze jest widoczny
    Comment    Sleep    2s
    Comment    Click Element    ${MNOstatniLink XPath}    # FIX_IT Element is not clickable at point
    Comment    Execute javascript    document.body.style.zoom="0%"    # reset zoom
    Sleep    3s
    ${NoweY}    Execute JavaScript    return window.pageYOffset
    Comment    Should Not Be Equal    ${Y}    ${NoweY}    # Fails if the given objects are equal.
    Click Element    ${MNPierwszyLink XPath}

Czekaj Na Zakonczenie Ajax
    [Documentation]    Sprawdza w pętli czy działanie Ajax zakończyło się. Jeśli tak, wychodzi z pętli.
    : FOR    ${INDEX}    IN RANGE    1    1000
    \    ${IsAjaxComplete}    Execute JavaScript    return window.jQuery!=undefined && jQuery.active==0
    \    Log    ${INDEX}
    \    Log    ${IsAjaxComplete}
    \    Run Keyword If    ${IsAjaxComplete}==True    Exit For Loop

Zapis Danych Dziala
    [Documentation]    Krok 1: Wprowadź pusty ciąg znaków do pola Tytuł projektu
    ...    Krok 2: Zapisz dane i odśwież stronę
    ...    Krok 3: Pobierz wartośc z pola Tytuł projektu
    ...    Krok 4: Wartość z pola Tytuł projektu powinna być pustym ciągiem znaków
    ...    Krok 5: Wypełnij pole Tytuł projektu losowym tekstem o długości 1000 znaków (limit pola Tytuł projektu)
    ...    Krok 6: Zapisz dane i odśwież stronę
    ...    Krok 7: Pobierz wartośc z pola Tytuł projektu
    ...    Krok 8: Wartość z pola Tytuł projektu powinna być równa wylosowanemu wcześniej tekstowi
    ...    Krok 9: Pobierz pierwszą linię wylosowanego tekstu
    ...    Krok 10: Utwórz zapytanie do bazy sprawdzające, czy w bazie jest pole tytul_projektu o wartości zaczynającej się od pierwszej linii wylosowanego tekstu (postgres nie zwraca wyniku, gdy w WHERE jest string z new line)
    ...    Krok 11: Zapisz utworzone zapytanie
    ...    Krok 12: Sprawdź czy dane z pola Tytuł projektu zostały zapisane w bazie
    Input Text    ${TytulProjektu Id}    ${EMPTY}
    Zapisz Dane I Przeladuj
    ${TytulProjektuWartosc}    Get Value    ${TytulProjektu Id}
    Should Be Equal    ${TytulProjektuWartosc}    ${EMPTY}
    ${Tresc}    Wypelnij Pole Losowym Tekstem    ${TytulProjektu Id}    1000    Faker
    Zapisz Dane I Przeladuj
    ${TytulProjektuWartosc}    Get Value    ${TytulProjektu Id}
    Should Be Equal    ${TytulProjektuWartosc}    ${Tresc}
    ${Tresc}    Get Line    ${Tresc}    0
    ${Zapytanie}    Catenate    SEPARATOR=    SELECT id FROM wnioski.wnioski_ogolne_projekt WHERE tytul_projektu LIKE '    ${Tresc}    %';
    Log    ${Zapytanie}
    Sprawdz Czy Jest W Bazie    ${Zapytanie}

Zapisz Dane I Przeladuj
    [Documentation]    Krok 1: Kliknij przycisk Zapisz.
    ...    Krok 2: Czekaj, aż przycisk zapisz będzie ponownie widoczny (max 30s). Na czas zapisu przycisk znika.
    ...    Krok 3: Przeładuj stronę.
    Click Element    ${ZapiszWniosek Id}
    Wait Until Element Is Visible    ${ZapiszWniosek Id}    30s
    Reload Page

Zapisz Dane
    [Documentation]    Krok 1: Kliknij przycisk Zapisz.
    ...    Krok 2: Czekaj, aż przycisk zapisz będzie ponownie widoczny (max 30s). Na czas zapisu przycisk znika.
    Click Element    ${ZapiszWniosek Id}
    Wait Until Element Is Visible    ${ZapiszWniosek Id}    30s

Pobierz Parametr Z Pliku
    [Arguments]    ${SciezkaDoPliku}    ${SzukanyParametr}
    [Documentation]    Pobiera szukany parametr ze wskazanego pliku i zwraca jego wartość
    ...    Krok 1: Znajdź i pobierz szukany paramert ze wskazango pliku
    ...    Krok 2: Oczyść wartość parametru z jego nazwy
    ${Wartosc}    Grep File    ${SciezkaDoPliku}    ${SzukanyParametr}
    ${Wartosc}    Fetch From Right    ${Wartosc}    :${SPACE}
    [Return]    ${Wartosc}

Utworz Zmienna DaneBazy
    [Documentation]    Krok 1: Sprawdź czy nie istnieje zdefiniowana zmienna DaneBazy - mogła zostać wprowadzona z linii poleceń
    ...    Krok 2: Sprawdź czy istnieje plik NoGitLabLibrary/Dane_do_bazy_danych.txt
    ...    Krok 3: Jeżeli zmienna nie istnieje i plik istnieje, to go zaimportuj,
    ...    Krok 4: Jeżeli zmienna nie istnieje i plik nie istnieje, to utwórz zmienną z pliku parameters.yml
    ${CzyNieIstniejeZmienna}    Run Keyword And Return Status    Variable Should Not Exist    ${DaneBazy}
    ${CzyPlikIstniejePlik}    Run Keyword And Return Status    File Should Exist    NoGitLabLibrary/Dane_do_bazy_danych.txt
    Run Keyword If    ${CzyNieIstniejeZmienna} and ${CzyPlikIstniejePlik}    Import Resource    ${CURDIR}/../NoGitLabLibrary/Dane_do_bazy_danych.txt
    Log    ${CURDIR}/../NoGitLabLibrary/Dane_do_bazy_danych.txt
    Run Keyword If    ${CzyNieIstniejeZmienna} and ${CzyPlikIstniejePlik} == 0    Utworz Zmienna DaneBazy Z Yml

Utworz Zmienna DaneBazy Z Yml
    [Documentation]    Tworzy zmienną DaneBazy z pliku app/config/parameters.yml
    ${DaneBazy}    Set Variable    database='{database}', user='{user}', password='{password}', host='{host}.parp.gov.pl', port={port}
    ${Host}    Pobierz Parametr Z Pliku    ${ParametersSciezka}    database_host
    ${DaneBazy}    Replace String    ${DaneBazy}    {host}    ${Host}
    ${Port}    Pobierz Parametr Z Pliku    ${ParametersSciezka}    database_port
    ${DaneBazy}    Replace String    ${DaneBazy}    {port}    ${Port}
    ${Database}    Pobierz Parametr Z Pliku    ${ParametersSciezka}    database_name
    ${DaneBazy}    Replace String    ${DaneBazy}    {database}    ${Database}
    ${User}    Pobierz Parametr Z Pliku    ${ParametersSciezka}    database_user
    ${DaneBazy}    Replace String    ${DaneBazy}    {user}    ${User}
    ${Password}    Pobierz Parametr Z Pliku    ${ParametersSciezka}    database_password
    ${DaneBazy}    Replace String    ${DaneBazy}    {password}    ${Password}
    Set Test Variable    ${DaneBazy}

Sprawdz Czy Jest W Bazie
    [Arguments]    ${Zapytanie}
    [Documentation]    Check If Exists In Database nie działa dla zapytań mających w WHERE ciąg znaków z BR
    ...    Krok 1: Połącz z bazą danych
    ...    Krok 2: Sprawdź czy zapytanie zwraca jakąś wartość
    ...    Krok 3: Rozłącz z bazą danych
    Utworz Zmienna DaneBazy
    Connect To Database Using Custom Params    ${dbapiModuleName}    ${DaneBazy}
    Check If Exists In Database    ${Zapytanie}
    Disconnect From Database

Odpytaj Baze Danych
    [Arguments]    ${Zapytanie}
    [Documentation]    *Dla pojedyńczego pola z bazy*
    ...    Krok 1: Połącz z bazą danych
    ...    Krok 2: Uruchom zapytanie SELECT i zwróć wynik
    ...    Krok 3: Rozłącz z bazą danych
    Utworz Zmienna DaneBazy
    Connect To Database Using Custom Params    ${dbapiModuleName}    ${DaneBazy}
    @{WynikZapytania}    Query    ${Zapytanie}
    Disconnect From Database
    ${DlugoscListy}    Get Length    ${WynikZapytania}
    : FOR    ${i}    IN RANGE    ${DlugoscListy}
    \    ${Element}    Remove From List    ${WynikZapytania}    ${i}
    \    ${Element}    Catenate    ${Element}
    \    ${Element}    Remove String Using Regexp    ${Element}    (^\\(|,\\)$)
    \    ${Element}    Remove String Using Regexp    ${Element}    (^'|'$)
    \    ${Element}    Zwroc Polskie Znaki    ${Element}
    \    Insert Into List    ${WynikZapytania}    ${i}    ${Element}
    [Return]    @{WynikZapytania}

Wykonaj Zapytanie Na Bazie Danych
    [Arguments]    ${Zapytanie}
    [Documentation]    Krok 1: Połącz z bazą danych
    ...    Krok 2: Wykonaj zapytanie na bazie danych
    ...    Krok 3: Rozłącz z bazą danych
    Utworz Zmienna DaneBazy
    Connect To Database Using Custom Params    ${dbapiModuleName}    ${DaneBazy}
    Execute Sql String    ${Zapytanie}
    Disconnect From Database

Walidacja Dziala
    [Documentation]    Krok 1: Kliknij przycisk Sprawdź poprawność
    ...    Krok 2: Czekaj, aż będą widoczne elementy listy modala z walidacją
    ...    Krok 3: Naciśnij Escape
    ...    Krok 4: Czekaj, aż nie będą widoczne elementy listy modala z walidacją
    Click Element    ${SprawdzPoprawnosc Id}
    Wait Until Element Is Visible    ${ElementListyWalidacja Id}    10s
    Press Key    ${ModalWalidacja Id}    \\27    #ASCII code dla Escape
    Wait Until Element Is Not Visible    ${ElementListyWalidacja Id}

Filtruj Id Wniosku
    [Arguments]    ${IdWniosku}
    [Documentation]    Krok 1: Wprowadź Id wniosku do pola w filtrze
    ...    Krok 2: Naciśnij Enter
    Wait Until Element Is Visible    ${FiltrIdWniosku Id}    5s
    Input Text    ${FiltrIdWniosku Id}    ${IdWniosku}
    Press Key    ${FiltrIdWniosku Id}    \\13    #ASCII code dla Enter
    Click Button    ${FiltrFiltrujPrzycisk XPath}    # Alternatywne zatwierdzenie filtrowania

Ukryj Cookie Info
    [Documentation]    Krok 1: Sprawdź czy widoczny jest przycisk "Nie pokazuj więcej tej informacji" na cookie info
    ...    Krok 2: Jeżeli przycisk jest widoczny, to kliknij go
    ${CzyCookieInfoWidoczne}    Run Keyword And Return Status    Element Should Be Visible    ${UkryjCookieInfoButton Id}
    Run Keyword If    ${CzyCookieInfoWidoczne}    Click Button    ${UkryjCookieInfoButton Id}

Logowanie Admin Powiodlo Sie
    [Documentation]    Krok 1: Adres strony powinien zawierać ciąg znaków "administracja"
    Location Should Contain    ${AdresAdminstracja}

Pobierz Dane Do Logowania
    [Documentation]    User keyword konieczny, bo dane z logowania jako pracownik PARP nie powinny znaleźć sie w logu, a pybot umożliwia ukrycie tylko keyword, a nie zmiennych
    ...    *W związku z koniecznością ukrycia hasła uzytkownika PARP, user keyword należy uruchamiać z prarametrami --outputdir TestResult --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania*
    ${Login}    Get Value From User    Podaj Login użytkownika PARP    marcin_laskowski
    ${Password}    Get Value From User    Podaj hasło    hidden=yes
    [Return]    ${Login}    ${Password}

Otworz Liste Wzorow Wniosku
    [Documentation]    Krok 1: Czekaj, aż widoczne będzie menu Nabór
    ...    Krok 2: Kliknij menu Nabór
    ...    Krok 3: Czekaj, aż widoczne będzie menu Nabór/Wzory wniosków
    ...    Krok 4: Kliknij menu Wzory wniosków
    ...    Krok 5: Adres powinien zawierać administracja/wersje_elementow
    Wait Until Element Is Visible    ${MenuNabor Id}    30s
    Click Element    ${MenuNabor Id}
    Wait Until Element Is Visible    ${MenuWzoryWnioskow Id}    5s
    Click Element    ${MenuWzoryWnioskow Id}
    Location Should Contain    ${AdresWersjeElementow}

Filtruj Wzory Wniosku
    [Arguments]    ${NrDzialania}    ${NrWersji}
    [Documentation]    Krok 1: Wprowadź Działanie do pola w filtrze
    ...    Krok 2: Wprowadź Numer wersji do pola w filtrze
    ...    Krok 3: Naciśnij Enter
    Select From List By Label    ${ListaWersjeElementow_NumerWersji_OperatorFiltra}    Równe
    Input Text    ${FiltrDzialanie Id}    ${NrDzialania}
    Input Text    ${FiltrNumerWersji Id}    ${NrWersji}
    Press Key    ${FiltrNumerWersji Id}    \\13    #ASCII code dla Enter

Kliknij Przycisk
    [Arguments]    ${Przycisk XPath}
    [Documentation]    Krok 1: Czekaj, aż przycisk będzie widoczny
    ...    Krok 2: Sprawdź, czy przycisk zgodny z XPath jest widoczny tylko raz
    ...    Krok 3: Kliknij przycisk
    Wait Until Element Is Visible    ${Przycisk XPath}    10s
    Sleep    5s
    Comment    Xpath Should Match X Times    ${Przycisk XPath}    1    Powinien byc widoczny tylko jeden przycisk
    Page Should Contain Element    ${Przycisk XPath}    \    \    1
    Click Element    ${Przycisk XPath}
    Comment    ${aaa}=    Get Webelement    ${EdycjaPrzycisk XPath}
    Comment    Click Element    ${aaa}    # też nie dziala ${aaa}; znajduje niewłaściwy element

Widac Naglowek Tabeli Elementy
    [Arguments]    ${Tresc}
    [Documentation]    Krok 1: Czekaj, aż widoczny będzie nagłówek tabeli z elementami wniosku
    ...    Krok 2: Nagłówek tabeli z elementami wniosku zawiera treść
    Wait Until Element Is Visible    ${NaglowekTabeliElement XPath}
    Element Text Should Be    ${NaglowekTabeliElement XPath}    ${Tresc}

Pobierz Liczbe Wierszy Tabeli
    [Arguments]    ${Tabela XPath}    # XPath wskazujący na tabelę, dla której należy podać liczbę wierszy
    [Documentation]    Zwraca liczbę wierszy wskazanej tabeli
    ...    Krok 1: Zastąp w treści skryptu zmienną ${Tabela XPath} jej wartością
    ...    Krok 2: Wykonaj skrypt zliczający wiersze tabeli i wstaw uzyskaną wartość do zmiennej ${LiczbaWierszyTabeli}
    ...    Krok 3: Zapisz warość zmiennej ${LiczbaWierszyTabeli}
    ${Skrypt}    Replace Variables    return (document.evaluate( '${Tabela XPath}' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.rows.length);
    ${LiczbaWierszyTabeli}    Execute JavaScript    ${Skrypt}    # Skrypt zlicza wiersze tabeli wskazanej za pomocą XPath
    Log    ${LiczbaWierszyTabeli}
    [Return]    ${LiczbaWierszyTabeli}

Element Istnieje W Tabeli
    [Arguments]    ${ElementNazwa}    ${WystapienieNr}    ${Tabela XPath}
    [Documentation]    Zwraca numer wiersza z szukanym elementem występującym w tabeli po raz n-ty
    ...    Krok 1: Pobierz liczbę wierszy tabeli
    ...    Krok 2: Zapisz wartość zmiennej ${LiczbaWierszyTabeliZastosowaneElementy}
    ...    Krok 3: Konwertuj numer wystąpienia na liczbę całkowitą
    ...    Krok 4: Utwórz zmienną ${WierszZElementem} = 0. Zmienna służy do przekazania numeru wiersza z szukanym elementem. Wartośc 0 oznacza, że element nie został znaleziony.
    ...    Krok 5: Dodaj ciąg xpath= przed XPath tabeli, bo takiej składni wymaga Table Row Should Contain
    ...    Krok 6: Pętla FOR dla ${wiersz} od 1 do ${LiczbaWierszyTabeliZastosowaneElementy}
    ...    Krok 6.1: Zapisz wartość zmiennej ${wiersz}
    ...    Krok 6.2: Sprawdź czy w ${wiersz} tabeli istnieje poszukiwany teskt
    ...    Krok 6.3: Zapisz rezultat poszukiwania elementu w wierszu
    ...    Krok 6.4: Jezeli element itnieje w wierszu, to odejmij 1 od nr wystąpienia
    ...    Krok 6.5: Zapisz zmienną z nr wystąpienia
    ...    Krok 6.6: Jeżeli znaleziono tekst i numer wystąpienia = 0, to wstaw numer wiersza do zmiennej ${WierszZElementem}
    ...    Krok 6.7: Jeżeli znaleziono tekst i numer wystąpienia = 0, to wyjdź z pętli
    ${LiczbaWierszyTabeliZastosowaneElementy}    Pobierz Liczbe Wierszy Tabeli    ${Tabela XPath}
    Log    ${LiczbaWierszyTabeliZastosowaneElementy}
    ${WystapienieNr}    Convert To Integer    ${WystapienieNr}
    ${WierszZElementem}    Set Variable    0
    ${TabelaZXPath XPath}    Catenate    SEPARATOR=    xpath=    ${Tabela XPath}    #Dodaję ciąg xpath= przed XPath tabeli, bo takiej składni wymaga Table Row Should Contain
    : FOR    ${wiersz}    IN RANGE    1    ${LiczbaWierszyTabeliZastosowaneElementy}    1    #Table Row Schould contain pomija sprawdezanie nagłówka i ztopki tabeli
    \    Log    ${wiersz}
    \    ${ElementIstniejeWWierszu}    Run Keyword And Return Status    Table Row Should Contain    ${TabelaZXPath XPath}    ${wiersz}    ${ElementNazwa}
    \    Log    ${ElementIstniejeWWierszu}
    \    ${WystapienieNr}    Set Variable If    ${ElementIstniejeWWierszu}    ${WystapienieNr-1}    ${WystapienieNr}
    \    Log    ${WystapienieNr}
    \    ${WierszZElementem}    Set Variable If    ${ElementIstniejeWWierszu} and ${WystapienieNr} == 0    ${wiersz}
    \    Run Keyword If    ${ElementIstniejeWWierszu} and ${WystapienieNr} == 0    Exit For Loop
    [Return]    ${WierszZElementem}

Otworz Edycje Elementu
    [Arguments]    ${WierszTabeli}
    [Documentation]    Otwiera edycję elementu znajdującego się we wskazanym wierszu tabeli
    ...    Krok 1: Przekształć numer wiersza tabeli na string
    ...    Krok 2: Podstaw numer wiersza tabeli do XPath wskazującego na przycisk Konfiguruj
    ...    Krok 3: Czekaj, aż widoczny będzie przycisk Konfiguruj
    ...    Krok 4: Kliknij przycisk Konfiguruj
    ...    Krok 5: Czekaj, aż widoczny będzie modal z konfiguracją elementu
    ${WierszTabeli}    Convert To String    ${WierszTabeli}
    ${KonfigurujPrzycisk XPath}    Replace String    ${KonfigurujPrzycisk XPath}    {WierszTabeli}    ${WierszTabeli}
    ${KonfigurujPrzycisk id}    Get Element Attribute    ${KonfigurujPrzycisk XPath}@id
    Execute Javascript    document.getElementById('${KonfigurujPrzycisk id}').scrollIntoView();
    Execute JavaScript    window.scroll(0,window.pageYOffset - 100)    # przesuwa stronę o 100 px do góry, bo zlokalizowany przycisk jest zasłonięty przez menu @id=bs-example-navbar-collapse-1
    Comment    Focus    ${KonfigurujPrzycisk XPath}    # to nie działa - zatrzymuje się na 13 elemencie, który nie jest widoczny na stronie)
    Comment    Execute Javascript    "arguments[0].scrollIntoView()", ${KonfigurujPrzycisk XPath}    # to nie działa (przypuszczalnie nie tak używa sięJs'a)
    Wait Until Element Is Visible    ${KonfigurujPrzycisk XPath}
    Log    ${KonfigurujPrzycisk XPath}
    Comment    Execute Javascript    document.getElementById('${KonfigurujPrzycisk id}').scrollIntoView();
    Click Element    ${KonfigurujPrzycisk XPath}    # WebDriverException: Message: unknown error: Element <a href="/administracja/wersje_elementow/konfiguruj_element/1043/109" class="btn btn-xs btn-success" id="konfigurujDokument_1043" data-toggle="modal" data-target="#modal" data-id="1043">...</a> is not clickable at point (881, 11). Other element would receive the click: <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">...</div> (Session info: chrome=70.0.3538.77) (Driver info: chromedriver=2.43.600210 (68dcf5eebde37173d4027fa8635e332711d2874a),platform=Windows NT 6.1.7601 SP1 x86_64)
    Wait Until Element Is Visible    ${ModalKonfigurujTytul Id}

Zamknij Edycje Elementu
    [Documentation]    Krok 1: Nacisnij Escape
    ...    Krok 2: Czekaj, aż nie będzie widoczny modal z konfiguracją elementu
    Press Key    ${ModalKonfiguruj Id}    \\27    #ASCII code dla Escape
    Wait Until Element Is Not Visible    ${ModalKonfiguruj Id}

Pobierz Wartosc Z Listy
    [Arguments]    ${Lista Id}
    [Documentation]    Krok 1: Pobierz zaznaczoną wartość z listy
    ${WartoscWybranaZListy}    Get Selected List Label    ${Lista Id}
    [Return]    ${WartoscWybranaZListy}

Pobierz Klase Elementu
    [Arguments]    ${Element Id}
    [Documentation]    Krok 1: Dodaj do Id elementu ciąg "@class". Wymaga tego Get Element Attribute.
    ...    Krok 2: Pobierz klasę elementu
    ${Element Id}    Catenate    SEPARATOR=    ${Element Id}    @class
    ${Klasa}    Get Element Attribute    ${Element Id}
    [Return]    ${Klasa}

Sprawdz Checkbox I Zaznacz
    [Arguments]    ${Checkbox XPath}
    [Documentation]    Zwraca true/false czy chceckbox był zatwierdzony.
    ...    Krok 1: Sprawdź, czy wskazany checkbox jest zaznaczony
    ...    Krok 2: Zaznacz wskazany chceckbox
    ${CzyZaznaczony}    Run Keyword And Return Status    Checkbox Should Be Selected    ${Checkbox XPath}
    Select Checkbox    ${Checkbox XPath}
    [Return]    ${CzyZaznaczony}

Wszystkie Checkboxy Sa Zaznaczone
    [Arguments]    ${Checkbox XPath}
    [Documentation]    Sprawdza, czy wszystkie checkboxy zgodne z podanym XPath są zaznaczone
    ...    Krok 1: Pobierz liczbę checkbox-ów na stornie zgodnych z podanym XPath
    ...    Krok 2: Dla wiersza od 1 do ile:
    ...    Krok 2.1: Zapisz nr wiersza
    ...    Krok 2.2: Połącz XPath dla checkbox-a z nr wiersza. Tym razem musi być xpath=, bo na początku jest ( i inaczej nie przyjmuje lokatora.
    ...    Krok 2.3: Zapisz XPath checkbox-a
    ...    Krok 2.4: Sprawdź, czy checkbox jest zaznaczony
    ${Ile}    Get Matching XPath Count    ${Checkbox XPath}
    : FOR    ${Wiersz}    IN RANGE    1    ${Ile}    1
    \    Log    ${Wiersz}
    \    ${NCheckbox XPath}    Catenate    SEPARATOR=    xpath=(    ${Checkbox XPath}    )[
    \    ...    ${Wiersz}    ]
    \    Log    ${NCheckbox XPath}
    \    Checkbox Should Be Selected    ${NCheckbox XPath}

Sprawdz Czy Wartosc Pola Jest Rowna
    [Arguments]    ${PoleTekstowe Id}    ${WartoscDoPorownania}
    [Documentation]    Krok 1: Pobierz wartośc pola tekstowego o wskazanym Id.
    ...    Krok 2: Sprawdź, czy poprana wartość jest równa wartości do porównania.
    ${WartoscPola}    Get Value    ${PoleTekstowe Id}
    Should Be Equal    ${WartoscPola}    ${WartoscDoPorownania}

Sprawdz Czy Wartosc Listy Jest Rowna
    [Arguments]    ${PoleTekstowe Id}    ${WartoscDoPorownania}
    [Documentation]    Krok 1: Pobierz wartość listy (value) o wskazanym Id.
    ...    Krok 2: Sprawdź, czy poprana wartość jest równa wartości do porównania.
    ${WartoscPola}    Get Selected List Label    ${PoleTekstowe Id}
    Should Be Equal    ${WartoscPola}    ${WartoscDoPorownania}

Sprawdz Czy Wartosc Select2 Jest Rowna
    [Arguments]    ${Pole Id}    ${WartoscDoPorownania}
    [Documentation]    Krok 1: Pobierz tekst z wybranego elementu
    ...    Krok 2: Pobierz ostatnią linię z tekstu wylosowanego elementu
    ...    Krok 3: Dla IE usuń pierwszy znak, bo dla IE piwrwszy znak to krzyżyk
    ...    Krok 4: Warunek dla pustej listy ulic w adresie, wtedy wartość aktywnego elementu to "Ulica" i trzeba to zmienić na ${EMPTY}
    ...    Krok 5: Sprawdź, czy poprana wartość jest równa wartości do porównania.
    ${WybranyElement}    Get Text    ${Pole Id}
    ${WybranyElement}    Get Line    ${WybranyElement}    -1
    ${WybranyElement}    Run Keyword If    '${BROWSER}'=='ie' and '${WybranyElement}'!='Ulica'    Get Substring    ${WybranyElement}    1
    ...    ELSE    Set Variable    ${WybranyElement}
    Run Keyword If    '${WybranyElement}'=='Ulica'    Set Test Variable    ${WybranyElement}    ${EMPTY}
    Should Be Equal    ${WybranyElement}    ${WartoscDoPorownania}

Wypelnij Pole Losowym Tekstem
    [Arguments]    ${Pole Id}    ${LiczbaZnakow}    ${Biblioteka}=String
    [Documentation]    Wymaga bibloteki String i FakerLibrary. Wypełnia wskazane pole losowym tekstem o wskazanej długości + 5 znaków i zwraca wylosowany ciąg o wskazanej długości.
    ...    Krok 1: Dodaj 5 do wskazanej liczy znaków
    ...    Krok 2: Wygeneruj losowy ciąg znaków przy pomocy wskazanej biblioteki: Faker, String
    ...    Krok 3: Wstaw wylosowany ciąg znaków do pola tekstowego
    ...    Krok 4: Przytnij wylosowany ciąg do podanej ilości znaków
    ${LiczbaZnakowPowiekszona}    Evaluate    ${LiczbaZnakow}+5
    ${CiagZnakow}    Run Keyword If    '${Biblioteka}'=='Faker'    Text    ${LiczbaZnakowPowiekszona}
    ...    ELSE    Generate Random String    ${LiczbaZnakowPowiekszona}    chars=[LETTERS][NUMBERS] ~`!@#$%^&*()_+-={}|[]\:";'<>?,./ę óąśłżźćń
    Input Text    ${Pole Id}    ${CiagZnakow}
    ${CiagZnakow}    Get Substring    ${CiagZnakow}    0    ${LiczbaZnakow}
    [Return]    ${CiagZnakow}

Otworz Okno Walidacji
    [Documentation]    Krok 1: Kliknij przycisk Sprawdź poprawność
    ...    Krok 2: Czekaj, aż będą widoczne elementy listy modala z walidacją
    Click Element    ${SprawdzPoprawnosc Id}
    Wait Until Element Is Visible    ${ElementListyWalidacja Id}

Zamknij Okno Walidacji
    [Documentation]    Krok 1: Naciśnij Escape
    ...    Krok 2: Czekaj, aż nie będą widoczne elementy listy modala z walidacją
    ...    Krok 3: Czekaj 1 sekundę, żeby okno faktycznie się zamknęło
    Press Key    ${ModalWalidacja Id}    \\27    #ASCII code dla Escape
    Wait Until Element Is Not Visible    ${ElementListyWalidacja Id}
    Sleep    1s

Walidacja Zawiera Label Z Komunikatem
    [Arguments]    ${TrescLabel}    ${TrescKomunikatu}    ${TakNie}
    [Documentation]    Słowo kluczowe sprawdza, czy na ekranie walidacji jest wskazana treść komunikatu dla elementu o wskazanej labelce. Wartość true zmiennej ${TakNie} oznacza, że komunikat powinien znajdować się na stornie, false, że nie powinien.
    ...    Krok 1: Połącz treść labelki i komunikatu w pełną treść do wyszukania
    ...    Krok 2: Połącz treść komunikatu z XPath linka na modalu walidacji
    ...    Krok 3.1: Jeżeli ${TakNie} = 1, element powinien znajfować się na stronie
    ...    Krok 3.2: Jeżeli ${TakNie} = 0, element nie powinien znajfować się na stronie
    ${Tresc}    Catenate    SEPARATOR=    ${TrescLabel}    ${TrescKomunikatu}
    ${ModalWalidacjaLink XPath}    Catenate    SEPARATOR=    ${ModalWalidacjaLink XPath}    [text()='    ${Tresc}    ']
    Run Keyword If    ${TakNie}    Page Should Contain Element    ${ModalWalidacjaLink XPath}
    ...    ELSE    Page Should Not Contain Element    ${ModalWalidacjaLink XPath}

Walidacja Zawiera Label Z Prefixem I Komunikatem
    [Arguments]    ${TrescPrefixu}    ${TrescLabel}    ${TrescKomunikatu}    ${TakNie}
    [Documentation]    Słowo kluczowe sprawdza, czy na ekranie walidacji jest wskazana treść komunikatu dla elementu o wskazanym prefixie i wskazanej labelce. Wartość true zmiennej ${TakNie} oznacza, że komunikat powinien znajdować się na stornie, false, że nie powinien.
    ...    Krok 1: Połącz treść labelki i komunikatu w pełną treść do wyszukania
    ...    Krok 2: Połącz treść komunikatu z XPath linka na modalu walidacji
    ...    Krok 3.1: Jeżeli ${TakNie} = 1, element powinien znajfować się na stronie
    ...    Krok 3.2: Jeżeli ${TakNie} = 0, element nie powinien znajfować się na stronie
    ${Tresc}    Catenate    ${TrescPrefixu}    ${TrescLabel}
    ${Tresc}    Catenate    SEPARATOR=    ${Tresc}    ${TrescKomunikatu}
    ${ModalWalidacjaLink XPath}    Catenate    SEPARATOR=    ${ModalWalidacjaLink XPath}    [text()='    ${Tresc}    ']
    Run Keyword If    ${TakNie}    Page Should Contain Element    ${ModalWalidacjaLink XPath}
    ...    ELSE    Page Should Not Contain Element    ${ModalWalidacjaLink XPath}

Pole O Wartosci Jest W Bazie
    [Arguments]    ${Tabela}    ${Pole}    ${Wartosc}
    [Documentation]    Krok 1: Zbuduj zapytanie w stylu: SELECT * FROM ${Tabela} WHERE ${Pole} = '${Wartosc}'; łącz ze spacjami
    ...    Krok 2: Zbuduj zapytanie w stylu: SELECT * FROM ${Tabela} WHERE ${Pole} = '${Wartosc}'; łącz bez spacji
    ...    Krok 3: Zapisz zapytanie
    ...    Krok 4: Sprawdź, czy jest w bazie
    ${Zapytanie}    Catenate    SELECT * FROM    ${Tabela}    WHERE    ${Pole}    = '
    ${Zapytanie}    Catenate    SEPARATOR=    ${Zapytanie}    ${Wartosc}    ';
    Log    ${Zapytanie}
    Sprawdz Czy Jest W Bazie    ${Zapytanie}

Wyczysc Pole Data
    [Arguments]    ${Pole Id}
    [Documentation]    Czyści pole typu data bez czasu. Domyślne rozwiązanie nie działa na Chrome, konieczne jest zastosowanie JavaScript.
    Run Keyword If    '${BROWSER}'=='chrome'    Wyczysc Pole Data Chrome    ${Pole Id}
    ...    ELSE    Wyczysc Pole Data Domyslnie    ${Pole Id}

Wyczysc Pole Data Domyslnie
    [Arguments]    ${Pole Id}
    [Documentation]    Wykonuje ośmiokrotne naciśnięcie backspace na polu z datą, co powoduje usunięcie daty bez czasu
    : FOR    ${Index}    IN RANGE    7
    \    Press Key    ${Pole Id}    \\8    # ASCII code dla Backspace

Wyczysc Pole Data Chrome
    [Arguments]    ${Pole Id}
    [Documentation]    Krok 1: Usuń ciąg "id=" ze zmiennej. JavaScript nie działa z tym przedrostkiem.
    ...    Krok 2: Przesuń ekran do pola z datą
    ...    Krok 3: Za pomocą JavaScript wstaw pusty ciąg znaków do pola z datą.
    ${PoleBezId}    Fetch From Right    ${Pole Id}    =
    Focus    ${Pole Id}
    Execute JavaScript    document.getElementById('${PoleBezId}').value='';

Pilnuj Formatu Daty
    [Arguments]    ${Pole Id}
    [Documentation]    Sprawdza, czy w pola typu data można wprowadzić wartości niebędące datą. Domyślne rozwiązanie nie działa na Chrome, konieczne jest zastosowanie JavaScript.
    Run Keyword If    '${BROWSER}'=='chrome'    Pilnuj Formatu Daty Chrome    ${Pole Id}
    ...    ELSE    Pilnuj Formatu Daty Domyslnie    ${Pole Id}

Pilnuj Formatu Daty Domyslnie
    [Arguments]    ${Pole Id}
    [Documentation]    Sprawdza, czy w pola typu data można wprowadzić wartości niebędące datą
    ...    Krok 1: Wprowadź 20136070 do pola
    ...    Krok 2: Przestaw fokus na kolejne pole
    ...    Krok 3: Pobierz wartość z pola
    ...    Krok 4: Pole powinno być puste
    ...    Krok 5: Wprowadź abcd do pola
    ...    Krok 6: Przestaw fokus na przycisk Zapisz
    ...    Krok 7: Pobierz wartość z pola
    ...    Krok 8: Pole powinno być puste
    Input Text    ${Pole Id}    20136070
    Press Key    ${Pole Id}    \\9    # ASCII code dla Tab
    Comment    Focus    ${ZapiszWniosek Id}    # Żeby raz sprawdzić Tab, a raz przeniesieniem focus-a
    ${WartoscZPolaDataPoEdycji}    Get Value    ${Pole Id}
    Should Be Equal    ${WartoscZPolaDataPoEdycji}    ${EMPTY}
    Input Text    ${Pole Id}    abcd
    Focus    ${ZapiszWniosek Id}
    ${WartoscZPolaDataPoEdycji}    Get Value    ${Pole Id}
    Should Be Equal    ${WartoscZPolaDataPoEdycji}    ${EMPTY}

Pilnuj Formatu Daty Chrome
    [Arguments]    ${Pole Id}
    [Documentation]    Sprawdza, czy w pola typu data można wprowadzić wartosci niebędące datą
    ...    Krok 1: Usuń ciąg "id=" ze zmiennej. JavaScript nie działa z tym przedrostkiem.
    ...    Krok 2: Ustaw zmienną z błędą datą 2013-60-70
    ...    Krok 3: Ustaw ekran na polu z datą
    ...    Krok 4: Za pomocą JavaScript wprowadź błedną datę do pola
    ...    Krok 5: Przestaw fokus na kolejne pole
    ...    Krok 6: Pobierz wartość z pola
    ...    Krok 7: Pole powinno być puste
    ...    Krok 8: Ustaw zmienną z błędą datą abcd
    ...    Krok 9: Ustaw ekran na polu z datą
    ...    Krok 10: Za pomocą JavaScript wprowadź błedną datę do pola
    ...    Krok 11: Przestaw fokus na przycisk Zapisz
    ...    Krok 12: Pobierz wartość z pola
    ...    Krok 13: Pole powinno być puste
    ${PoleBezId}    Fetch From Right    ${Pole Id}    =
    ${BlednaData}    Set Variable    2013-60-70
    Focus    ${Pole Id}
    Execute JavaScript    document.getElementById('${PoleBezId}').value='${BlednaData}';
    Focus    ${ZapiszWniosek Id}
    ${WartoscZPolaDataPoEdycji}    Get Value    ${Pole Id}
    Should Be Equal    ${WartoscZPolaDataPoEdycji}    ${EMPTY}
    ${BlednaData}    Set Variable    abcd
    Focus    ${Pole Id}
    Execute JavaScript    document.getElementById('${PoleBezId}').value='${BlednaData}';
    Focus    ${ZapiszWniosek Id}
    ${WartoscZPolaDataPoEdycji}    Get Value    ${Pole Id}
    Should Be Equal    ${WartoscZPolaDataPoEdycji}    ${EMPTY}

Dodaj Do Dzisiejszej Daty
    [Arguments]    ${PrzedzialCzasu}
    [Documentation]    Zwiększa dzisiejszą datę o wskazany przedział czasu i zwraca w formacie do wpisania w polach typu data w LSI, tj. same cyfry bez myślników.
    ...    Krok 1: Pobierz bierzącą datę
    ...    Krok 2: Dodaj do daty wskazany przedział czasu
    ...    Krok 3: Pobierz samą datę
    ${DataPowiekszona}    Get Current Date
    ${DataPowiekszona}    Add Time To Date    ${DataPowiekszona}    ${PrzedzialCzasu}
    ${DataPowiekszona}    Get Substring    ${DataPowiekszona}    0    10
    [Return]    ${DataPowiekszona}

Wprowadz Date
    [Arguments]    ${Pole Id}    ${Data}
    [Documentation]    Wprowadza datę do wskazanego pola. Domyślne rozwiązanie nie działa na Chrome, konieczne jest zastosowanie JavaScript.
    Run Keyword If    '${BROWSER}'=='chrome'    Wprowadz Date Chrome    ${Pole Id}    ${Data}
    ...    ELSE    Wprowadz Date Domyslnie    ${Pole Id}    ${Data}

Wprowadz Date Domyslnie
    [Arguments]    ${Pole Id}    ${Data}
    [Documentation]    Krok 1: Usuń myślniki z daty
    ...    Krok 2: Wprowadź datę do pola
    ${Data}    Remove String    ${Data}    -
    Input Text    ${Pole Id}    ${Data}

Wprowadz Date Chrome
    [Arguments]    ${Pole Id}    ${Data}
    [Documentation]    Krok 1: Usuń ciąg "id=" ze zmiennej. JavaScript nie działa z tym przedrostkiem.
    ...    Krok 2: Ustaw ekran na polu z datą
    ...    Krok 3: Za pomocą JavaScript wprowadź datę do pola.
    ${PoleBezId}    Fetch From Right    ${Pole Id}    =
    Focus    ${Pole Id}
    Execute JavaScript    document.getElementById('${PoleBezId}').value='${Data}';

XPath Ostatni
    [Arguments]    ${XPath}
    ${XPathOstatni}    Catenate    SEPARATOR=    xpath=(    ${XPath}    )[last()]
    [Return]    ${XPathOstatni}

Rowna Liczba Dwoch Elementow XPath
    [Arguments]    ${Element1 XPath}    ${Element2 XPath}
    [Documentation]    Sprawdza, czy liczba dwóch elementów XPath jest równa
    ...    Krok 1: Pobierz liczbę elementów 1
    ...    Krok 2: Pobierz liczbę elementów 2
    ...    Krok 3: Liczby obu elementów powinny być równe
    ${Liczba Elementow1}    Get Matching Xpath Count    ${Element1 XPath}
    ${Liczba Elementow2}    Get Matching Xpath Count    ${Element2 XPath}
    Should Be Equal    ${Liczba Elementow1}    ${Liczba Elementow2}

Odslon Element Id
    [Arguments]    ${Element Id}
    [Documentation]    Odsłania element zasłonięty przez pasek Akcje wniosku
    ...    Krok 1: Odetnij od id listy "id="
    ...    Krok 2: Przewija okno do wskazanego elementu (element jest zasłonięty przez pasek Akcje wniosku)
    ...    Krok 3: Wykonaj JavaScript przesuwający tekst w dół
    ${Element Id}    Fetch From Right    ${Element Id}    =
    Execute JavaScript    document.getElementById('${Element Id}').scrollIntoView();
    Execute JavaScript    window.scroll(0,window.pageYOffset - 100)    # przesuwa stronę o 100 px do góry, bo przycisk zapisz jest załonięty przez akcje wniosku

Odslon Element XPath
    [Arguments]    ${Przycisk XPath}
    [Documentation]    Odsłania element zasłonięty przez pasek Akcje wniosku
    ...    Krok 1: Przenieś zaznaczenie na przycisk dodaj
    ...    Krok 2: Wykonaj JavaScript przesuwający tekst w dół - w Firefox przycisk Dodaj jest zasłaniany przez Akcje wniosku
    Set Focus To Element    ${Przycisk XPath}
    Execute JavaScript    window.scroll(0,window.pageYOffset - 100)    # przesuwa stronę o 100 px do góry, bo przycisk zapisz jest załonięty przez akcje wniosku

Dodaj Elementy Przyciskiem
    [Arguments]    ${Pole XPath}    ${PrzyciskDodaj XPath}    ${IleDodac}    ${PrefixZmiennej}    ${DlugoscLosowanegoTekstu}
    [Documentation]    *W przyszłości należy przerobić na obsługę listy pól w miejsce uzupełniania jednego pola*
    ...    Podaj XPath pola do uzupełnienia, XPath przycisku dodaj, liczbę elementów do dodania, prefix dla tworzonych zmiennych typu test, długość losowanego tekstu.
    ...    Krok 1: Pobierz liczbę rekordów przed dodaniem wierszy
    ...    Krok 2: Oblicz liczbę wierszy po dodaniu
    ...    Krok 3: Stwórz XPath dla ostatniego elementu
    ...    Krok 4: Odsłoń przycisk dodaj zasłonięty w Firefox przez Akcje wniosku
    ...    Krok 5: Wykonaj operacje od liczby wierszy przed do liczby wierszy po
    ...    Krok 5.1: Kliknij przycisk dodaj
    ...    Krok 5.2: Wypełnij pole wylosowanym tekstem
    ...    Krok 5.3: Stwórz zmienną typu test zgodnie z prefixem i numerem iteracji, zawierającą wylosowany tekst
    ...    Krok 9: Stwórz zmienną typu tekst zgodnie z prefixem, zawierającą liczbę rekordów przed dodaniem
    ${LiczbaPolPrzed}    Get Element Count    ${Pole XPath}
    ${LiczbaPolPo}    Evaluate    ${LiczbaPolPrzed} + ${IleDodac}
    ${PoleOstatnie XPath}    XPath Ostatni    ${Pole XPath}
    Odslon Element XPath    ${PrzyciskDodaj XPath}
    : FOR    ${Index}    IN RANGE    ${LiczbaPolPrzed}    ${LiczbaPolPo}
    \    Click Button    ${PrzyciskDodaj XPath}
    \    ${Tekst}    Wypelnij Pole Losowym Tekstem    ${PoleOstatnie XPath}    ${DlugoscLosowanegoTekstu}
    \    Set Test Variable    ${${PrefixZmiennej}${Index}}    ${Tekst}
    Set Test Variable    ${${PrefixZmiennej}Przed}    ${LiczbaPolPrzed}

Sprawdz I Usun Elementy Dodane Przyciskiem
    [Arguments]    ${Pole XPath}    ${PoleXDoPodmiany Id}    ${PrzyciskUsun XPath}    ${IleDodano}    ${PrefixZmiennej}
    [Documentation]    *W przyszłości należy przerobić na obsługę sprawdzania wielu pól*
    ...    Podaj XPath pola do sprawdzenia, Id pola do sprawdzenia ze znakiem "x" do podmieniania, XPath przycisku usuń, liczbę elementów, ktróra została dodana, prefix dla tworzonych zmiennych typu test.
    ...    Krok 1: Pobierz liczbę rekordów po dodaniu wierszy (przed usuwaniem)
    ...    Krok 2: Oblicz różnicę liczb wierszy po dodaniu i przed (przed usuwaniem i po)
    ...    Krok 3: Przekształć liczbę elementów, która została dodana na liczbę
    ...    Krok 4: Róznica liczb wierszy i liczba dodanych wierszy powinny być równe
    ...    Krok 5: Stwórz XPath dla ostatniego przycisku usuń
    ...    Krok 6: Wykonaj operacje od liczby wierszy po (przed usunięciem) do liczby wierszy przed (po usunięciu), krok -1
    ...    Krok 6.1: Odsłoń przycisk usuń zasłonięty w Firefox przez Akcje wniosku
    ...    Krok 6.2: Odejmij 1 od indeksu i przekształć na string
    ...    Krok 6.3: Zastąp znak x indeksem w Id pola do sprawdzenia
    ...    Krok 6.4: Pobierz wartość z pola o Id z poprzedniego kroku
    ...    Krok 6.5: Pobrany tekst powinien być równy wylosowanemu tekstowi zapisanemu w zmiennej
    ...    Krok 6.6: Kliknij OK w najbliższym alercie
    ...    Krok 6.7: Kliknij przycisk usuń
    ...    Krok 6.8: Wykonaj JavaScript przesuwający tekst w dół - w Firefox przycisk Dodaj jest zasłaniany przez Akcje wniosku
    ...    Krok 13: Pobierz liczbę rekordów po usnięciu wierszy
    ...    Krok 14: Stwórz zmienną typu tekst zgodnie z prefixem, zawierającą liczbę rekordów po usunięciu wierszy
    ${LiczbaPolPo}    Get Matching Xpath Count    ${Pole XPath}
    ${LiczbaPolRoznica}    Evaluate    ${LiczbaPolPo} - ${${PrefixZmiennej}Przed}
    ${IleDodano}    Evaluate    ${IleDodano}
    Should Be Equal    ${LiczbaPolRoznica}    ${IleDodano}
    ${PrzyciskUsunOstatni XPath}    XPath Ostatni    ${PrzyciskUsun XPath}
    : FOR    ${Index}    IN RANGE    ${LiczbaPolPo}    ${${PrefixZmiennej}Przed}    -1
    \    Odslon Element XPath    //*[text()='Słowa kluczowe']    # ${PrzyciskUsunOstatni XPath}
    \    ${Index}    Convert To String    ${Index-1}
    \    ${PoleTekstowe Id}    Replace String    ${PoleXDoPodmiany Id}    x    ${Index}
    \    ${Tekst}    Get Value    ${PoleTekstowe Id}
    \    Should Be Equal    ${Tekst}    ${${PrefixZmiennej}${Index}}
    \    Choose Ok On Next Confirmation
    \    Click Button    ${PrzyciskUsunOstatni XPath}
    ${LiczbaPolPo}    Get Matching Xpath Count    ${Pole XPath}
    Set Test Variable    ${${PrefixZmiennej}Po}    ${LiczbaPolPo}

Sprawdz Czy Zmienne Przed I Po Rowne
    [Arguments]    ${PrefixZmiennej}
    [Documentation]    Sprawdza, czy zmienne o sufiksie "Przed" i "Po" dla podanego prefiksu są równe
    Should Be Equal    ${${PrefixZmiennej}Przed}    ${${PrefixZmiennej}Po}

Select2 Pobierz Liste Elementow
    [Arguments]    ${Lista Id}
    [Documentation]    Pobiera liste elementów z listy rozwijanej stworzonej za pomocą Select2. Zwraca liczbę elementów i liste elementów
    ...    Krok 1: Kliknij listę o wskazanym id
    ...    Krok 2: Czekaj na zakończenie działań JavaScript
    ...    Krok 3: Zamień container na results - to wynika z miejsca, gdzie Select2 przechowuje dane
    ...    Krok 4: Odetnij od id listy "id="
    ...    Krok 5: Stwórz XPath dla elementów li zawierających elementy listy
    ...    Krok 6: Sprawdź ile jest elemetów listy
    ...    Krok 7: Stwórz zmienną na listę
    ...    Krok 8.1: Pobierz tekst z kolejnych elemetów li
    ...    Krok 8.2: Dodaj element do listy, jezeli jest różny od "No results found"
    ...    Krok 11: Sprawdz ile jest elementów listy po odjęciu "No results found"
    Click Element    ${Lista Id}
    Czekaj Na Zakonczenie Ajax
    ${Lista XPath}    Replace String    ${Lista Id}    container    results
    ${Lista XPath}    Fetch From Right    ${Lista XPath}    =
    ${Lista XPath}    Catenate    SEPARATOR=    //*[@id='    ${Lista XPath}    ']/li
    ${LiczbaElementow}    Get Matching Xpath Count    ${Lista XPath}
    ${ListaElementow}    Create List
    : FOR    ${i}    IN RANGE    1    ${LiczbaElementow} + 1
    \    ${ElementLabel}    Get Text    xpath=(${Lista XPath})[${i}]
    \    Run Keyword If    "${ElementLabel}"!="No results found"    Append To List    ${ListaElementow}    ${ElementLabel}
    ${LiczbaElementow}    Get Length    ${ListaElementow}
    [Return]    ${LiczbaElementow}    ${ListaElementow}

Select2 Wybierz Element
    [Arguments]    ${Lista Id}    ${NumerKolejnyElementu}
    [Documentation]    Wybiera wskazany element z listy Select2
    ...    Krok 1: Zamień container na results - to wynika z miejsca, gdzie Select2 przechowuje dane
    ...    Krok 2: Odetnij od id listy "id="
    ...    Krok 3: Stwórz XPath dla wskazanego li zawierającego element listy
    ...    Krok 4: Kliknij wskazany element listy
    ${Lista XPath}    Replace String    ${Lista Id}    container    results
    ${Lista XPath}    Fetch From Right    ${Lista XPath}    =
    ${Lista XPath}    Catenate    SEPARATOR=    xpath=(//*[@id='    ${Lista XPath}    ']/li)[    ${NumerKolejnyElementu}
    ...    ]
    Click Element    ${Lista XPath}

Select2 Wybrany Element Wartosc
    [Arguments]    ${Lista Id}
    [Documentation]    Zwraca wartość wybranego elementu ze wskazanej listy
    ...    Krok 1: Pobierz tekst z wybranego elementu
    ...    Krok 2: Pobierz ostatnią linię z tekstu wylosowanego elementu
    ...    Krok 3: Dla IE usuń pierwszy znak, bo dla IE piwrwszy znak to krzyżyk
    ${WybranyElement}    Get Text    ${Lista Id}
    ${WybranyElement}    Get Line    ${WybranyElement}    -1
    ${WybranyElement}    Run Keyword If    '${BROWSER}'=='ie'    Get Substring    ${WybranyElement}    1
    ...    ELSE    Set Variable    ${WybranyElement}
    [Return]    ${WybranyElement}

Zwroc Polskie Znaki
    [Arguments]    ${StringZKodamiUTF8}
    [Documentation]    Słowo kluczowe stworzone na potrzeby zmiany kodów UTF-8 na polskie znaki. Nie potrafię tego inaczej zrobić. Próbowałem Decode Bytes To String, ale nie działa. Zamienia bity UTF-8 na polskie znaki. Backslash trzeba poprzedzić backslash-em
    ${StringZPolskimiZnakami}    Replace String    ${StringZKodamiUTF8}    \\xc4\\x85    ą
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc4\\x87    ć
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc4\\x99    ę
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x82    ł
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x84    ń
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\xb3    ó
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x9b    ś
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\xba    ź
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\xbc    ż
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc4\\x84    Ą
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc4\\x86    Ć
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc4\\x98    Ę
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x81    Ł
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x83    Ń
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\x93    Ó
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\x9a    Ś
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\xb9    Ź
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc5\\xbb    Ż
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\xa1    á    # Bo w Opolu jest ulica Václava Havla
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\xa4    ä    # Bo w Bydgoszczy jest ulica Augustyna i Romana Trägerów
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\xbc    ü    # Bo w Bydgoszczy jest ulica Edwarda Zürna
    ${StringZPolskimiZnakami}    Replace String    ${StringZPolskimiZnakami}    \\xc3\\xa9    é    # Bo w Bydgoszczy jest ulica gen. Wiktora Thommée
    [Return]    ${StringZPolskimiZnakami}

Losuj Liczbe Z Przedzialu
    [Arguments]    ${Od}    ${Do}
    [Documentation]    Wylosuj liczbę z przedziału <"Od"; "Do">
    ${WylosowanaLiczba}    Evaluate    random.randint(${Od}, ${Do})    modules=random
    [Return]    ${WylosowanaLiczba}

Adres Wprowadz Z Listy
    [Arguments]    ${Lista Id}    ${Zapytanie}
    [Documentation]    Porównuje zawartość wskazanej listy select2 z bazą danych i zwraca wartość wylosowanego elementu z listy.
    ...    Krok 1: Lista powinna być widoczna na stronie
    ...    Krok 2: Odsłoń listę powiatów zasłoniętą przez pasek Akcje wniosku
    ...    Krok 3: Pobierz liczbę elementów i same elementy z listy select2
    ...    Krok 4: Zapisz pobraną listę
    ...    Krok 5: Pobierz listę z bazy
    ...    Krok 6: Zapisz listę pobraną z bazy
    ...    Krok 7: Obie listy powinny być równe
    ...    Krok 8: Jeżeli liczba elementów listy z formularza <> 0, to wylosuj numer elementu
    ...    Krok 9: Zapisz wylosowany numer elementu
    ...    Krok 10: Jeżeli liczba elementów listy z formularza <> 0, to wybierz element o wylosowanym kolejnym numerze
    ...    Krok 11: Jeżeli liczba elementów listy z formularza <> 0, to pobierz wartość wysowanego elementu
    ...    Krok 12: Zapisz wartość wylosowanego elementu
    Element Should Be Visible    ${Lista Id}
    Odslon Element Id    ${Lista Id}
    ${LiczbaElementow}    ${ListaElementow}    Select2 Pobierz Liste Elementow    ${Lista Id}
    Log List    ${ListaElementow}
    ${ListaElementowZBazy}    Odpytaj Baze Danych    ${Zapytanie}
    Log List    ${ListaElementowZBazy}
    Lists Should Be Equal    ${ListaElementow}    ${ListaElementowZBazy}
    ${WylosowanyElement}    Run Keyword If    ${LiczbaElementow}    Losuj Liczbe Z Przedzialu    1    ${LiczbaElementow}
    Log    ${WylosowanyElement}
    Run Keyword If    ${LiczbaElementow}    Select2 Wybierz Element    ${Lista Id}    ${WylosowanyElement}
    ${WylosowanyElement}    Run Keyword If    ${LiczbaElementow}    Select2 Wybrany Element Wartosc    ${Lista Id}
    ...    ELSE    Set Variable    ${EMPTY}
    Log    ${WylosowanyElement}
    [Return]    ${WylosowanyElement}

Pilnuj Formatu Kodu Pocztowego
    [Arguments]    ${Pole Id}
    [Documentation]    Sprawdza, czy w pola typu kod pocztowy można wprowadzić litery
    ...    Krok 1: Wprowadź abcdef do pola
    ...    Krok 2: Przestaw fokus na przycisk Zapisz
    ...    Krok 3: Pobierz wartość z pola
    ...    Krok 4: Pole powinno być puste
    Input Text    ${Pole Id}    abcdef
    Focus    ${ZapiszWniosek Id}
    ${WartoscZPolaDataPoEdycji}    Get Value    ${Pole Id}
    Should Be Equal    ${WartoscZPolaDataPoEdycji}    ${EMPTY}

Wprowadz Kod Pocztowy
    [Arguments]    ${Pole Id}    ${LiczbaZnakow}
    [Documentation]    Wymaga bibloteki String. Wypełnia wskazane pole losowym ciągiem cyfr o wskazanej długości + 5 znaków i zwraca kod pocztowy wprowadzony do pola.
    ...    Krok 1: Dodaj 5 do wskazanej liczy znaków
    ...    Krok 2: Wygeneruj losowy ciąg cyfr
    ...    Krok 3: Wstaw wylosowany ciąg cyfr do pola tekstowego
    ...    Krok 4: Przytnij wylosowany ciąg do podanej ilości znaków
    ${LiczbaZnakowPowiekszona}    Evaluate    ${LiczbaZnakow}+5
    ${CiagZnakow}    Generate Random String    ${LiczbaZnakowPowiekszona}    [NUMBERS]
    Input Text    ${Pole Id}    ${CiagZnakow}
    ${CiagZnakow}    Get Value    ${Pole Id}
    [Return]    ${CiagZnakow}

Usun Informacje O Zlozeniu Wniosku
    [Arguments]    ${IdWniosku}
    [Documentation]    Krok 1: Utwórz zapytanie do bazy usuwające z tabeli historia_statusow wpisy późniejsze niż rejestracja wniosku oraz z tabeli wniosek numer rejestracyjny wniosku
    ...    Krok 2: Wykonaj zapytanie na bazie danych
    ${Zapytanie}    Replace String    DELETE FROM dziennik.historia_statusow WHERE id >= (SELECT min(id) id FROM dziennik.historia_statusow WHERE wniosek_id = {IdWniosku} AND zdarzenie = 'Rozpoczęcie rejestracji') AND wniosek_id = {IdWniosku}; UPDATE dziennik.historia_statusow SET status_id = 2 WHERE wniosek_id = {IdWniosku} AND status_id = 3 AND komunikat = 'Skopiowano status z oryginału'; UPDATE wnioski.wnioski SET numer_rejestracyjny_wniosku = NULL, numer_kolejny_wniosku = NULL WHERE id = {IdWniosku};    {IdWniosku}    ${IdWniosku}
    Wykonaj Zapytanie Na Bazie Danych    ${Zapytanie}

Zloz Wniosek
    [Arguments]    ${IdWnioskuDoZlozenia}
    [Documentation]    Krok 1: Kliknij przycisk Złóż.
    ...    Krok 2: Czekaj, aż widoczny będzie modal
    ...    Krok 3: Czekaj, aż widoczny bedzie nagłówek w modalu
    ...    Krok 4: Nagłówek w modalu powinien zawierać tekst Czy na pewno chcesz złożyć ten wniosek?
    ...    Krok 5: Czekaj, aż widoczny będzie przycisk Złóż wniosek
    ...    -- Krok 5a: Sprawdź czy zdefiniowano zmienną {DataDoZlozeniaWniosku}
    ...    -- Krok 5b: Jeżeli zdefiniowano zmienną {DataDoZlozeniaWniosku}, to czekaj, aż minie {DataDoZlozeniaWniosku}
    ...    Krok 6: Kliknij przycisk Złóż wniosek
    ...    Krok 7: Czekaj, aż będzie widoczny komunikat o pozytywnej rejestracji (max 30s)
    ...    Krok 8: Alert powinien zawierać komunikat o pomyślnym złożeniu wniosku
    ...    Krok 11: Idz do strony lista konkursów
    ...    Krok 9: Wstaw Id wniosku do adresu podglądu wniosku
    ...    -- Krok 10: Lokacja powinna zawierać adresu podglądu wniosku
    ...    Krok 12: Wstaw Id wniosku do adresu edycji wniosku
    ...    Krok 13: Filtruj listę wniosków dla podanego Id wniosku
    ...    Krok 14: Strona nie powinna zawierać linku do adresu edycji wniosku
    ...    Krok 15: Strona powinna zawierać link do adresu podglądu wniosku
    Click Element    ${ZlozWniosek Id}
    Wait Until Element Is Visible    ${ModalZlozWniosek Id}
    Wait Until Element Is Visible    ${ModalZlozWniosekH4 CSS}
    Element Should Contain    ${ModalZlozWniosekH4 CSS}    ${ModalZlozWniosekH4Tresc}
    Wait Until Element Is Visible    ${ModalZlozWniosekPrzycisk XPath}
    Comment    ${CzyJestDataDoZlozeniaWniosku}    Run Keyword And Return Status    Variable Should Exist    ${DataDoZlozeniaWniosku}
    Comment    Run Keyword If    ${CzyJestDataDoZlozeniaWniosku}    Czekaj Na Punkt W Czasie    ${DataDoZlozeniaWniosku}
    Click Element    ${ModalZlozWniosekPrzycisk XPath}
    Wait Until Keyword Succeeds    30s    1s    Page Should Contain Element    ${AlertSuccess CSS}
    Element Should Contain    ${AlertSuccess CSS}    ${AlertZlozono}
    Comment    ${AdresWnioskuPodglad}    Catenate    SEPARATOR=    ${AdresWnioskuPodglad}    ${IdWnioskuDoZlozenia}
    Idz Do Strony Lista Konkursow
    Comment    Wait Until Keyword Succeeds    30s    1s    Page Should Contain    ${AnkietaPoRejestracjiNaglowekTresc}
    Comment    Click Element    ${AnkietaPoRejestracjiWrocPrzycisk XPath}
    ${AdresWnioskuEdycja}    Catenate    SEPARATOR=    ${AdresWnioskuEdycja}    ${IdWnioskuDoZlozenia}    # zmienna nazwana była $ {AdresWniosku}
    ${AdresWnioskuPodglad}    Catenate    SEPARATOR=    ${AdresWnioskuPodglad}    ${IdWnioskuDoZlozenia}
    Filtruj Id Wniosku    ${IdWnioskuDoZlozenia}
    Wait Until Keyword Succeeds    10s    1s    Page Should Contain Link    ${AdresWnioskuPodglad}
    Page Should Not Contain Link    ${AdresWnioskuEdycja}
    Comment    Location Should Contain    ${AdresWnioskuPodglad}

Sprawdz Czy Wniosek Zlozony
    [Arguments]    ${IdWniosku}
    [Documentation]    Krok 1: Utwórz zapytanie do bazy sprawdzające czy wniosek o wskazanym id ma nadany numer
    ...    Krok 2: Utwórz zapytanie do bazy sprawdzające czy wniosek ma wpis o rejestracji w historii statusów
    ...    Krok 3 i 4: Wykonaj zapytanie na bazie danych
    ${ZapytanieWniosek}    Replace String    SELECT id FROM wnioski.wnioski WHERE id = {IdWniosku} AND numer_rejestracyjny_wniosku IS NOT NULL;    {IdWniosku}    ${IdWniosku}
    ${ZapytanieHistoriaStatusow}    Replace String    SELECT id FROM dziennik.historia_statusow WHERE wniosek_id = {IdWniosku} AND status_id = 3 AND zdarzenie = 'Rejestracja';    {IdWniosku}    ${IdWniosku}
    Wykonaj Zapytanie Na Bazie Danych    ${ZapytanieWniosek}
    Wykonaj Zapytanie Na Bazie Danych    ${ZapytanieHistoriaStatusow}

Wybierz Zlozony Wniosek Z Otwartego Naboru
    [Documentation]    Wybiera z bazy i zwraca Id ostatatniego złożonego wniosku z każdego aktywnego naboru. Następnie przypisuje wnioski do użytkownika ${Login}
    @{Wnioski}    Odpytaj Baze Danych    SELECT wniosek_id FROM (SELECT max(dziennik.id) dziennik_id FROM wnioski.wnioski ww JOIN (SELECT wk.id FROM wnioski.konkurs wk JOIN slowniki.poziom_wdrazania pw ON pw.id = wk.id_poziomu_wdrazania WHERE wk.data_czas_zakonczenia - interval '1 hour' >= now() AND pw.kod NOT LIKE 'POWR%') nabor ON nabor.id = ww.id_konkursu JOIN (SELECT id, wniosek_id FROM dziennik.historia_statusow WHERE status_id = 3 AND zdarzenie = 'Rejestracja') dziennik ON dziennik.wniosek_id = ww.id JOIN wnioski.wnioski_ogolne_projekt wop ON wop.id_wniosku = ww.id LEFT JOIN uzupelnienia.uzupelnienia uu ON uu.id_wniosku = ww.id WHERE ww.numer_rejestracyjny_wniosku IS NOT NULL AND wop.okres_realizacji_projektu_poczatek > current_date AND uu.id IS NULL GROUP BY ww.id_konkursu) a JOIN dziennik.historia_statusow dhs ON dhs.id = dziennik_id ORDER BY wniosek_id;
    : FOR    ${IdWniosku}    IN    @{Wnioski}
    \    Log    UPDATE wnioski.wnioski ww SET id_uzytkownika = uu.id FROM uzytkownicy.uzytkownicy uu WHERE uu.nazwa = '${Login}' AND ww.id = ${IdWniosku};
    \    Wykonaj Zapytanie Na Bazie Danych    UPDATE wnioski.wnioski ww SET id_uzytkownika = uu.id FROM uzytkownicy.uzytkownicy uu WHERE uu.nazwa = '${Login}' AND ww.id = ${IdWniosku};
    [Return]    ${Wnioski}

Blad Przy Skladaniu Wniosku
    [Arguments]    ${IdWnioskuDoZlozenia}
    [Documentation]    Krok 1: Dołącz do listy ${BlendyPrzySkladaniuWniosku} podane id wniosku z błędami przy rejestracji
    ...    Krok 2: Zrób screenshot
    ...    Krok 3: Otwórz okno walidacji
    ...    Krok 4: Zrób screenshot
    ...    Krok 5: Zamknij okno walidacji
    ...    Krok 6: Idź do strony lista konkursów
    Append To List    ${BlendyPrzySkladaniuWniosku}    ${IdWnioskuDoZlozenia}
    Capture Page Screenshot
    Otworz Okno Walidacji
    Capture Page Screenshot
    Zamknij Okno Walidacji
    Idz Do Strony Lista Konkursow

Wybierz Otwarte Nabory
    [Documentation]    *Uwaga! Nie uwzglednia naboru o id 35, 53, 54, 65 (POIR 2.3.4/POIR 2.3.1/staroć, bo tam trzeba wybrać rodzaj wniosku oraz naboru); 97 (zepsuty wniosek obejmujacy wszystkie pola w generatorze); 132 (PAMI, bo tam nie ma tytułu projektu wg którego prowadzony jest test zapisu); POPR.01.01.00 Pożyczka na rozwój, bo tam nie ma tytułu projektu wg którego prowadzony jest test zapisu)*
    ...    Wybiera z bazy i zwraca Id aktywnych naborów.
    @{Nabory}    Odpytaj Baze Danych    SELECT wk.id FROM wnioski.konkurs wk JOIN slowniki.poziom_wdrazania pw ON pw.id = wk.id_poziomu_wdrazania WHERE wk.data_czas_rozpoczecia < now() AND wk.data_czas_zakonczenia - interval '1 hour' >= now() AND pw.kod NOT LIKE 'POWR%' AND wk.id NOT IN (35, 53, 54, 65, 97, 132, 124);    # pomocniczy atrybut testu do podmiany, aby wykonać test jedynie dla wniosków z naborów POPW.01.01.01 i POIR.02.04.01): SELECT wk.id FROM wnioski.konkurs wk JOIN slowniki.poziom_wdrazania pw ON pw.id = wk.id_poziomu_wdrazania WHERE wk.data_czas_rozpoczecia < now() AND wk.data_czas_zakonczenia - interval '1 hour' >= now() AND pw.kod NOT LIKE 'POWR%' AND wk.id IN (13, 133);
    Log List    ${Nabory}
    [Return]    ${Nabory}

Zmien Daty Rozpoczecia Naboru
    [Documentation]    Zmienia daty rozpoczecia naboru dla naborów, które mają się rozpocząć w ciągu 7 dni.
    Wykonaj Zapytanie Na Bazie Danych    UPDATE wnioski.konkurs SET data_czas_rozpoczecia = now() - interval '1 day' WHERE id IN (SELECT wk.id FROM wnioski.konkurs wk JOIN slowniki.poziom_wdrazania pw ON pw.id = wk.id_poziomu_wdrazania WHERE wk.data_czas_rozpoczecia BETWEEN now() AND (now() + interval '7 day') AND pw.kod NOT LIKE 'POWR%');

Wskazany Czas Minal
    [Arguments]    ${WskazanyCzas}
    [Documentation]    Sprawdza, czy wskazany czas minał
    ${Czas}    Get Time
    ${CzyMinal}    Evaluate    '${WskazanyCzas}' <= '${Czas}'
    Should Be True    ${CzyMinal}
    [Return]    ${CzyMinal}

Czekaj Na Punkt W Czasie
    [Arguments]    ${CzasUruchomienia}
    [Documentation]    Czeka przez zadany czas, aż minie wskazany moment w czasie.
    ...    Krok 1: Pobierz bieżący czas
    ...    Krok 2: Zapisz czas do logu
    ...    Krok 3: Czekaj przez x sekund, aż minie czas ze zmiennej {CzasUruchomienia}
    Log    ${CzasUruchomienia}
    Wait Until Keyword Succeeds    5 min    500ms    Wskazany Czas Minal    ${CzasUruchomienia}

Wybierz Nabor
    [Arguments]    ${id_naboru}
    [Documentation]    Tworzy wniosek we wskazanym konkursie
    ${IdNaboru}    Set Variable    ${id_naboru}
    ${AdresNaboru}    Replace String    ${AdresNaboru XPath}    IdNaboru    ${IdNaboru}
    ${IdUtworzonegoWniosku}    Utworz Wniosek    ${AdresNaboru}
    Set Suite Variable    ${IdUtworzonegoWniosku}

Wybierz element z listy
    [Arguments]    ${Lista XPath}
    [Documentation]    1. Pobiera elementy z listy wyboru określonej poprzez lokator w zmiennej ${Lista XPath} 2. Losuje jeden element z listy wyboru. 3. Wylosowany element listy wstawia do zbudowanego lokatora zmiennej ${wybor}. 4. Klika wylosowany element.
    ${elementy_listy}=    Get List Items    ${Lista XPath}
    ${element}=    Random Element    ${elementy_listy}
    ${wybor}=    Catenate    SEPARATOR=    ${Lista XPath}    /option[text()='${element}']
    Click Element    ${wybor}
    [Return]    ${element}

Wybierz elementy z listy
    [Arguments]    ${Lista_XPath}    ${IleElementowDodac}
    Set Focus To Element    ${Lista_XPath}
    @{ElementyListy}    Get List Items    ${Lista_XPath}
    @{WybraneElementy}    Random Elements    ${ElementyListy}    length=${IleElementowDodac}    unique=True
    ${ElementSlownika}    Set Variable    0
    : FOR    ${i}    IN RANGE    0    ${IleElementowDodac}
    \    ${Element}    Get From List    ${WybraneElementy}    ${ElementSlownika}
    \    ${Wybor}    Catenate    SEPARATOR=    ${Lista_XPath}    /option[text()='${Element}']
    \    Wait Until Element Is Visible    ${Wybor}    timeout=10s
    \    Click Element    ${Wybor}
    \    ${ElementSlownika}    Evaluate    ${ElementSlownika}+1

Wybierz element z listy bez 1 pozycji
    [Arguments]    ${lista_wyboru_locator}
    [Documentation]    Krok 1. Pobierz listę elementów z pola. Krok 2. Pozbaw listę pierwszej opcji. Krok 3. Wylosuj element z okrojonej listy. Krok 4. Wybierz wylosowany element na liście
    ${elementy_listy}=    Get List Items    ${lista_wyboru_locator}
    ${elementy_listy_bez_1}=    Get Slice From List    ${elementy_listy}    1
    ${wybrany_element}=    Evaluate    random.choice($elementy_listy_bez_1)    random
    Select From List By Label    ${lista_wyboru_locator}    ${wybrany_element}
    [Return]    ${wybrany_element}

Kliknij Dropdown i wybierz losową opcje
    [Arguments]    ${AdresDropdowna}
    [Documentation]    Klika na dropdown, tworzy adres containera results, pobiera liczbę elementów, wpisuje wartość losowego elementu w pole input i zatwierdza klawiszem enter. Zwraca wartość elementu
    ${AdresDropdownaBezId}    Fetch From Right    ${AdresDropdowna}    =
    ${AdresDropdownaBezId}    ${last} =    Split String From Right    ${AdresDropdownaBezId}    -    1
    ${Lista XPath}    Catenate    SEPARATOR=    ${AdresDropdownaBezId}-results
    ${Lista XPath}    Catenate    SEPARATOR=    //*[@id=    ${Lista XPath}    "]/li
    Comment    Focus Javascript Id    ${AdresDropdowna}
    Skroluj o kawalek ekranu
    Click    ${AdresDropdowna}
    Czekaj Na Zakonczenie Ajax
    Wait Until Element Is Visible    ${Lista XPath}    timeout=20s
    ${LiczbaElementow}    Get Element Count    ${Lista XPath}
    : FOR    ${i}    IN RANGE    1
    \    ${LosowyIndexElementu} =    run keyword if    ${LiczbaElementow}==0    set variable    ${Empty}
    \    run keyword if    ${LiczbaElementow}==0    exit for loop
    \    ${LosowyIndexElementu} =    run keyword if    ${LiczbaElementow}==1    set variable    1
    \    run keyword if    ${LiczbaElementow}==1    exit for loop
    \    ${LosowyIndexElementu} =    Random Int    1    ${LiczbaElementow}
    ${TekstWybranegoElementu} =    Get Text    xpath=(${Lista XPath})[${LosowyIndexElementu}]
    Run Keyword If    '${TekstWybranegoElementu}'!='No results found'    press key    xpath=//span/input    ${TekstWybranegoElementu}
    ...    ELSE    set variable    ${Empty}
    press key    xpath=//span/input    \\13
    [Return]    ${TekstWybranegoElementu}

Focus Javascript Id
    [Arguments]    ${id}
    [Documentation]    Skroluje do elementu używająć javascript i lokatora id
    ${lokatorBezId}    Fetch From Right    ${id}    =
    Execute JavaScript    document.getElementById('${lokatorBezId}').scrollIntoView(false);

Skroluj o kawalek ekranu
    [Documentation]    Skroluje stronę w dół
    execute javascript    window.scrollBy(0,350);

Click2
    [Arguments]    ${locator}
    [Documentation]    Click ktory dziala na IE
    Press Key    ${locator}    \\13

Click
    [Arguments]    ${Lokator}
    [Documentation]    Skroluje do elementu a następnie klika go
    Set Focus To Element    ${Lokator}
    Skroluj o kawalek ekranu do gory
    Run Keyword If    '${BROWSER}'=='ie'    Click2    ${Lokator}
    ...    ELSE    click element    ${Lokator}

Skroluj o kawalek ekranu do gory
    [Documentation]    Skroluje stronę do góry
    execute javascript    window.scrollBy(0,-350);

Click Javascript Id
    [Arguments]    ${id}
    [Documentation]    Klika element przy uzyciu javascript i wykorzystujac lokator id
    ${lokatorBezId}    Fetch From Right    ${id}    =
    Execute JavaScript    document.getElementById('${lokatorBezId}').click();

Kliknij Województwo i wybierz z Polska Wschodnia
    [Arguments]    ${AdresDropdowna}
    [Documentation]    Klika na dropdown, tworzy adres containera results, pobiera liczbę elementów, wpisuje wartość losowego elementu w pole input i zatwierdza klawiszem enter. Zwraca wartość elementu
    ${AdresDropdownaBezId}    Fetch From Right    ${AdresDropdowna}    =
    ${AdresDropdownaBezId}    ${last} =    Split String From Right    ${AdresDropdownaBezId}    -    1
    ${Lista XPath}    Catenate    SEPARATOR=    ${AdresDropdownaBezId}-results
    ${Lista XPath}    Catenate    SEPARATOR=    //*[@id=    ${Lista XPath}    "]/li
    Skroluj o kawalek ekranu
    Click    ${AdresDropdowna}
    Czekaj Na Zakonczenie Ajax
    Wait Until Element Is Visible    ${Lista XPath}    timeout=20s
    ${LiczbaElementow}    Get Element Count    ${Lista XPath}
    : FOR    ${i}    IN RANGE    999
    \    ${LosowyIndexElementu} =    run keyword if    ${LiczbaElementow}==0    set variable    ${Empty}
    \    run keyword if    ${LiczbaElementow}==0    exit for loop
    \    ${LosowyIndexElementu} =    run keyword if    ${LiczbaElementow}==1    set variable    1
    \    run keyword if    ${LiczbaElementow}==1    exit for loop
    \    ${LosowyIndexElementu} =    Random Int    1    ${LiczbaElementow}
    \    ${TekstWybranegoElementu} =    Get Text    xpath=(${Lista XPath})[${LosowyIndexElementu}]
    \    Run Keyword If    $TekstWybranegoElementu in $PolskaWschodnia    press key    xpath=//span/input    ${TekstWybranegoElementu}
    \    Run Keyword If    $TekstWybranegoElementu in $PolskaWschodnia    exit for loop
    press key    xpath=//span/input    \\13
    [Return]    ${TekstWybranegoElementu}
