*** Variables ***
${WalidacjaPoleObowiazkowe}    : To pole jest obowiązkowe    # Komunikat walidacji dla pól które nie mogą być puste
${WalidacjaDataOdMniejszaRownaDataDo}    Okres realizacji projektu <od> powinno być wcześniej niż Okres realizacji projektu <do>    # Komunikat dla "daty od" większej od "daty do"
${WalidacjaDataNieMozeBycWczesniejsza}    : Data nie może być wcześniejsza niż 01.01.2014    # Komunikat dla daty mniejszej od 2014-01-01
${WalidacjaDataNieMozeBycPozniejsza}    : Zakończenie realizacji projektu nie może nastąpić później niż 31.12.2023    # Komunikat dla daty większej od 2023-12-31
${WalidacjaDataOdMniejszaOdJutro}    Planowany termin rozpoczęcia realizacji projektu nie może być wcześniejszy niż dzień następny po dniu złożenia wniosku w generatorze.    # Data rozpoczęcia realizacji mniejsza od jutro
${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    Adres siedziby wnioskodawcy -    # Prefix dla grupy Adres siedziby wnioskodawcy
${WalidacjaPierwszyLink Xpath}    //*[@id="modal"]/div/div/div[2]/table/tbody/tr[1]//a[@class="jumpToError sectionTitle"]    # Pierwszy link na oknie walidacji
