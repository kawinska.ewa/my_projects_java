*** Variables ***
${EdycjaPrzycisk XPath}    //a[contains(text(),'Edycja')]    # Przycisk Edycja
${NaglowekTabeliElement XPath}    //div[@id='gwaHeading']//a    # Nagłowek tabeli z elementami wniosków
${NaglowekTabeliElementTresc}    Generator Wniosków Aplikacyjnych - <dzialanie>, wersja <nrWersji>    # Treść nagłówka tabeli z elementami wniosków
${NaglowekTabeliElementDzialanie}    <dzialanie>    # Ciąg znaków z treści do zmiany na działanie
${NaglowekTabeliElementNrWersji}    <nrWersji>    # Ciąg znaków z treści do zmiany na nr wersji
${TabelaZastosowaneElementy XPath}    //*[text()="Zastosowane elementy"]/following::table[1]    # Pierwsza z wszystkich tabel po elemencie z tekstem "Zastosowane elementy"
# ${TabelaZastosowaneElementy XPath}    /descendant::table[1]    # Pierwsza z wszystkich tabel na stronie
${KonfigurujPrzycisk XPath}    //table[1]//tr[{WierszTabeli}]//a[contains(@class, 'btn-success')][1]    # Przycisk Konfiguruj na stronie z zastosowanymi elementami
${ModalKonfigurujTytul Id}    id=exampleModalLabel
${ModalKonfiguruj Id}    id=modal
${LogotypElemetNazwa}    Logotyp    # Nazwa elementu z kolumny Element
${AkcjeWnioskuElemetNazwa}    Akcje wniosku    # Nazwa elementu z kolumny Element
${OgolneInformacjeOProjekcieNazwa}    Ogólne informacje o projekcie    # Nazwa elementu z kolumny Element
${WnioskodawcaOgolneNazwa}    Wnioskodawca - ogólne    # Nazwa elementu z kolumny Element
${ListaWersjeElementow_NumerWersji_OperatorFiltra}    //*[@aria-label="Operator filtra dla Numer wersji"]    # Operator filtra dla kolumny "Numer wersji" na liście wersji elementów (Wzory Wniosków)
