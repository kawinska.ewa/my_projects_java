*** Variables ***
${KlasProjNrKoduPKD XPath}    //*[@id='klasyfikacja_projektu_pkdProjektu']    # Numer kodu PKD działalności, której dotyczy projekt
${KlasProjOpisRodzajuDzialalnsci Id}    id=klasyfikacja_projektu_pkdProjektuWyjasnienie    # Opis rodzaju działalności
${KlasProjWplywNiepelnosprawniNeutralny Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansNiepelnosprawnychWylaczonyNeutralny_0    # Wybor neutralny
${KlasProjWplywNiepelnosprawniPozytywny Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansNiepelnosprawnychWylaczonyNeutralny_1    # Wybór pozytywny
${KlasProjUzasadnienieWplywNiepelnosprawni Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansNiepelnosprawnychOpis    # Uzasadnienie wpływu projektu na realizację zasady równości szans i niedyskryminacji, w tym dostępności dla osób z niepełnosprawnościami
${KlasProjCzyProduktyDlaNiepelnosprawnychTak Id}    id=klasyfikacja_projektu_produktyDostepneDlaNiepelnosprawnychWithNieDotyczy_0    # Wybór Tak
${KlasProjCzyProduktyDlaNiepelnosprawnychNie Id}    id=klasyfikacja_projektu_produktyDostepneDlaNiepelnosprawnychWithNieDotyczy_1    # Wybór Nie
${KlasProjCzyProduktyDlaNiepelnosprawnychNieDotyczy Id}    id=klasyfikacja_projektu_produktyDostepneDlaNiepelnosprawnychWithNieDotyczy_2    # Wybór Nie dotyczy
${KlasProjUzasadnienieDostepnosciProduktowDlaNiepelnosprawnych Id}    id=klasyfikacja_projektu_produktyDostepneDlaNiepelnosprawnychOpis    # Uzasadnienie dostępności produktów dla osób z niepełnosprawnościami
${KlasProjWplywRownoscSzansKobietIMezczyznNeutralny Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansPlci_0    # Wybór Neutralny
${KlasProjWplywRownoscSzansKobietIMezczyznPozytywny Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansPlci_1    # Wybór Pozytywny
${KlasProjUzasadnienieRownoscSzansKobietIMezczyzn XPath}    //textarea[contains(@id, 'klasyfikacja_projektu_wplywNaRownoscSzansPlci')]    # Uzasadnienie wpływu projektu na realizację zasady równości szans kobiet i mężczyzn
${KlasProjWplywZrownowazonyRozwojNeutralny Id}    id=klasyfikacja_projektu_wplywNaZrownowazonyRozwoj_1    # Wybór Neutralny
${KlasProjWplywZrownowazonyRozwojPozytywny Id}    id=klasyfikacja_projektu_wplywNaZrownowazonyRozwoj_2    # Wybór Pozytywny
${KlasProjUzasadnienieZrownowazonyRozwoj Id}    id=klasyfikacja_projektu_wplywNaZrownowazonyRozwojOpis    # Uzasadnienie wpływu projektu na realizację zasady zrównoważonego rozwoju
${KlasProjRodzDzialGosp Id}    //select[contains(@id, '_rodzajDzialalnosciGospodarczej') and contains(@name,'klasyfikacja_projektu')]    # Lista wyboru rodzaj działalności gospodarczej
${KlasProjTypObszaruRealizacji Id}    id=klasyfikacja_projektu_typObszaruRealizacji    # Lista wyboru typ obszaru realizacji
${KlasProjOpisProduktuRezultatProjektu Id}    id=klasyfikacja_projektu_opisProduktuRezultatu    # Opis produktu będącego rezultatem projektu
${KlasProjZnaczenieNowychCech Id}    id=klasyfikacja_projektu_znaczenieCechIFunkcjonalnosciProduktu    # Znaczenie nowych cech i funkcjonalności dla odbiorców produktu
${KlasProjPrzyciskDodajProdukt XPath}    //button[@data-bind="click: addKonkurencyjnoscProduktu"]    # Przycisk dodaj produkt
${KlasProjRynekDocelowy Id}    id=klasyfikacja_projektu_rynekDocelowy    # Rynek docelowy
${KlasProjZapotrzebowanieRynkoweNaProdukt Id}    id=klasyfikacja_projektu_zapotrzebowanieRynkowe    # Zapotrzebowanie rynkowe na produkt
${KlasProjDystrybucjaISprzedazProduktu Id}    id=klasyfikacja_projektu_dystrybucjaSprzedazProduktu    # Dystrybucja i sprzedaż produktu
${KlasProjPromocjaProduktu Id}    id=klasyfikacja_projektu_promocjaProduktu    # Promocja produktu
${KlasProjUzasadnieniePozWplywuNiepelnosprawni Id}    id=klasyfikacja_projektu_wplywNaRownoscSzansINiedyskryminacjiOpis    # Uzasadnienie pozytywnego wpływu projektu na realizację zasady równości szans i niedyskryminacji, w tym dostępności dla osób z niepełnosprawnościami
${KlasProjPrzyciskDodajProduktNiepelnosprawni XPath}    //button[@data-bind="click: addDostepnoscProduktu"]    # Dodaj - Dostępność produktów projektu dla osób z niepełnosprawnościami
${KlasProjKIS XPath}    //*[@id="klasyfikacja_projektu_projektDotyczyKis"]    # Projekt wpisuje się w Krajową Inteligentną Specjalizację (KIS)
${KlasProjObszarKIS XPath}    //*[@id="klasyfikacja_projektu_obszaryKis2019"]    # Obszar KIS, w który wpisuje się projekt
${KlasProjUzasadnienieWyboruKIS Id}    id=klasyfikacja_projektu_obszarKisOpis    # Uzasadnienie wybranego obszaru KIS, w który wpisuje się projekt
