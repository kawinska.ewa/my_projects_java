*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Osoba_do_kontaktow_roboczychUI.robot

*** Keywords ***
Wpisz Imie
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${imie}    FakerLibrary.First Name
    Input Text    ${ODKRImie Id}    ${imie}

Wpisz Nazwisko
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${nazwisko}    FakerLibrary.Last Name
    Input Text    ${ODKRNazwisko Id}    ${nazwisko}

Wpisz Stanowisko
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${ODKRStanowisko Id}    ${Podaj_Liczbe_Znakow}

Wpisz Instytucje
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${ODKRInstytucja Id}    ${Podaj_Liczbe_Znakow}

Wpisz Telefon
    [Documentation]    Wypełnia pole losową liczbą o długości 15.
    ${ODKRTelefon}    Generate Random String    15    chars=[NUMBERS]+
    Input Text    ${ODKRTelefon Id}    ${ODKRTelefon}

Wpisz Telefon Komorkowy
    [Documentation]    Wypełnia pole losową liczbą o długości 15.
    ${ODKRTelefon}    Generate Random String    15    chars=[NUMBERS]+
    Input Text    ${ODKRTelefonKom Id}    ${ODKRTelefon}

Wpisz Adres E-mail
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${ODKRE-mail}    Email    jaka.to.melodia.ru
    Input Text    ${ODKRE-mail Id}    ${ODKRE-mail}

Wpisz Fax
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${ODKRFax}    Generate Random String    15    chars=[NUMBERS]+
    Input Text    ${ODKRFax Id}    ${ODKRFax}
