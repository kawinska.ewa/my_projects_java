*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Wnioskodawca_informacje_ogolneUI.robot
Resource          Wnioskodawca_adres_korespondencyjnyUI.robot

*** Keywords ***
Nazwa wnioskodawcy
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia losowym tekstem pole ${WcaONazwaWnioskodawcy Id} zadaną liczbą znaków.
    Wypelnij Pole Losowym Tekstem    ${WcaONazwaWnioskodawcy Id}    ${Podaj_Liczbe_Znakow}

Status wnioskodawcy
    [Arguments]    ${lokator_StatusWnioskodawcy}    ${ile_opcji_do_wyboru}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${losuj}    Random Int    min=1    max=${ile_opcji_do_wyboru}
    ${wybor}    Convert To String    ${losuj}
    Select From List By Value    ${lokator_StatusWnioskodawcy}    ${wybor}

Data rozpoczecia dzialalnosci
    [Documentation]    Po wyczyszczeniu pola z maski uzupełnia pole datą bieżącą pomniejszoną o 250 dni.
    Click Element    ${WcaODataRozpoczeciaDzialalnosci Id}
    Clear Element Text    ${WcaODataRozpoczeciaDzialalnosci Id}
    ${rozpoczecie}=    Get Time    \    NOW -250d
    Input Text    ${WcaODataRozpoczeciaDzialalnosci Id}    ${rozpoczecie}
    Press Key    ${WcaODataRozpoczeciaDzialalnosci Id}    \\13    # symulacja klawisza Enter

Forma prawna wnioskodawcy
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${WcaOFormaPrawnaWnioskodawcy Xpath}

Forma wlasnosci
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${WcaOFormaWlasnosci Xpath}

NIP wnioskodawcy
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${wybor}=    Company Vat
    Input Text    ${WcaONipWnioskodawcy Id}    ${wybor}

REGON wnioskodawcy
    [Documentation]    Wypełnia pole używając biblioteki Faker.
    ${wybor}=    FakerLibrary.Regon
    Input Text    ${WcaORegon Id}    ${wybor}

Numer w KRS
    [Documentation]    Losuje liczbę z zadanego przedziału. Wykorzystanie słowa kluczowego "Losuj Liczbe Z Przedzialu". Wylosowana wartość zamieniana jest na string i wprowadzana w pole.
    ${krs}    Losuj Liczbe Z Przedzialu    1000000000    9999999999
    ${KRS}    Convert To String    ${krs}
    Input Text    ${WcaONumerKRS Id}    ${KRS}

Numer PKD przewazajacej dzialalnosci
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${WcaONumerKoduPKDPrzewazajacejDzialalnosci Xpath}

Mozliwosc odzyskania VAT
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${WcaOMozliowscOdzyskaniaVAT Xpath}

Uzasadnienie braku mozliwosci odzyskania VAT
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${WcaOUzasadnienieBrakuMozliwosciOdzyskaniaVAT Id}    ${Podaj_Liczbe_Znakow}

Adres siedziby wnioskodawcy
    [Documentation]    Wypełnia danymi część \ "Adres siedziby wnioskodawcy".
    Element Should Be Visible    ${WcaOAdresKrajPl Id}
    Wybierz element z listy bez 1 pozycji    ${WcaOAdresKraj Xpath}
    Element Should Be Visible    ${WcaOAdresWojewodztwo Id}
    Wybierz element z listy bez 1 pozycji    ${WcaOAdresWojewodztwo XPath}
    #powiat
    Click Element    ${WcaOAdresPowiat Id}
    Sleep    2s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${powiat}    Get Selected List Value    ${WcaOAdresPowiat XPath}
    #gmina
    Click Element    ${WcaOAdresGmina Id}
    Sleep    2s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${gmina}    Get Selected List Value    ${WcaOAdresGmina XPath}
    #miejscowość
    Click Element    ${WcaOAdresMiejscowoscMirId Id}
    Sleep    2s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${miejscowosc}    Get Selected List Value    ${WcaOAdresMiejscowosc XPath}
    #ulica
    Click Element    ${WcaOAdresUlicaMirId Id}
    Sleep    4s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${ulica}    Get Selected List Value    ${WcaOAdresUlica XPath}
    ${nrbudynku}    Random Number    digits=3
    Input Text    ${WcaOAdresNrBudynku Id}    ${nrbudynku}
    ${nrlokalu}    Random Number    digits=3
    Input Text    ${WcaOAdresNrLokalu Id}    ${nrlokalu}
    Clear Element Text    ${WcaOAdresKodPocztowy Id}
    Sleep    1s
    Set Focus To Element    ${WcaOAdresKodPocztowy Id}
    ${kodpocztowy}    Random Number    digits=5    fix_len==True
    Click Element    ${WcaOAdresKodPocztowy Id}
    Input Text    ${WcaOAdresKodPocztowy Id}    ${kodpocztowy}
    ${poczta}    FakerLibrary.City
    Input Text    ${WcaOAdresPoczta Id}    ${poczta}
    ${telefon}    FakerLibrary.Phone Number
    Input Text    ${WcaOAdresTelefon Id}    ${telefon}
    ${faks}    FakerLibrary.Phone Number
    ${Czy_jest_pole}    Run Keyword And Return Status    Element Should Be Visible    ${WcaOAdresFaks Id}
    Run Keyword If    ${Czy_jest_pole}    Input Text    ${WcaOAdresFaks Id}    ${faks}
    Input Text    ${WcaOAdresEmail Id}    maciej_rogulski@parp.gov.pl
    ${www}    Url
    Input Text    ${WcaOAdresWWW}    ${www}

Dodaj wspolnika
    [Arguments]    ${ile_razy}
    [Documentation]    Dodaje wspólnika tyle razy ile zostanie określone przez zmienną ${ile_razy}.
    ${Cyfra}    Set Variable    1
    : FOR    ${Wspolnik}    IN RANGE    0    ${ile_razy}
    \    Run Keyword If    '${ile_razy}'>'0'    Kliknij Przycisk    ${WcaODodajWspolnikaPrzycisk XPath}
    \    ...    ELSE    Exit For Loop
    \    ${WcaOWspolnikImie Id}    ${WcaOWspolnikNazwisko Id}    ${WcaOWspolnikNIP Id}    ${WcaOWspolnikWojewodztwo XPath}    ${WcaOWspolnikPowiat XPath}    ${WcaOWspolnikGmina XPath}
    \    ...    ${WcaOWspolnikUlica Id}    ${WcaOWspolnikNrBudynku Id}    ${WcaOWspolnikNrLokalu Id}    ${WcaOWspolnikKodPocztowy Id}    ${WcaOWspolnikPoczta Id}
    \    ...    ${WcaOWspolnikMiejscowosc Id}    ${WcaOWspolnikTelefon Id}    ${WcaOWspolnikFaks Id}    Wspolnicy lokatory    ${Cyfra}
    \    Wypelnij danymi wspolnika    ${WcaOWspolnikImie Id}    ${WcaOWspolnikNazwisko Id}    ${WcaOWspolnikNIP Id}    ${WcaOWspolnikWojewodztwo XPath}    ${WcaOWspolnikPowiat XPath}
    \    ...    ${WcaOWspolnikGmina XPath}    ${WcaOWspolnikUlica Id}    ${WcaOWspolnikNrBudynku Id}    ${WcaOWspolnikNrLokalu Id}    ${WcaOWspolnikKodPocztowy Id}
    \    ...    ${WcaOWspolnikPoczta Id}    ${WcaOWspolnikMiejscowosc Id}    ${WcaOWspolnikTelefon Id}    ${WcaOWspolnikFaks Id}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Wspolnicy lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Zawiera lokatory występujące w sekcji "Wspólnicy". W lokatorach przekazywana jest zmienna ${Cyfra}.
    ${WcaOWspolnikWojewodztwo XPath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-wnioskodawca_ogolne_wspolnicy___name___adres_wojewodztwo-container"])    [${Cyfra}]
    ${WcaOWspolnikPowiat XPath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-wnioskodawca_ogolne_wspolnicy___name___adres_powiat-container"])    [${Cyfra}]
    ${WcaOWspolnikGmina XPath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-wnioskodawca_ogolne_wspolnicy___name___adres_gmina-container"])    [${Cyfra}]
    ${Cyfra}    Evaluate    ${Cyfra}-1
    ${WcaOWspolnikImie Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _imie
    ${WcaOWspolnikNazwisko Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _nazwisko
    ${WcaOWspolnikNIP Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _nip
    ${WcaOWspolnikUlica Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_ulica
    ${WcaOWspolnikNrBudynku Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_nrBudynku
    ${WcaOWspolnikNrLokalu Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_nrLokalu
    ${WcaOWspolnikKodPocztowy Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_kodPocztowy
    ${WcaOWspolnikPoczta Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_poczta
    ${WcaOWspolnikMiejscowosc Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_miejscowosc
    ${WcaOWspolnikTelefon Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_telefon
    ${WcaOWspolnikFaks Id}    Catenate    SEPARATOR=    id=wnioskodawca_ogolne_wspolnicy_    ${Cyfra}    _adres_faks
    [Return]    ${WcaOWspolnikImie Id}    ${WcaOWspolnikNazwisko Id}    ${WcaOWspolnikNIP Id}    ${WcaOWspolnikWojewodztwo XPath}    ${WcaOWspolnikPowiat XPath}    ${WcaOWspolnikGmina XPath}
    ...    ${WcaOWspolnikUlica Id}    ${WcaOWspolnikNrBudynku Id}    ${WcaOWspolnikNrLokalu Id}    ${WcaOWspolnikKodPocztowy Id}    ${WcaOWspolnikPoczta Id}    ${WcaOWspolnikMiejscowosc Id}
    ...    ${WcaOWspolnikTelefon Id}    ${WcaOWspolnikFaks Id}

Wypelnij danymi wspolnika
    [Arguments]    ${WcaOWspolnikImie Id}    ${WcaOWspolnikNazwisko Id}    ${WcaOWspolnikNIP Id}    ${WcaOWspolnikWojewodztwo XPath}    ${WcaOWspolnikPowiat XPath}    ${WcaOWspolnikGmina XPath}
    ...    ${WcaOWspolnikUlica Id}    ${WcaOWspolnikNrBudynku Id}    ${WcaOWspolnikNrLokalu Id}    ${WcaOWspolnikKodPocztowy Id}    ${WcaOWspolnikPoczta Id}    ${WcaOWspolnikMiejscowosc Id}
    ...    ${WcaOWspolnikTelefon Id}    ${WcaOWspolnikFaks Id}
    [Documentation]    Wypełniane danymi pola sekcji "Wspólnicy".
    Set Focus To Element    ${WcaOWspolnikImie Id}
    ${imie}=    FakerLibrary.First Name
    Input Text    ${WcaOWspolnikImie Id}    ${imie}
    ${nazwisko}=    FakerLibrary.Last Name
    Input Text    ${WcaOWspolnikNazwisko Id}    ${nazwisko}
    ${wybor}=    Company Vat
    Input Text    ${WcaOWspolnikNIP Id}    ${wybor}
    Set Focus To Element    ${WcaOWspolnikWojewodztwo XPath}
    ${WcaOWspolnikWojewodztwoWartosc}=    Kliknij Dropdown i wybierz losową opcje    ${WcaOWspolnikWojewodztwo XPath}
    ${WcaOWspolnikPowiatWartosc}=    Kliknij Dropdown i wybierz losową opcje    ${WcaOWspolnikPowiat XPath}
    ${WcaOWspolnikGminaWartosc}=    Kliknij Dropdown i wybierz losową opcje    ${WcaOWspolnikGmina XPath}
    Set Focus To Element    ${WcaOWspolnikUlica Id}
    ${ulica}=    FakerLibrary.Street Name
    Input Text    ${WcaOWspolnikUlica Id}    ${ulica}
    ${nrbudynku}=    Random Number    digits=3
    Input Text    ${WcaOWspolnikNrBudynku Id}    ${nrbudynku}
    ${nrlokalu}=    Random Number    digits=3
    Input Text    ${WcaOWspolnikNrLokalu Id}    ${nrlokalu}
    Clear Element Text    ${WcaOAdresKodPocztowy Id}
    Sleep    1s
    ${kodpocztowy}=    Random Number    digits=5    fix_len==True
    Input Text    ${WcaOWspolnikKodPocztowy Id}    ${kodpocztowy}
    ${poczta}=    FakerLibrary.City
    Input Text    ${WcaOWspolnikPoczta Id}    ${poczta}
    ${miejscowosc}=    FakerLibrary.City
    Input Text    ${WcaOWspolnikMiejscowosc Id}    ${miejscowosc}
    ${telefon}=    FakerLibrary.Phone Number
    Input Text    ${WcaOWspolnikTelefon Id}    ${telefon}
    ${faks}=    FakerLibrary.Phone Number
    Input Text    ${WcaOWspolnikFaks Id}    ${faks}

Imie i nazwisko pomyslodawcy
    [Documentation]    Wypełnia losowym tekstem pole ${WcaONazwaWnioskodawcy Id} zadaną liczbą znaków.
    ${imie}    FakerLibrary.First Name
    ${nazwisko}    FakerLibrary.Last Name
    ${imie_nazwisko}=  Catenate  ${imie}  ${nazwisko}
    Input Text  ${WcaONazwaWnioskodawcy Id}  ${imie_nazwisko}

Pesel pomyslodawcy
    [Documentation]    Wypełnia losowym tekstem pole ${WcaOPesel Id} poprawnym nr PESEL.
    ${lista}=  Create List  54120997265  31072284032  45111837419  00252137897  78051203710
    Input Text    ${WcaOPesel Id}  ${lista}

Telefon pomyslodawcy
   [Documentation]    Wypełnia nr telefonu pole ${WcaAdresKorespTelefon Id}.
   ${telefon}=    FakerLibrary.Phone Number
   Input Text    ${WcaAdresKorespTelefon Id}    ${telefon}

E-mail pomyslodawcy
    [Documentation]    Wypełnia adresem e-mail pole ${WcaAdresKorespEmail Id}.
    Input Text    ${WcaAdresKorespEmail Id}   testy@parp.gov.pl

Dane czlonka zespolu realizujacego pomysl
    [Documentation]    Wypełnia cala sekcje "Zespol realizujacy pomysl"  danymi z zadaną liczbą znaków.
    ${imie}    FakerLibrary.First Name
    ${nazwisko}    FakerLibrary.Last Name
    ${imie_nazwisko}=  Catenate  ${imie}  ${nazwisko}
    Input Text  ${ImieNazwiskoCzlonkaZespolu Id}  ${imie_nazwisko}
    Input Text  ${RolaWProjekcie Id}  Test
    Input Text  ${DoswiadczenieWProjekcie Id}  Wyzsze
    Input Text  ${CzlonekZespoluPesel Id}  40061721601
    ${telefon}=  FakerLibrary.Phone Number
    Input Text  ${CzlonekZespoluTelefon Id}   ${telefon}
    Input Text  ${CzlonekZespoluEMail id}  test@parp.gov.pl
