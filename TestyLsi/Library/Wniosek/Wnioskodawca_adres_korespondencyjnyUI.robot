*** Variables ***
${WcaAdresKorespKraj XPath}    //*[@id='wnioskodawca_adres_korespondencyjny_koresp_krajPl']    # Kraj - lista wyboru
${WcaAdresKorespWojewodztwo XPath}    //*[@id='wnioskodawca_adres_korespondencyjny_koresp_wojewodztwo']    # Wojewodztwo - lista wyboru
${WcaAdresKorespPowiat XPath}    //*[@id='wnioskodawca_adres_korespondencyjny_koresp_powiat']    # Powiat - lista wyboru
${WcaAdresKorespGmina XPath}    //*[@id='wnioskodawca_adres_korespondencyjny_koresp_gmina']    # Gmina - lista wyboru
${WcaAdresKorespKraj id}    id=wnioskodawca_adres_korespondencyjny_koresp_krajPl    # Kraj
${WcaAdresKorespPowiat Id}    id=select2-wnioskodawca_adres_korespondencyjny_koresp_powiat-container    # Powiat
${WcaAdresKorespGmina Id}    id=select2-wnioskodawca_adres_korespondencyjny_koresp_gmina-container    # Gmina
${WcaAdresKorespUlica Id}    id=wnioskodawca_adres_korespondencyjny_koresp_ulica    # Ulica
${WcaAdresKorespNrBudynku Id}    id=wnioskodawca_adres_korespondencyjny_koresp_nrBudynku    # Nr budynku
${WcaAdresKorespNrLokalu Id}    id=wnioskodawca_adres_korespondencyjny_koresp_nrLokalu    # Nr lokalu
${WcaAdresKorespKodPocztowy Id}    id=wnioskodawca_adres_korespondencyjny_koresp_kodPocztowy    # Kod pocztowy
${WcaAdresKorespPoczta Id}    id=wnioskodawca_adres_korespondencyjny_koresp_poczta    # Poczta
${WcaAdresKorespMiejscowosc Id}    id=wnioskodawca_adres_korespondencyjny_koresp_miejscowosc    # Miejscowość
${WcaAdresKorespTelefon Id}    id=wnioskodawca_adres_korespondencyjny_koresp_telefon    # Telefon
${WcaAdresKorespFaks Id}    id=wnioskodawca_adres_korespondencyjny_koresp_faks    # Faks
${WcaAdresKorespEmail Id}    id=wnioskodawca_adres_korespondencyjny_koresp_adresEmail    # Email
${AdresPomyslKraj Id}  id=wykaz_partnerow_partnerzy_0_siedziba_krajSf2  # Kraj w adresie pomyslodawcy
${AdresPomyslWojewodztwo Id}  id=wykaz_partnerow_partnerzy_0_siedziba_wojewodztwo  #Wojewodztwo w adresie pomyslodawcy
${MiejscowoscPomysl Id}  id=wykaz_partnerow_partnerzy_0_siedziba_miejscowosc  #Miejscowosc
${DodajAdresPomysl}  //button[@data-bind='click: addPartner']  # Dodaj adres
${UlicaPomysl Id}  id=wykaz_partnerow_partnerzy_0_siedziba_ulica  #Ulica pomyslodawca
${NrBudynkuPomysl Id}  id=wykaz_partnerow_partnerzy_0_siedziba_nrBudynku  #Nr budynku pomyslodawca
${NrLokaluPomysl Id}  id=wykaz_partnerow_partnerzy_0_siedziba_nrLokalu  #Nr lokalu pomyslodawca
${KodPocztowyPomysl Id}  id=wykaz_partnerow_partnerzy_0_siedziba_kodPocztowy  #Kod pocztowy pomyslodawca