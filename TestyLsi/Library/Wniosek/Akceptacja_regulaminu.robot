*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL

*** Variables ***
${AkceptacjaRegulaminu Id}    id=akceptacja_regulaminu_akceptacja    # Akceptacja regulaminu

*** Keywords ***
Akceptuj regulamin
    [Documentation]    Akceptuje regulamin konkursu, wybiera opcję TAK.
    Select From List By Value    ${AkceptacjaRegulaminu Id}    1
