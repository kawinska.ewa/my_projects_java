*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../Resource.robot

*** Variables ***
${OknoWalidacji XPath}    xpath=//*[@id="modal"]/div/div/div[2]/table    # Okno walidcji
${ModalZlozWniosek Tresc2}    Po złożeniu wniosku, w przypadku pozytywnej walidacji, wniosek zostanie zablokowany do edycji.    # Informacja przed złożeniem wniosku
${AlertZlozenieNiePowiodloSie Tresc}    Złożenie wniosku nie powiodło się. Wniosek posiada błędy walidacyjne, które uniemożliwiają złożenie wniosku. Błędy widoczne są po kliknięciu na przycisk "Sprawdź poprawność".​    # Komunikat po nieudanej próbie złożenia wniosku
${AlertZlozeniePowiodloSie XPath}    //*[@class="alert alert-warning alert-dismissible"]    # Okno komunikatu po złozeniu wniosku

*** Keywords ***
Waliduj wniosek
    [Documentation]    Otwiera okno walidacji wniosku
    Click Element    ${SprawdzPoprawnosc Id}
    Wait Until Element Is Visible    ${ElementListyWalidacja Id}    10s

Walidacja nie powinna wyswietlac
    [Arguments]    ${text}
    [Documentation]    Sprawdza czy w oknie walidacji nie ma tekstu określonego zmienną ${tekst}
    ${OknoWalidacji} =    Get Text    ${OknoWalidacji XPath}
    Should Not Contain Match    ${OknoWalidacji}    ${text}

Zarejestruj wniosek
    [Documentation]    Złożenie wniosku: 1. Kliknij w przycisk "Złóż". 2. Czekaj na modal z oczekiwaną treścią. 3. Kliknij przycisk "Złóż wniosek". 4. Na stronie powinien być widoczny komunikat o zarejestrowaniu wniosku. 5. Pobierz tekst z komunikatu.
    Click Element    ${ZlozWniosek Id}
    Wait Until Element Is Visible    ${ModalZlozWniosek Id}
    Wait Until Element Is Visible    ${ModalZlozWniosekH4 CSS}
    Element Should Contain    ${ModalZlozWniosek Id}    ${ModalZlozWniosekH4Tresc}
    Wait Until Element Is Visible    ${ModalZlozWniosekPrzycisk XPath}
    Element Should Contain    ${ModalZlozWniosek Id}    ${ModalZlozWniosek Tresc2}
    Click Element    ${ModalZlozWniosekPrzycisk XPath}
    Wait Until Element Is Visible    ${AlertZlozeniePowiodloSie XPath}
    Element Should Contain    ${AlertZlozeniePowiodloSie XPath}    Wniosek został zarejestrowany
    ${TekstKomunikatu}    Get Text    ${AlertZlozeniePowiodloSie XPath}
