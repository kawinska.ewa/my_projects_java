*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot

*** Variables ***
${LiczbaOswiadczen Xpath}    xpath=//*[starts-with(@id,'oswiadczenia_sekcje')]
${PodstawaPrawna Id}    id=oswiadczenia_podstawaPrawnaUstawa
${PodstawaPrawnaOpis Id}    id=oswiadczenia_podstawaPrawnaInneOpis

*** Keywords ***
Zaznacz Losowo Oswiadczenia
    [Documentation]    Wypełnia oświadczenia.
    ${Cyfra}    Evaluate    0
    @{ListaXpath}=    Create List
    : FOR    ${wiersz}    IN RANGE    0    20
    \    ${Oswiadczenie}    Lokatory Oswiadczenia    ${Cyfra}
    \    ${OswiadczenieIstnieje}    Run Keyword And Return Status    Page Should Contain Checkbox    ${Oswiadczenie}
    \    Run Keyword If    ${OswiadczenieIstnieje}    Dodaj do listy    ${Oswiadczenie}    ${ListaXpath}
    \    ...    ELSE    Exit For Loop
    \    ${Cyfra}    Evaluate    ${Cyfra}+1
    ${LiczbaIstniejacychOswiadczen}=    Get Length    ${ListaXpath}
    Log Many    ${ListaXpath}
    ${wybraneOswiadczenia}    Wybierz Losowo Oswiadczenia    ${LiczbaIstniejacychOswiadczen}    ${ListaXpath}
    : FOR    ${i}    IN    @{wybraneOswiadczenia}
    \    Select Checkbox    ${i}

Lokatory Oswiadczenia
    [Arguments]    ${Cyfra}
    [Documentation]    Przechowuje lokator oświadczeń.
    ${Oswiadczenie}    Catenate    SEPARATOR=    xpath=(//*[@id="oswiadczenia_sekcje_    ${Cyfra}    _zaznaczono"])
    [Return]    ${Oswiadczenie}

Dodaj do listy
    [Arguments]    ${Oswiadczenie}    ${ListaXpath}
    [Documentation]    Dodaje oświadczenie do listy.
    Append To List    ${ListaXpath}    ${Oswiadczenie}

Wybierz Losowo Oswiadczenia
    [Arguments]    ${LiczbaIstniejacychOswiadczen}    ${ListaXpath}
    [Documentation]    Wybiera losowo oświadczenia.
    ${LiczbaOswiadczenDoZaznaczenia}    Random Int    min=0    max=${LiczbaIstniejacychOswiadczen}
    @{wybraneOswiadczenia}    Random Elements    elements=${ListaXpath}    length= ${LiczbaOswiadczenDoZaznaczenia}    unique=True
    Log Many    ${wybraneOswiadczenia}
    [Return]    ${wybraneOswiadczenia}

Wypelnij Podstawe Prawna
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Select Checkbox    ${PodstawaPrawna Id}
    Wypelnij Pole Losowym Tekstem    ${PodstawaPrawnaOpis Id}    ${Podaj_Liczbe_Znakow}
