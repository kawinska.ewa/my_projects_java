*** Variables ***
${IDKKKwnioskodawcaJestkoordynatorem XPath}    //*[@id="informacje_kkk_czyKoordynatorKlastra"]    # Wnioskodawca jest koordynatorem klastra posiadającego status Krajowego Klastra Kluczowego
${IDKKKnazwaKKKkoordynatorWnioskodawca Id}    id=informacje_kkk_nazwa    # Nazwa Krajowego Klastra Kluczowego, którego koordynatorem jest wnioskodawca
${IDKKKstronaWWW Id}    id=informacje_kkk_adresWWW    # Adres strony internetowej
${IDKKKdataUzyskaniaStatusuKKK Id}    id=informacje_kkk_dataUzyskaniaStatusuKKK    # Data uzyskania statusu Krajowego Klastra Kluczowego
${IDKKKdataObowiazywaniaStatusuKKK Id}    id=informacje_kkk_dataObowiazywaniaStatusuKKK    # Data obowiązywania statusu Krajowego Klastra Kluczowego
${IDKKKspecjalizacjaKlastra Id}    id=informacje_kkk_specjalizacja    # Specjalizacja klastra
${IDKKKglowneWyrobyUslugi Id}    id=informacje_kkk_produkowaneWyrobyUslugi    # Główne produkowane wyroby, usługi
${IDKKKliczbaPodmiotowKKK Id}    id=informacje_kkk_liczbaPodmiotow    # Liczba podmiotów wchodzących w skład Krajowego Klastra Kluczowego
${IDKKKliczbaPrzedsiebiorstwKKK Id}    id=informacje_kkk_liczbaPodmiotowZaangazowanychOgolem    # Liczba przedsiębiorstw wchodzących w skład Krajowego Klastra Kluczowego bezpośrednio zaangażowanych w projekt, w tym:
${IDKKKliczbaMikroiMalych Id}    id=informacje_kkk_liczbaPodmiotowZaangazowanychMikroMale    # Liczba mikro i małych przedsiębiorstw
${IDKKKliczbaSrednich Id}    id=informacje_kkk_liczbaPodmiotowZaangazowanychSrednie    # Liczba średnich przedsiębiorstw
${IDKKKliczbaDuzych Id}    id=informacje_kkk_liczbaPodmiotowZaangazowanychDuze    # Liczba dużych przedsiębiorstw
