*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Wnioskodawca_adres_korespondencyjnyUI.robot

*** Keywords ***
Adres korespondencyjny
    [Documentation]    Wypełnia danymi sekcję "Wnioskodawca - Adres korespondencyjny".
    Element Should Be Visible    ${WcaAdresKorespKraj id}
    Wybierz element z listy bez 1 pozycji    ${WcaAdresKorespKraj XPath}
    Wybierz element z listy bez 1 pozycji    ${WcaAdresKorespWojewodztwo XPath}
    #powiat
    Click Element    ${WcaAdresKorespPowiat Id}
    Sleep    2s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${powiat}=    Get Selected List Value    ${WcaAdresKorespPowiat XPath}
    Log    ${powiat}
    #gmina
    Click Element    ${WcaAdresKorespGmina Id}
    Sleep    2s
    Press Key    ${PoleSzukajWLiscie XPath}    \\13
    ${gmina}=    Get Selected List Value    ${WcaAdresKorespGmina XPath}
    Log    ${gmina}
    #ulica
    ${ulica}=    FakerLibrary.Street Name
    Input Text    ${WcaAdresKorespUlica Id}    ${ulica}
    # nr budynku
    ${nrbudynku}=    Random Number    digits=3
    Input Text    ${WcaAdresKorespNrBudynku Id}    ${nrbudynku}
    # nr lokalu
    ${nrlokalu}=    Random Number    digits=3
    Input Text    ${WcaAdresKorespNrLokalu Id}    ${nrlokalu}
    # kod pocztowy
    Clear Element Text    ${WcaAdresKorespKodPocztowy Id}
    Sleep    1s
    ${kodpocztowy}=    Random Number    digits=5    fix_len==True
    Input Text    ${WcaAdresKorespKodPocztowy Id}    ${kodpocztowy}
    # poczta
    ${poczta}=    FakerLibrary.City
    Input Text    ${WcaAdresKorespPoczta Id}    ${poczta}
    #miejscowość
    ${miejscowosc}=    FakerLibrary.City
    Input Text    ${WcaAdresKorespMiejscowosc Id}    ${miejscowosc}
    # telefon
    ${telefon}=    FakerLibrary.Phone Number
    Input Text    ${WcaAdresKorespTelefon Id}    ${telefon}
    # faks
    ${Czy_jest_pole}    Run Keyword And Return Status    Element Should Be Visible    ${WcaAdresKorespFaks Id}
    ${faks}    FakerLibrary.Phone Number
    Run Keyword If    ${Czy_jest_pole}    Input Text    ${WcaAdresKorespFaks Id}    ${faks}
    # email
    Input Text    ${WcaAdresKorespEmail Id}    maciej_rogulski@parp.gov.pl

Adres pomyslodawcy
    [Documentation]    Wypełnia danymi sekcję "Adres pomyslodawcy".
    Element Should Be Visible  ${AdresPomyslKraj Id}
    Wybierz element z listy bez 1 pozycji  ${AdresPomyslKraj Id}
    Wybierz element z listy bez 1 pozycji  ${AdresPomyslWojewodztwo Id}
    ${ulica}=    FakerLibrary.Street Name
    Input Text    ${UlicaPomysl Id}    ${ulica}
    ${nrbudynku}=    Random Number    digits=3
    Input Text    ${NrBudynkuPomysl Id}    ${nrbudynku}
    ${nrlokalu}=    Random Number    digits=3
    Input Text    ${NrLokaluPomysl Id}    ${nrlokalu}
    Clear Element Text    ${KodPocztowyPomysl Id}
    Sleep    1s
    ${kodpocztowy}=    Random Number    digits=5    fix_len==True
    Input Text    ${KodPocztowyPomysl Id}    ${kodpocztowy}
    ${miejscowosc}=    FakerLibrary.City
    Input Text  ${MiejscowoscPomysl Id}  ${miejscowosc}

Dodaj adres pomyslodawcy
    [Documentation]    Przycisk dodaj miejsce realizacji projektu.
    Set Focus To Element    ${DodajAdresPomysl}
    Click Button    ${DodajAdresPomysl}
