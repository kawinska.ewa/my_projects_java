*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Szczegolowy_opis_projektuUI.robot

*** Keywords ***
Glowne zalozenia planu dzialan
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPglowneZalozeniaPlanuDzialan Id}    ${Podaj_Liczbe_Znakow}

Opis celow projektu internacjonalizacja
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisCelowProjektuInternacjonalizacja Id}    ${Podaj_Liczbe_Znakow}

Opis celow projektu BR
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisProjektuDzialalnoscBR Id}    ${Podaj_Liczbe_Znakow}

Czy projekt odpowiada specjalizacji klastra
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPczyProjektOdpowiadaSpecjalizacjiKlastra XPath}

Uzasadnienie zgodnosci ze specjalizacja klastra
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPuzasadnienieZgodnosciZeSpecjalizacja Id}    ${Podaj_Liczbe_Znakow}

Opis zaplanowanych uslug internacjonalizacja
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisZaplanowanychUslugInternacjonalizacja Id}    ${Podaj_Liczbe_Znakow}

Charakterystyka odbiorcow uslug internacjonalizacja
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPcharakterystykaOdbiorcowUslug Id}    ${Podaj_Liczbe_Znakow}

Czy projekt przewiduje wykorzystanie wynikow BR
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    ${wybrany_element}    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieWynikowBR XPath}
    [Return]    ${wybrany_element}

Czy wykorzystanie wynikow BR wlasnych
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieWynikowBRwlasnych XPath}

Czy wykorzystanie wynikow BR krajowych
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieWynikowBRkrajowych XPath}

Czy wykorzystanie wynikow BR zagranicznych
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieWynikowBRzagranicznych XPath}

Opis wykorzystania wynikow prac BR
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisWykorzystaniaWynikowPracBR Id}    ${Podaj_Liczbe_Znakow}

Czy wykorzystanie zaplecza BR
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    ${wybrany_element}    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieSpecjalistycznegoZapleczaBR XPath}
    [Return]    ${wybrany_element}

Czy wykorzystanie zaplecza BR wlasnego
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieSpecjalistycznegoZapleczaBRwlasnego XPath}

Czy wykorzystanie zaplecza BR krajowego
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieSpecjalistycznegoZapleczaBRkrajowego XPath}

Czy wykorzystanie zaplecza BR zagranicznego
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPwykorzystanieSpecjalistycznegoZapleczaBRzagranicznego XPath}

Opis wykorzystania zaplecza BR
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisWykorzystaniaZapleczaBR Id}    ${Podaj_Liczbe_Znakow}

Czy zostanie opracowany innowacyjny produkt technologia usluga
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPczyOpracowanyInnowacyjnyProdTechU XPath}

Opis innowacyjnego produktu technologii uslugi
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisInnowacyjnegoProdTechU Id}    ${Podaj_Liczbe_Znakow}

Czy analiza zapotrzebowania
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPczyAnalizaZapotrzebowania XPath}

Opis analizy
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisAnalizy Id}    ${Podaj_Liczbe_Znakow}

Czy innowacyjny produkt technologia usluga na nowy rynek
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPczyInnowacyjnyProdTechUnaNowyRynek XPath}

Opis wprowadzenia produktu technologii uslugi na nowy rynek
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisWprowadzeniaNaNowyRynek Id}    ${Podaj_Liczbe_Znakow}

Opis efektow podniesienia konkurencyjnosci
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisEfektowKonkurencyjnoscPrzedsiebiorstw Id}    ${Podaj_Liczbe_Znakow}

Czy podmioty klastra zyskaja nowego odbiorce
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${SOPczyNowyOdbiorca XPath}

Opis efektow internacjonalizacji czlonkow klastra
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPefektyInternacjonalizacjiCzlonkowKlastra Id}    ${Podaj_Liczbe_Znakow}

Opis wzmocnienia wspolpracy w ramach klastra
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPefektyWzmocnieniaWspolpracy Id}    ${Podaj_Liczbe_Znakow}

Opis ryzyk w zakresie internacjonalizacji
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisRyzykInternacjonalizacja Id}    ${Podaj_Liczbe_Znakow}

Opis ryzyk w zakresie wskaznikow projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPopisRyzykWskaznikiProjektu Id}    ${Podaj_Liczbe_Znakow}

Opis innowacyjnego pomyslu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisInnowacyjnegoPomyslu Id}    ${Podaj_Liczbe_Znakow}

Opis problemow ktore rozwiazuje produkt potrzeby rynkowej
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisProblemowKtoreRozwProdPotrzebyRynk Id}    ${Podaj_Liczbe_Znakow}

Opis rozwoju przedsiewziecia w perspektywie 2 lat oraz 5 lat
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisRozwoujPrzedsiewziecia2Oraz5Lat Id}    ${Podaj_Liczbe_Znakow}

Glowne zrodla przychodow
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPGlowneZrodlaPrzychodow Id}    ${Podaj_Liczbe_Znakow}

Opis konkurencji
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisKonkurencji Id}    ${Podaj_Liczbe_Znakow}

Charakterystyka niszy rynkowej lub konkretnego segmentu klientow
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPCharakterystykaNiszyRynkowej Id}    ${Podaj_Liczbe_Znakow}

Opis etapu rozwoju pomyslu -poziom gotowosci technologicznej
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisEtapuRozwojuPomyslu Id}    ${Podaj_Liczbe_Znakow}

Opis planowanych kosztow wprowadzenia produktu na rynek wraz z ich orientacyjna wysokoscia
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisPlanowanychKosztowWprowadzeniaProduktu Id}    ${Podaj_Liczbe_Znakow}

Opis zasobow niezbednych do wdrozenia pomyslu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPOpisZasobowNiezbednychDoWdrozeniaPomyslu Id}    ${Podaj_Liczbe_Znakow}

Glowne bariery i ograniczenia powodujacy aktualny brak realizacji przedsiewziecia
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${SOPGlowneBarieryIograniczenia Id}    ${Podaj_Liczbe_Znakow}