*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Miejsce_realizacji_projektuUI.robot

*** Keywords ***
Wypelnij sekcje MRP
    [Arguments]    ${MRPWojewodztwo Xpath}    ${MRPPowiat Xpath}    ${MRPGmina Xpath}    ${MRPPodregion Xpath}    ${MRPMiejscowosc Xpath}    ${MRPUlica Xpath}
    ...    ${MRPBudynek Id}    ${MRPLokal Id}    ${MRPKod Id}    ${MRPTytulPrawny Id}
    [Documentation]    Wypełnia danymi pola sekcji miejsce realizacji projektu.
    Set Focus To Element    ${MRPWojewodztwo Xpath}
    ${MRPWojewodztwo} =    Kliknij Województwo i wybierz z Polska Wschodnia    ${MRPWojewodztwo Xpath}
    ${MRPPowiat} =    Kliknij Dropdown i wybierz losową opcje    ${MRPPowiat Xpath}
    ${MRPGmina} =    Kliknij Dropdown i wybierz losową opcje    ${MRPGmina Xpath}
    ${MRPPodregion} =    Kliknij Dropdown i wybierz losową opcje    ${MRPPodregion Xpath}
    ${MRPMiejscowosc} =    Kliknij Dropdown i wybierz losową opcje    ${MRPMiejscowosc Xpath}
    ${MRPUlica} =    Kliknij Dropdown i wybierz losową opcje    ${MRPUlica Xpath}
    ${MRPBudynek} =    Random Number    2    True
    Input Text    ${MRPBudynek Id}    ${MRPBudynek}
    ${MiejsceRealizacjiProjektuNrLokaluWartosc} =    Random Number    2    True
    Input Text    ${MRPLokal Id}    ${MiejsceRealizacjiProjektuNrLokaluWartosc}
    ${MiejsceRealizacjiProjektuKodPocztowyWartosc} =    Random Number    5    True
    Input Text    ${MRPKod Id}    ${MiejsceRealizacjiProjektuKodPocztowyWartosc}
    ${MiejsceRealizacjiProjektuTytulPrawnyWartosc} =    Text    30
    ${Czy_jest_pole}    Run Keyword And Return Status    Element Should Be Visible    ${MRPTytulPrawny Id}
    Run Keyword If    ${Czy_jest_pole}    Input Text    ${MRPTytulPrawny Id}    ${MiejsceRealizacjiProjektuTytulPrawnyWartosc}

Dodaj i Wypelnij MRP
    [Arguments]    ${LiczbaMRP}
    [Documentation]    Dodaje miejsca realizacji projektu w ilości określonej przez testera i wypełnia pola danymi. Losowo zaznacza wśród dodanych miejsc realizacji siedzibę główną.
    ${Cyfra}    Set Variable    1
    : FOR    ${MRP}    IN RANGE    0    ${LiczbaMRP}
    \    Dodaj MRP
    \    ${MRPWojewodztwo Xpath}    ${MRPPowiat Xpath}    ${MRPGmina Xpath}    ${MRPPodregion Xpath}    ${MRPMiejscowosc Xpath}    ${MRPUlica Xpath}
    \    ...    ${MRPBudynek Id}    ${MRPLokal Id}    ${MRPKod Id}    ${MRPTytulPrawny Id}    Lokatory
    \    ...    ${Cyfra}
    \    Wypelnij sekcje MRP    ${MRPWojewodztwo Xpath}    ${MRPPowiat Xpath}    ${MRPGmina Xpath}    ${MRPPodregion Xpath}    ${MRPMiejscowosc Xpath}
    \    ...    ${MRPUlica Xpath}    ${MRPBudynek Id}    ${MRPLokal Id}    ${MRPKod Id}    ${MRPTytulPrawny Id}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1
    Zaznacz losowo glowna siedzibe    ${LiczbaMRP}

Zaznacz losowo glowna siedzibe
    [Arguments]    ${LiczbaMRP}
    [Documentation]    Wypełnia losowo główną siedzibę.
    ${LiczbaMRP}    Evaluate    ${LiczbaMRP}-1
    ${Cyfra}    Random Int    0    ${LiczbaMRP}
    ${Locator}    Catenate    SEPARATOR=    id=miejsca_realizacji_projektu_miejscaRealizacjiProjektu_    ${Cyfra}    _glownaLokalizacja
    Set Focus To Element    ${Locator}
    Click Javascript Id    ${Locator}

Dodaj MRP
    [Documentation]    Przycisk dodaj miejsce realizacji projektu.
    Set Focus To Element    ${MRPDodaj Xpath}
    Click Button    ${MRPDodaj Xpath}

Lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Lokatory miejsca realizacji projektu.
    ${MRPWojewodztwo Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___jednostkaTerytorialna_wojewodztwo-container"])[    ${Cyfra}    ]
    ${MRPPowiat Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___jednostkaTerytorialna_powiat-container"])[    ${Cyfra}    ]
    ${MRPGmina Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___jednostkaTerytorialna_gmina-container"])[    ${Cyfra}    ]
    ${MRPPodregion Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___jednostkaTerytorialna_podregion-container"])[    ${Cyfra}    ]
    ${MRPMiejscowosc Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___miejscowoscMirId-container"])[    ${Cyfra}    ]
    ${MRPUlica Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="select2-miejsca_realizacji_projektu_miejscaRealizacjiProjektu___name___ulicaMirId-container"])[    ${Cyfra}    ]
    ${Cyfra}    Evaluate    ${Cyfra}-1
    ${MRPBudynek Id}    Catenate    SEPARATOR=    id=miejsca_realizacji_projektu_miejscaRealizacjiProjektu_    ${Cyfra}    _nrBudynku
    ${MRPLokal Id}    Catenate    SEPARATOR=    id=miejsca_realizacji_projektu_miejscaRealizacjiProjektu_    ${Cyfra}    _nrLokalu
    ${MRPKod Id}    Catenate    SEPARATOR=    id=miejsca_realizacji_projektu_miejscaRealizacjiProjektu_    ${Cyfra}    _kodPocztowy
    ${MRPTytulPrawny Id}    Catenate    SEPARATOR=    id=miejsca_realizacji_projektu_miejscaRealizacjiProjektu_    ${Cyfra}    _tytulPrawny
    [Return]    ${MRPWojewodztwo Xpath}    ${MRPPowiat Xpath}    ${MRPGmina Xpath}    ${MRPPodregion Xpath}    ${MRPMiejscowosc Xpath}    ${MRPUlica Xpath}
    ...    ${MRPBudynek Id}    ${MRPLokal Id}    ${MRPKod Id}    ${MRPTytulPrawny Id}



