*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot

*** Variables ***
${WtabelaWskaznikiRezultatu XPath}    //*[@id="tabela_wskazniki_rezultatu"]/tr    # Wskaźniki rezultatu
${WtabelaWskaznikiProduktu XPath}    //*[@id="tabela_wskazniki_produktu"]/tr    # Wskaźniki produktu

*** Keywords ***
Wskazniki Lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Przechowuje lokatory do wskaźników.
    ${WskazRokBazowy Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="wykaz_wskaznikow_wniosku_wskaznikiWniosku_    ${Cyfra}    _rokBazowy"])
    ${WskazWartoscBazowa Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="wykaz_wskaznikow_wniosku_wskaznikiWniosku_    ${Cyfra}    _wartoscBazowa"])
    ${WskazRokDocelowy Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="wykaz_wskaznikow_wniosku_wskaznikiWniosku_    ${Cyfra}    _rokDocelowy"])
    ${WskazWartoscDocelowa Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="wykaz_wskaznikow_wniosku_wskaznikiWniosku_    ${Cyfra}    _wartoscDocelowa"])
    ${WskazMetodWeryfikacji Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="wykaz_wskaznikow_wniosku_wskaznikiWniosku_    ${Cyfra}    _metodologiaIWeryfikacja"])
    [Return]    ${WskazRokBazowy Xpath}    ${WskazWartoscBazowa Xpath}    ${WskazRokDocelowy Xpath}    ${WskazWartoscDocelowa Xpath}    ${WskazMetodWeryfikacji Xpath}

Wpisz rok docelowy
    [Arguments]    ${WskazRokDocelowy Xpath}
    [Documentation]    Wypełnia pole wartością roku bieżącego zwiększoną o 1.
    ${Rok_Biezacy}    Get Current Date    result_format=%Y
    ${Rok_Biezacy_Powiekszony}    Evaluate    ${Rok_Biezacy}+1
    ${Rok_Docelowy}    Convert To String    ${Rok_Biezacy_Powiekszony}
    Select From List By Value    ${WskazRokDocelowy Xpath}    ${Rok_Docelowy}

Wpisz wartosc docelowa
    [Arguments]    ${WskazWartoscDocelowa Xpath}
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=99
    Input Text    ${WskazWartoscDocelowa Xpath}    ${Wartosc}00

Wpisz Metode Weryfikacji
    [Arguments]    ${WskazMetodWeryfikacji Xpath}    ${Podaj_Liczbe_Znakow_Metoda}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    ${Metoda}    Generate Random String    ${Podaj_Liczbe_Znakow_Metoda}    chars=[LETTERS][NUMBERS] `~!@#$%^&*()_+-={}|[]\:";'<>?,./ęóąśłżźćń
    Input Text    ${WskazMetodWeryfikacji Xpath}    ${Metoda}

Wypelnij wskazniki rezultatu
    [Arguments]    ${KolejnyNumerWiersza}    ${Podaj_Liczbe_Znakow_Metoda}    # numeracja w id'kach wierszy jest wspólna z tabelą Wskaźniki produktu.
    [Documentation]    Wypełnia wskaźniki rezultatu w polach, które są do wypełnienia. Uwaga, numeracja lokatorów dla wskaźników rezultatu jest kontynuowana z numeracji lokatorów wskaźników produktu.
    ${KolejnyNumerWiersza}    Evaluate    ${KolejnyNumerWiersza}-1    # odejmuję 1, ponieważ numeracja w id'kach zaczyna się od 0, nie od 1
    ${ilosc_wierszy}    Get Element Count    ${WtabelaWskaznikiRezultatu XPath}    # liczy ilość wierszy w tabeli
    : FOR    ${wiersz}    IN RANGE    0    ${ilosc_wierszy}
    \    ${WskazRokBazowy Xpath}    ${WskazWartoscBazowa Xpath}    ${WskazRokDocelowy Xpath}    ${WskazWartoscDocelowa Xpath}    ${WskazMetodWeryfikacji Xpath}    Wskazniki Lokatory
    \    ...    ${KolejnyNumerWiersza}
    \    ${WymaganyRokBazowy}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazRokBazowy Xpath}
    \    Run Keyword If    ${WymaganyRokBazowy}    Wpisz rok bazowy    ${WskazRokBazowy Xpath}
    \    ...    ELSE    Log    Brak pola "Rok bazowy"
    \    ${WymaganaWartoscBazowa}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazWartoscBazowa Xpath}
    \    Run Keyword If    ${WymaganaWartoscBazowa}    Wpisz wartosc bazowa    ${WskazWartoscBazowa Xpath}
    \    ...    ELSE    Log    Brak pola "Wartość bazowa"
    \    ${WymaganyRokDocelowy}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazRokDocelowy Xpath}
    \    Run Keyword If    ${WymaganyRokDocelowy}    Wpisz rok docelowy    ${WskazRokDocelowy Xpath}
    \    ...    ELSE    Log    Brak pola "Rok docelowy"
    \    ${WymaganaWartoscDocelowa}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazWartoscDocelowa Xpath}
    \    Run Keyword If    ${WymaganaWartoscDocelowa}    Wpisz wartosc docelowa    ${WskazWartoscDocelowa Xpath}
    \    ...    ELSE    Log    Brak pola "Wartość docelowa"
    \    ${WymaganaMetodWeryfikacji}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazMetodWeryfikacji Xpath}
    \    Run Keyword If    ${WymaganaMetodWeryfikacji}    Wpisz Metode Weryfikacji    ${WskazMetodWeryfikacji Xpath}    ${Podaj_Liczbe_Znakow_Metoda}
    \    ...    ELSE    Log    Brak pola "Metoda wyliczenia wskaźnika"
    \    ${KolejnyNumerWiersza}    Evaluate    ${KolejnyNumerWiersza}+1

Wypelnij wskazniki produktu
    [Arguments]    ${KolejnyNumerWiersza}    ${Podaj_Liczbe_Znakow_Metoda}
    [Documentation]    Wypełnia wskaźniki produktu w polach, które są do wypełnienia.
    ${KolejnyNumerWiersza}    Evaluate    ${KolejnyNumerWiersza}-1    # odejmuję 1, ponieważ numeracja w id'kach zaczyna się od 0, nie od 1
    ${ilosc_wierszy}    Get Element Count    ${WtabelaWskaznikiProduktu XPath}    # liczy ilość wierszy w tabeli
    : FOR    ${wiersz}    IN RANGE    0    ${ilosc_wierszy}
    \    ${WskazRokBazowy Xpath}    ${WskazWartoscBazowa Xpath}    ${WskazRokDocelowy Xpath}    ${WskazWartoscDocelowa Xpath}    ${WskazMetodWeryfikacji Xpath}    Wskazniki Lokatory
    \    ...    ${KolejnyNumerWiersza}
    \    ${WymaganyRokDocelowy}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazRokDocelowy Xpath}
    \    Run Keyword If    ${WymaganyRokDocelowy}    Wpisz rok docelowy    ${WskazRokDocelowy Xpath}
    \    ...    ELSE    Log    Brak pola "Rok docelowy"
    \    ${WymaganaWartoscDocelowa}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazWartoscDocelowa Xpath}
    \    Run Keyword If    ${WymaganaWartoscDocelowa}    Wpisz wartosc docelowa    ${WskazWartoscDocelowa Xpath}
    \    ...    ELSE    Log    Brak pola "Wartość docelowa"
    \    ${WymaganaMetodWeryfikacji}=    Run Keyword And Return Status    Element Should Be Visible    ${WskazMetodWeryfikacji Xpath}
    \    Run Keyword If    ${WymaganaMetodWeryfikacji}    Wpisz Metode Weryfikacji    ${WskazMetodWeryfikacji Xpath}    ${Podaj_Liczbe_Znakow_Metoda}
    \    ...    ELSE    Log    Brak pola "Metoda weryfikacji"
    \    ${KolejnyNumerWiersza}    Evaluate    ${KolejnyNumerWiersza}+1

Wpisz rok bazowy
    [Arguments]    ${WskazRokBazowy Xpath}
    [Documentation]    Wypełnia pole wartością roku bieżącego.
    ${Data}    Get Current Date    result_format=%Y
    ${Rok_Biezacy}    Convert To String    ${Data}
    Select From List By Value    ${WskazRokBazowy Xpath}    ${Rok_Biezacy}

Wpisz wartosc bazowa
    [Arguments]    ${WskazWartoscBazowa Xpath}
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=99
    Input Text    ${WskazWartoscBazowa Xpath}    ${Wartosc}00
