*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Otrzymana_PomocUI.robot

*** Keywords ***
Pomoc de minimis
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${WyborDeMinimis}=    Random Element    elements=('${OPPomocDeMinimisTak Id}', '${OPPomocDeMinimisNie Id}')
    Set Focus To Element    ${WyborDeMinimis}
    Click Element    ${WyborDeMinimis}
    [Return]    ${WyborDeMinimis}

Kwota Pomocy de Minimis
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=99999999
    Input Text    ${OPPomocDeMinimisWartosc Id}    ${Wartosc}99

Rodzaje dot Pomocy de Minimis
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPPomocDeMinimisRodzaje Id}    100

Kwota Pomocy de minimis w rolnictwie
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=999999
    Input Text    ${OPPomocDeMinimisRolnictwoWartosc Id}    ${Wartosc}99

Pomoc Publiczna
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${WyborPubliczna}=    Random Element    elements=('${OPPomocPublicznaTak Id}', '${OPPomocPublicznaNie Id}')
    Set Focus To Element    ${WyborPubliczna}
    Click Element    ${WyborPubliczna}
    [Return]    ${WyborPubliczna}

Kwota Pomocy Publicznej
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=99999999
    Input Text    ${OPPomocPublicznaWartosc Id}    ${Wartosc}99

Rodzaje dot Pomocy Publicznej
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPPomocPublicznaRodzaje Id}    100

Opis Powiazania Projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPOpisPowiazania Id}    ${Podaj_Liczbe_Znakow}

Inwestycja Poczatkowa
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${WyborDeMinimis}=    Random Element    elements=('${OPInwestycjaPoczatkowaTak Id}', '${OPInwestycjaPoczatkowaNie Id}')
    Set Focus To Element    ${WyborDeMinimis}
    Click Element    ${WyborDeMinimis}
    [Return]    ${WyborDeMinimis}

Opis Istniejacy Zaklad
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPOpisIstniejacyZaklad Id}    ${Podaj_Liczbe_Znakow}

Opis Nowe Produkty
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPOpisNoweProdukty Id}    ${Podaj_Liczbe_Znakow}

Wartosc Ksiegowa
    [Documentation]    Wypełnia pole losową wartością z przedziału ustalonego wartościami min i max.
    ${Wartosc}    Random Int    min=1    max=99999999
    Input Text    ${OPWartoscKsiegowa Id}    ${Wartosc}99

Opis Zmiana Procesu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPOpisZmianaProcesu Id}    ${Podaj_Liczbe_Znakow}

Pomoc NUTS3
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${WyborNUTS3}=    Random Element    elements=('${OPNUTS3Tak Id}', '${OPNUTS3Nie Id}')
    Set Focus To Element    ${WyborNUTS3}
    Click Element    ${WyborNUTS3}
    [Return]    ${WyborNUTS3}

Dodaj i Wypelnij Projekt NUTS3
    [Arguments]    ${LiczbaProjektowNUTS3}
    [Documentation]    Dodaje w liczbie określonej przez testera projekty NUTS3 \ i wypełnia pola projektu.
    ${Cyfra}    Set Variable    0
    : FOR    ${ProjektNUTS3}    IN RANGE    0    ${LiczbaProjektowNUTS3}
    \    Dodaj Projekt NUTS3
    \    ${OPNumerUmowy Xpath}    ${OPKwotaDofinansowania Xpath}    ${OPDataPodpisania Xpath}    ${OPDataZakonczenia Xpath}    Lokatory NUTS3    ${Cyfra}
    \    Wypelnij Projekt NUTS3    ${OPNumerUmowy Xpath}    ${OPKwotaDofinansowania Xpath}    ${OPDataPodpisania Xpath}    ${OPDataZakonczenia Xpath}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Dodaj Projekt NUTS3
    [Documentation]    Przycisk dodawania projektu NUTS3.
    Set Focus To Element    ${OPDodajProjektNUTS3 Xpath}
    Click Button    ${OPDodajProjektNUTS3 Xpath}

Lokatory NUTS3
    [Arguments]    ${Cyfra}
    [Documentation]    Lokatory projektu NUTS3.
    ${OPNumerUmowy Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="otrzymana_pomoc_i_powiazanie_projektu_umowyProjektowObjetychNuts3_    ${Cyfra}    _numerUmowy"])
    ${OPKwotaDofinansowania Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="otrzymana_pomoc_i_powiazanie_projektu_umowyProjektowObjetychNuts3_    ${Cyfra}    _dofinansowanie_tbbc_amount"])
    ${OPDataPodpisania Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="otrzymana_pomoc_i_powiazanie_projektu_umowyProjektowObjetychNuts3_    ${Cyfra}    _dataPodpisaniaUmowy"])
    ${OPDataZakonczenia Xpath}    Catenate    SEPARATOR=    xpath=(//*[@id="otrzymana_pomoc_i_powiazanie_projektu_umowyProjektowObjetychNuts3_    ${Cyfra}    _dataZakonczeniaUmowy"])
    [Return]    ${OPNumerUmowy Xpath}    ${OPKwotaDofinansowania Xpath}    ${OPDataPodpisania Xpath}    ${OPDataZakonczenia Xpath}

Wypelnij Projekt NUTS3
    [Arguments]    ${OPNumerUmowy Xpath}    ${OPKwotaDofinansowania Xpath}    ${OPDataPodpisania Xpath}    ${OPDataZakonczenia Xpath}
    [Documentation]    Wypełnia danymi pola projektu NUTS3.
    Set Focus To Element    ${OPNumerUmowy Xpath}
    Wypelnij Pole Losowym Tekstem    ${OPNumerUmowy Xpath}    100
    ${Wartosc}    Random Int    min=1    max=99999999
    Input Text    ${OPKwotaDofinansowania Xpath}    ${Wartosc}99
    #data rozpoczecia projektu
    Click Element    ${OPDataPodpisania Xpath}
    Sleep    1s
    Clear Element Text    ${OPDataPodpisania Xpath}
    ${z1}=    Random Int    min=1    max=10
    ${poczatek}=    Get Time    \    NOW + ${z1}d
    Input Text    ${OPDataPodpisania Xpath}    ${poczatek}    #poczatek
    Press Key    ${OPDataPodpisania Xpath}    \\13    # symulacja klawisza Enter
    #data zakonczenia projektu
    Click Element    ${OPDataZakonczenia Xpath}
    Sleep    1s
    Clear Element Text    ${OPDataZakonczenia Xpath}
    ${z2}=    Random Int    min=12    max=28
    ${koniec}=    Get Time    \    NOW + ${z2}d
    Input Text    ${OPDataZakonczenia Xpath}    ${koniec}    #koniec
    Press Key    ${OPDataZakonczenia Xpath}    \\13    # symulacja klawisza Enter

Zalozenia do Prognoz
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${OPZalozeniaPrognozy Id}    ${Podaj_Liczbe_Znakow}

Rok Obrotowy
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${WyborRokObrotowy}    Random Element    elements=('${OPRokObrotowyTak Id}', '${OPRokObrotowyNie Id}')
    Set Focus To Element    ${WyborRokObrotowy}
    Click Element    ${WyborRokObrotowy}
    [Return]    ${WyborRokObrotowy}

Data Rozpoczecia Roku Obrotowego
    [Documentation]    Po wyczyszczeniu pola z maski uzupełnia pole datą bieżącą pomniejszoną o losową liczbę dni z przedziału 1-10.
    Click Element    ${OPRokObrotowyRozpoczecie Id}
    Clear Element Text    ${OPRokObrotowyRozpoczecie Id}
    ${z1}=    Random Int    min=1    max=10
    ${poczatek}=    Get Current Date    local    + ${z1} d    result_format=%Y-%m-%d
    Input Text    ${OPRokObrotowyRozpoczecie Id}    ${poczatek}    #poczatek
    Press Key    ${OPRokObrotowyRozpoczecie Id}    \\13    # symulacja klawisza Enter

Data Zakonczenia Roku Obrotowego
    [Documentation]    Po wyczyszczeniu pola z maski uzupełnia pole datą bieżącą pomniejszoną o losową liczbę dni z przedziału 1-10.
    Click Element    ${OPRokObrotowyZakonczenie Id}
    Clear Element Text    ${OPRokObrotowyZakonczenie Id}
    ${z1}=    Random Int    min=5    max=15
    ${koniec}=    Get Current Date    local    + ${z1} d    result_format=%Y-%m-%d
    Input Text    ${OPRokObrotowyZakonczenie Id}    ${koniec}    #koniec
    Press Key    ${OPRokObrotowyZakonczenie Id}    \\13    # symulacja klawisza Enter

Czy pomoc de minimis
    [Documentation]    W zależności od wyboru w słowie kluczowym "Pomoc de minimis" wykonuje słowa kluczowe - Kwota Pomocy de Minimis i Rodzaje dot Pomocy de Minimis. W następnym kroku wypełnia Kwota Pomocy de minimis w rolnictwie.
    ${WyborDeMinimis}    Pomoc de minimis
    Run Keyword If    "${WyborDeMinimis}"=="${OPPomocDeMinimisTak Id}"    Kwota Pomocy de Minimis
    Run Keyword If    "${WyborDeMinimis}"=="${OPPomocDeMinimisTak Id}"    Rodzaje dot Pomocy de Minimis
    Kwota Pomocy de minimis w rolnictwie

Czy pomoc publiczna
    [Documentation]    W zależności od wyboru w słowie kluczowym "Pomoc Publiczna" wykonuje słowa kluczowe - Kwota Pomocy Publicznej i Rodzaje dot Pomocy Publicznej.
    ${WyborPubliczna}    Pomoc Publiczna
    Run Keyword If    "${WyborPubliczna}"=="${OPPomocPublicznaTak Id}"    Kwota Pomocy Publicznej
    Run Keyword If    "${WyborPubliczna}"=="${OPPomocPublicznaTak Id}"    Rodzaje dot Pomocy Publicznej

Czy NUTS 3
    [Arguments]    ${LiczbaProjektowNUTS3}
    [Documentation]    W zależności od wyboru w słowie kluczowym "Pomoc NUTS3" wykonuje słowo kluczowe - Dodaj i Wypelnij Projekt NUTS3.
    ${WyborNUTS3}    Pomoc NUTS3
    Run Keyword If    "${WyborNUTS3}"=="${OPNUTS3Tak Id}"    Dodaj i Wypelnij Projekt NUTS3    ${LiczbaProjektowNUTS3}

Czy rok obrotowy jest kalendarzowym
    [Documentation]    W zależności od wyboru w słowie kluczowym "Rok Obrotowy" wykonuje słowa kluczowe - Data Rozpoczecia Roku Obrotowego i Data Zakonczenia Roku Obrotowego.
    ${WyborRokObrotowy}    Rok Obrotowy
    Run Keyword If    "${WyborRokObrotowy}"=="${OPRokObrotowyNie Id}"    Data Rozpoczecia Roku Obrotowego
    Run Keyword If    "${WyborRokObrotowy}"=="${OPRokObrotowyNie Id}"    Data Zakonczenia Roku Obrotowego
