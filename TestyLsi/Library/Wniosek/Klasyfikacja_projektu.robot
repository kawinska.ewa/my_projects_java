*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Klasyfikacja_projektuUI.robot

*** Keywords ***
Nr kodu PKD dzialalnosci ktorej dotyczy projekt
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${KlasProjNrKoduPKD XPath}

Opis rodzaju dzialalnosci
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjOpisRodzajuDzialalnsci Id}    ${Podaj_Liczbe_Znakow}

Wplyw projektu - niepelnosprawni
    [Documentation]    Zaznacza jedyną możliwą opcję wyboru.
    Set Focus To Element    ${KlasProjWplywNiepelnosprawniPozytywny Id}
    Click Element    ${KlasProjWplywNiepelnosprawniPozytywny Id}

Uzasadnienie wplywu projektu - niepelnosprawni
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnienieWplywNiepelnosprawni Id}    ${Podaj_Liczbe_Znakow}

Czy produkty beda dostepne dla niepelnosprawnych
    [Documentation]    Zaznacza losowo wybór.
    ${wybor}=    Random Element    elements=('${KlasProjCzyProduktyDlaNiepelnosprawnychTak Id}', '${KlasProjCzyProduktyDlaNiepelnosprawnychNieDotyczy Id}')
    Set Focus To Element    ${wybor}
    Click Element    ${wybor}

Uzasadnienie dostepnosci produktow dla niepelnosprawnych
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnienieDostepnosciProduktowDlaNiepelnosprawnych Id}    ${Podaj_Liczbe_Znakow}

Wplyw projektu - rownosc szans kobiet i mezczyzn
    [Documentation]    Zaznacza losowo wybór.
    ${wybor}=    Random Element    elements=('${KlasProjWplywRownoscSzansKobietIMezczyznNeutralny Id}', '${KlasProjWplywRownoscSzansKobietIMezczyznPozytywny Id}')
    Set Focus To Element    ${wybor}
    Click Element    ${wybor}

Uzasadnienie wplywu projektu - rownosc szans kobiet i mezczyzn
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnienieRownoscSzansKobietIMezczyzn XPath}    ${Podaj_Liczbe_Znakow}

Wplyw projektu - zrownowazony rozwoj
    [Documentation]    Zaznacza losowo wybór.
    ${wybor}=    Random Element    elements=('${KlasProjWplywZrownowazonyRozwojNeutralny Id}', '${KlasProjWplywZrownowazonyRozwojPozytywny Id}')
    Set Focus To Element    ${wybor}
    Click Element    ${wybor}

Uzasadnienie wplywu projektu - zrownowazony rozwoj
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnienieZrownowazonyRozwoj Id}    ${Podaj_Liczbe_Znakow}

Rodzaj dzialalnosci gospodarczej
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${KlasProjRodzDzialGosp Id}

Typ obszaru realizacji
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${KlasProjTypObszaruRealizacji Id}

Opis produktu bedacego rezultatem projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjOpisProduktuRezultatProjektu Id}    ${Podaj_Liczbe_Znakow}

Znaczenie nowych cech i funkcjonalnosci
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjZnaczenieNowychCech Id}    ${Podaj_Liczbe_Znakow}

Konkurencyjnosc produktu
    [Arguments]    ${ile_razy}    ${Podaj_Liczbe_Znakow_Oferta_Wnioskodawcy}    ${Podaj_Liczbe_Znakow_Oferta_Konkurencji}
    [Documentation]    Dodaje produkty w liczbie określonej przez testera w części Konkurencyjność produktu.
    ${Cyfra}    Set Variable    1
    : FOR    ${Konkurencyjnosc}    IN RANGE    0    ${ile_razy}
    \    Set Focus To Element    ${KlasProjPrzyciskDodajProdukt XPath}
    \    Kliknij Przycisk    ${KlasProjPrzyciskDodajProdukt XPath}
    \    ${KlasProjOfertaWnioskodawcy Id}    ${KlasProjOfertaKonkurencji Id}    Konkurencyjnosc produktu lokatory    ${Cyfra}
    \    Wypelnij danymi konkurencyjnosc produktu    ${KlasProjOfertaWnioskodawcy Id}    ${KlasProjOfertaKonkurencji Id}    ${Podaj_Liczbe_Znakow_Oferta_Wnioskodawcy}    ${Podaj_Liczbe_Znakow_Oferta_Konkurencji}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Konkurencyjnosc produktu lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Przechowuje lokatory do słowa kluczowego "Konkurencyjnosc produktu".
    ${Cyfra}    Evaluate    ${Cyfra}-1
    ${KlasProjOfertaWnioskodawcy Id}    Catenate    SEPARATOR=    id=klasyfikacja_projektu_konkurencyjnoscProduktu_    ${Cyfra}    _ofertaWnioskodawcy
    ${KlasProjOfertaKonkurencji Id}    Catenate    SEPARATOR=    id=klasyfikacja_projektu_konkurencyjnoscProduktu_    ${Cyfra}    _ofertaKonkurencji
    [Return]    ${KlasProjOfertaWnioskodawcy Id}    ${KlasProjOfertaKonkurencji Id}

Wypelnij danymi konkurencyjnosc produktu
    [Arguments]    ${KlasProjOfertaWnioskodawcy Id}    ${KlasProjOfertaKonkurencji Id}    ${Podaj_Liczbe_Znakow_Oferta_Wnioskodawcy}    ${Podaj_Liczbe_Znakow_Oferta_Konkurencji}
    [Documentation]    Wypełnia zestaw pól produktu - powiązanie ze słowem kluczowym "Konkurencyjnosc produktu".
    Set Focus To Element    ${KlasProjOfertaWnioskodawcy Id}
    Wypelnij Pole Losowym Tekstem    ${KlasProjOfertaWnioskodawcy Id}    ${Podaj_Liczbe_Znakow_Oferta_Wnioskodawcy}
    Set Focus To Element    ${KlasProjOfertaKonkurencji Id}
    Wypelnij Pole Losowym Tekstem    ${KlasProjOfertaKonkurencji Id}    ${Podaj_Liczbe_Znakow_Oferta_Konkurencji}

Rynek docelowy
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjRynekDocelowy Id}    ${Podaj_Liczbe_Znakow}

Zapotrzebowanie rynkowe na produkt
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjZapotrzebowanieRynkoweNaProdukt Id}    ${Podaj_Liczbe_Znakow}

Dystrybucja i sprzedaz produktu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjDystrybucjaISprzedazProduktu Id}    ${Podaj_Liczbe_Znakow}

Promocja produktu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjPromocjaProduktu Id}    ${Podaj_Liczbe_Znakow}

Dostepnosc produktow projektu dla niepelnosprawnych
    [Arguments]    ${Ile_Razy}    ${Podaj_Liczbe_Znakow_Produkt}    ${Podaj_Liczbe_Znakow_Uzasadnienie}
    [Documentation]    Dodaje produkty w liczbie określonej przez testera w części Konkurencyjność produktu. Wypełnia pola losowym tekstem o liczbie znaków określonej przez testera. Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    ${Cyfra}    Set Variable    0
    : FOR    ${Produkt}    IN RANGE    0    ${Ile_Razy}
    \    ${Nazwa_Produktu}    Catenate    SEPARATOR=    id=klasyfikacja_projektu_wykazDostepnosciProduktowProjektu_${Cyfra}_nazwa
    \    ${Czy_Produkt_Neutralny}    Catenate    SEPARATOR=    //*[@id="klasyfikacja_projektu_wykazDostepnosciProduktowProjektu_${Cyfra}_produktNeutralny"]
    \    ${Uzasadnienie}    Catenate    SEPARATOR=    id=klasyfikacja_projektu_wykazDostepnosciProduktowProjektu_${Cyfra}_uzasadnienie
    \    Set Focus To Element    ${KlasProjPrzyciskDodajProduktNiepelnosprawni XPath}
    \    Kliknij Przycisk    ${KlasProjPrzyciskDodajProduktNiepelnosprawni XPath}
    \    Wypelnij Pole Losowym Tekstem    ${Nazwa_Produktu}    ${Podaj_Liczbe_Znakow_Produkt}
    \    Wybierz element z listy bez 1 pozycji    ${Czy_Produkt_Neutralny}
    \    Wypelnij Pole Losowym Tekstem    ${Uzasadnienie}    ${Podaj_Liczbe_Znakow_Uzasadnienie}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Uzasadnienie pozytywnego wplywu projektu - niepelnosprawni
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnieniePozWplywuNiepelnosprawni Id}    ${Podaj_Liczbe_Znakow}

Projekt wpisuje się w KIS
    [Documentation]    Wybiera losowo jedną pozycję w polu.
    ${element}    Wybierz element z listy    ${KlasProjKIS XPath}
    [Return]    ${element}

Obszar KIS
    [Arguments]    ${Ile_Obszarow_Dodac}
    [Documentation]    Wybiera elementy z listy wyboru w ilości określonej przez testera.
    Wybierz elementy z listy    ${KlasProjObszarKIS XPath}    ${Ile_Obszarow_Dodac}

Uzasadnienie wybranego obszaru KIS
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${KlasProjUzasadnienieWyboruKIS Id}    ${Podaj_Liczbe_Znakow}
