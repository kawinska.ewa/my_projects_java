*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Zrodla_finansowania_wydatkowUI.robot    # zmienne sekcji wniosku

*** Keywords ***
Srodki wlasne
    [Documentation]    Wypełnia wiersz "Środki własne" dla wydatków ogółem i wydatków kwalifikowalnych.
    # wartości z zestawienia finansowego ogółem
    ${wydatki_ogolem}    Get Text    ${ZFOcalkowiteWydatkiOgolem Id}
    ${wo}    Replace String    ${wydatki_ogolem}    ${SPACE}    ${EMPTY}    #usuwam spacje
    ${wo1}    Replace String    ${wo}    .    ${EMPTY}    #usuwam kropke
    ${wydatki_kwalifikowalne}    Get Text    ${ZFOcalkowiteWydatkiKwalifikowalne Id}
    ${wk}    Replace String    ${wydatki_kwalifikowalne}    ${SPACE}    ${EMPTY}    #usuwam spacje
    ${wk1}    Replace String    ${wk}    .    ${EMPTY}    #usuwam kropke
    ${wnioskowane_dofinansowanie}    Get Text    ${ZFOcalkowiteWydatkiWnioskowaneDofinansowanie Id}
    ${wd}    Replace String    ${wnioskowane_dofinansowanie}    ${SPACE}    ${EMPTY}    #usuwam spacje
    ${wd1}    Replace String    ${wd}    .    ${EMPTY}    #usuwam kropke
    #obliczanie wartości - środki wlasne (wydatki ogółem)
    ${wynik1}    Evaluate    ${wo1}-${wd1}
    ${w1}    Convert To String    ${wynik1}
    #obliczanie wartości - środki własne (wydatki kwalifikowalne)
    ${wynik2}    Evaluate    ${wk1}-${wd1}
    ${w2}    Convert To String    ${wynik2}
    #wydatki ogółem z części zestawienia finansowego i źródła finansowania powinny być sobie równe
    Input Text    ${ZFWwydatkiOgolemSrodkiWlasne Id}    ${w1}
    #wydatki kwalifikowalne z części zestawienia finansowego i źródła finansowania powinny być sobie równe
    Input Text    ${ZFWwydatkiKwalifikowalneSrodkiWlasne Id}    ${w2}

Wypelnij zrodla finansowania wydatkow
    [Documentation]    Wypełnia dane dla słów kluczowych znajdujących się w bieżącym słowie kluczowym.
    Srodki wlasne    # wypelnia srodki wlasne dla wydatkow ogolem i kwalifikowalnych
