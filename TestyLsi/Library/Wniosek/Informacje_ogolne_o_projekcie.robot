*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot

*** Keywords ***
Nr wniosku o dofinansowanie w ramach I etapu dzialania
    [Documentation]    Wypełnia pole numerem wniosku z poprzedniego etapu tego samego poddziałania.
    Input Text    ${OIOPNumerWnioskuPoprzedniEtap Id}    POPW.01.04.00-18-0036/18

Kod wniosku z poprzedniego etapu w ramach I etapu dzialania
    [Documentation]    Wypełnia pole kodem wniosku - dostępny w karcie projektu wniosku złożonego w I etapie tego poddziałania.
    Input Text    ${OIOPKodWnioskuPoprzedniEtap Id}    WI51TMl1

Tytul projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPTytulProjektu Id}.
    Wypelnij Pole Losowym Tekstem    ${OIOPTytulProjektu Id}    ${Podaj_Liczbe_Znakow}

Krotki opis projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPKrotkiOpisProjektu Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPKrotkiOpisProjektu Id}    ${Podaj_Liczbe_Znakow}

Cel projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPCelProjektu Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPCelProjektu Id}    ${Podaj_Liczbe_Znakow}

Zgodnosc projektu z dzialalnoscia wnioskodawcy
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPZgodnoscProjektuZDzialalnosciaWnioskodawcy Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPZgodnoscProjektuZDzialalnosciaWnioskodawcy Id}    ${Podaj_Liczbe_Znakow}

Opis procesu projektowego
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPOpisProcesuProjektowego Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPOpisProcesuProjektowego Id}    ${Podaj_Liczbe_Znakow}

Opis i uzasadnienie dzialan wdrozeniowych
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPOpisDzialanWdrozeniowych Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPOpisDzialanWdrozeniowych Id}    ${Podaj_Liczbe_Znakow}

Oczekiwane efekty projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole ${OIOPOczekiwaneEfekty Id}
    Wypelnij Pole Losowym Tekstem    ${OIOPOczekiwaneEfekty Id}    ${Podaj_Liczbe_Znakow}

Projekt dotyczy innowacji produktowej
    [Documentation]    Losuje element z listy wyboru i go zaznacza.
    Wybierz element z listy bez 1 pozycji    ${OIOPProjektDotyczyInnowcjiProduktowej Id}

Poziom innowacyjnosci produktu
    [Documentation]    Losuje element z listy wyboru i go zaznacza.
    Wybierz element z listy bez 1 pozycji    ${OIOPPoziomInnowacyjnosci Id}

Opis innowacji wdrazanej w ramach projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole
    Wypelnij Pole Losowym Tekstem    ${OIOPOpisInnowacji Id}    ${Podaj_Liczbe_Znakow}

Projekt dotyczy innowacji innej niz produktowa
    [Documentation]    Losuje element z listy wyboru i go zaznacza.
    Wybierz element z listy bez 1 pozycji    ${OIOPProjektDotyczyInnowcjiInnejNizProduktowa Id}

Opis innowacji innej niż produktowa wdrazanej w ramach projektu
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Losuje ciąg znaków o zadanej długości i wstawia go w pole
    Wypelnij Pole Losowym Tekstem    ${OIOPOpisInnowcjiInnejNizProduktowa Id}    ${Podaj_Liczbe_Znakow}

Slowa kluczowe
    [Documentation]    Dodaje słowa kluczowe. Liczba i długość słów kluczowych jest definiowana.
    Dodaj Elementy Przyciskiem    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePrzyciskDodaj XPath}    3    1    4

Dziedzina projektu
    [Arguments]    ${ile_dziedzin}
    [Documentation]    Wybiera elementy z listy wyboru w ilości określonej przez testera.
    Wybierz elementy z listy    ${OIOPDziedzinaProjektu XPath}    ${ile_dziedzin}

Okres realizacji projektu
    [Documentation]    Czyści pola dat ${OIOPOkresRealizacjiProjektuPoczatek Id} i ${OIOPOkresRealizacjiProjektuKoniec Id} z maski. Do bieżącego czasu dodaje określoną liczbę dni i wstawia w pole.
    Click Element    ${OIOPOkresRealizacjiProjektuPoczatek Id}
    Wait Until Element Is Visible    ${OIOPOkresRealizacjiProjektuPoczatek Id}    10s
    Clear Element Text    ${OIOPOkresRealizacjiProjektuPoczatek Id}
    ${z1}    Random Int    min=1    max=10
    ${poczatek}    Get Current Date    local    + ${z1} d    result_format=%Y-%m-%d
    Input Text    ${OIOPOkresRealizacjiProjektuPoczatek Id}    ${poczatek}    #poczatek
    Click Element    ${OIOPOkresRealizacjiProjektuKoniec Id}
    Wait Until Element Is Visible    ${OIOPOkresRealizacjiProjektuKoniec Id}    10s
    Clear Element Text    ${OIOPOkresRealizacjiProjektuKoniec Id}
    ${z2}    Random Int    min=60    max=100
    ${koniec}    Get Current Date    local    + ${z2} d    result_format=%Y-%m-%d
    Input Text    ${OIOPOkresRealizacjiProjektuKoniec Id}    ${koniec}    #koniec
    Press Key    ${OIOPOkresRealizacjiProjektuKoniec Id}    \\13    # symulacja klawisza Enter

Wybierz branze
    [Arguments]    ${ile_branz}
    [Documentation]    Wybiera elementy z listy wyboru w ilości określonej przez testera.
    Wybierz elementy z listy    ${OIOPBranzaPomyslu XPath}    ${ile_branz}

Wybierz platforme startowa
    [Documentation]  Wybiera elementy z listy wyboru
    Wybierz element z listy  ${OIOPPlatformaStartowa XPath}