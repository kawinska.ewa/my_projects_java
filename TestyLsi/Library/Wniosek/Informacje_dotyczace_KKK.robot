*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          Informacje_dotyczace_KKKUI.robot

*** Keywords ***
Wnioskodawca jest koordynatorem klastra
    [Documentation]    Wybiera losowo z listy wyboru jedną pozycję pomijając opcję "Proszę wybrać".
    Wybierz element z listy bez 1 pozycji    ${IDKKKwnioskodawcaJestkoordynatorem XPath}

Nazwa KKK ktorego koordynatorem jest wnioskodawca
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${IDKKKnazwaKKKkoordynatorWnioskodawca Id}    ${Podaj_Liczbe_Znakow}

Adres strony www
    [Documentation]    Wypełnia adres www używając biblioteki Faker.
    ${www}    FakerLibrary.Url
    Input Text    ${IDKKKstronaWWW Id}    ${www}

Data uzyskania statusu KKK
    [Documentation]    Po wyczyszczeniu pola z maski uzupełnia pole datą bieżącą pomniejszoną o 80 dni.
    Click Element    ${IDKKKdataUzyskaniaStatusuKKK Id}
    Clear Element Text    ${IDKKKdataUzyskaniaStatusuKKK Id}
    ${Data_Uzyskania_StatusuKKK}    Get Time    \    NOW - 80d
    Input Text    ${IDKKKdataUzyskaniaStatusuKKK Id}    ${Data_Uzyskania_StatusuKKK}

Data obowiazywania statusu KKK
    [Documentation]    Po wyczyszczeniu pola z maski uzupełnia pole datą bieżącą powiększoną o 100 dni.
    Click Element    ${IDKKKdataObowiazywaniaStatusuKKK Id}
    Clear Element Text    ${IDKKKdataObowiazywaniaStatusuKKK Id}
    ${Data_Uzyskania_StatusuKKK}    Get Time    \    NOW + 100d
    Input Text    ${IDKKKdataObowiazywaniaStatusuKKK Id}    ${Data_Uzyskania_StatusuKKK}

Specjalizacja klastra
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${IDKKKspecjalizacjaKlastra Id}    ${Podaj_Liczbe_Znakow}

Glowne wyroby uslugi
    [Arguments]    ${Podaj_Liczbe_Znakow}
    [Documentation]    Wypełnia pole losowym tekstem o liczbie znaków określonej przez testera.
    Wypelnij Pole Losowym Tekstem    ${IDKKKglowneWyrobyUslugi Id}    ${Podaj_Liczbe_Znakow}

Liczba podmiotow wchodzacych w sklad KKK
    [Arguments]    ${Podaj_Wartosc_Max}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${Liczba_Podmiotow_w_KKK}    Random Int    min=1    max=${Podaj_Wartosc_Max}
    Input Text    ${IDKKKliczbaPodmiotowKKK Id}    ${Liczba_Podmiotow_w_KKK}

Liczba przedsiebiorstw w KKK
    [Arguments]    ${Podaj_Wartosc_Max}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${Liczba_Przedsiebiorstw_w_KKK}    Random Int    min=1    max=${Podaj_Wartosc_Max}
    Input Text    ${IDKKKliczbaPrzedsiebiorstwKKK Id}    ${Liczba_Przedsiebiorstw_w_KKK}

Liczba mikro i malych przedsiebiorstw
    [Arguments]    ${Podaj_Wartosc_Max}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${Liczba_Mikro_i_Malych_w_KKK}    Random Int    min=1    max=${Podaj_Wartosc_Max}
    Input Text    ${IDKKKliczbaMikroiMalych Id}    ${Liczba_Mikro_i_Malych_w_KKK}

Liczba srednich przedsiebiorstw
    [Arguments]    ${Podaj_Wartosc_Max}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${Liczba_Srednich_w_KKK}    Random Int    min=1    max=${Podaj_Wartosc_Max}
    Input Text    ${IDKKKliczbaSrednich Id}    ${Liczba_Srednich_w_KKK}

Liczba duzych przedsiebiorstw
    [Arguments]    ${Podaj_Wartosc_Max}
    [Documentation]    Wypełnia pole losową wartością z przedziału od 1 do wartości określonej przez testera.
    ${Liczba_Duzych_w_KKK}    Random Int    min=1    max=${Podaj_Wartosc_Max}
    Input Text    ${IDKKKliczbaDuzych Id}    ${Liczba_Duzych_w_KKK}
