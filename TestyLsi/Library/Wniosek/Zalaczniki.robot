*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot
Resource          ZalacznikiUI.robot

*** Keywords ***
Przejdz do edycji zalacznikow
    [Documentation]    Przycis edycji załączników.
    Set Focus To Element    ${ZalacznikiEdycjaZal XPath}
    Click Element    ${ZalacznikiEdycjaZal XPath}
    Wait Until Element Is Visible    ${ZalacznikiPrzyciskWgraj}    20s

Dodaj plik
    [Arguments]    ${LpZalacznika}    ${ile_zalacznikow}
    [Documentation]    1. Podaj Lp. załącznika, który chcesz wgrać - zmienna ${LpZalacznika} oraz określ ile razy chcesz dodać dany typ załącznika - zmienna ${ile_zalacznikow}. 2. Zmienną ${LpZalacznika} zamień na string i podstaw do zmiennej ${NrWiersza}. 3. Pobierz atrybut elementu dla lokatora ze zmienną ${NrWiersza} i podstaw do zmiennej ${element}. 4. Pozyskaj id ze zmiennej ${element} i podstaw do zmiennej ${Id}. 5. Utwórz lokator do wskazania załącznika ze zmienną ${Id} i podstaw go do zmiennej ${lokator}. 6. Przy użyciu pętli FOR wykonaj czynnosci tyle razy ile określono zmienną ${ile_zalacznikow}: 6.1 \ Pobierz załącznik ze wskazanej lokalizacji i wstaw go do pola opisanego lokatorem (pkt 5.). 6.2. Dla przycisku "Wgraj" utwórz lokator wstawiając do niego zmienną ${NrWiersza} i podstaw go do zmiennej ${button}. 6.3. Klinknij przycisk Wgraj określony zmienną ${button}.
    ${NrWiersza}    Convert To String    ${LpZalacznika}
    ${element}    Get Element Attribute    //html/body/div[3]/div[4]/table/tbody/tr[${NrWiersza}]/td[2]//form[contains(@name, 'formularz_zalacznik_')]    name
    ${Id}    Fetch From Right    ${element}    _
    ${lokator}    Catenate    SEPARATOR=    id=formularz_zalacznik_    ${Id}    _plikWgrywany
    : FOR    ${wgraj_zalacznik}    IN RANGE    ${ile_zalacznikow}
    \    Press Key    ${lokator}    W:${/}Zespoly2016${/}BI${/}Wewn${/}Wsp${/}Testowanie${/}1000.docx
    \    ${button}    Catenate    SEPARATOR=    //html/body/div[3]/div[4]/table/tbody/tr[    ${NrWiersza}    ]/td[2]
    \    ...    ${ZalacznikiPrzyciskWgraj}
    \    Click Element    ${button}

Wroc do edycji wniosku
    [Documentation]    Powrót z edycji załączników do edycji wniosku.
    Click Element    ${ZalacznikiLinkWniosek}
