*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           OperatingSystem
Library           FakerLibrary    locale=pl_PL
Resource          ../ElementyWnioskuUI.robot
Resource          ../Resource.robot

*** Variables ***
${HRFdodajZadaniePrzycisk XPath}    //*[@id="formularz_harmonogram_rzeczowo_finansowy"]/div[2]/button    # Przycisk dodaj zadanie
${HRFdodajWydatekRzeczywisciePonoszony XPath}    //*[@id="formularz_harmonogram_rzeczowo_finansowy"]/div[4]/button    # Przycisk dodaj wydatek rzeczywiście ponoszony

*** Keywords ***
Dodaj zadanie w HRF
    [Arguments]    ${ile_razy}    ${Podaj_Liczbe_Znakow_Nazwa_Zadania}    ${Podaj_Liczbe_Znakow_Opis_Dzialan}
    [Documentation]    Dodaje zadanie w HRF tyle razy ile zostanie określone przez zmienną ${ile_razy}.
    ${Cyfra}    Set Variable    0
    : FOR    ${hrf_zadanie}    IN RANGE    0    ${ile_razy}
    \    Set Focus To Element    ${HRFdodajZadaniePrzycisk XPath}
    \    Kliknij Przycisk    ${HRFdodajZadaniePrzycisk XPath}
    \    ${NazwaZadania Id}    ${OpisDzialan Id}    ${DataRozpoczeciaZadania Id}    ${DataZakonczeniaZadania Id}    Zadanie HRF lokatory    ${Cyfra}
    \    Wypelnij danymi zadanie w HRF    ${NazwaZadania Id}    ${OpisDzialan Id}    ${DataRozpoczeciaZadania Id}    ${DataZakonczeniaZadania Id}    ${Podaj_Liczbe_Znakow_Nazwa_Zadania}
    \    ...    ${Podaj_Liczbe_Znakow_Opis_Dzialan}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Zadanie HRF lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Przechowuje lokatory formularza opisującego zadanie. W lokatory przekazywany jest argument ${Cyfra}.
    ${NazwaZadania Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_zadania_${Cyfra}_nazwa
    ${OpisDzialan Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_zadania_${Cyfra}_opisPlanowanychDzialan
    ${DataRozpoczeciaZadania Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_zadania_${Cyfra}_dataRozpoczeciaZadania
    ${DataZakonczeniaZadania Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_zadania_${Cyfra}_dataZakonczeniaZadania
    [Return]    ${NazwaZadania Id}    ${OpisDzialan Id}    ${DataRozpoczeciaZadania Id}    ${DataZakonczeniaZadania Id}

Wypelnij danymi zadanie w HRF
    [Arguments]    ${NazwaZadania Id}    ${OpisDzialan Id}    ${DataRozpoczeciaZadania Id}    ${DataZakonczeniaZadania Id}    ${Podaj_Liczbe_Znakow_Nazwa_Zadania}    ${Podaj_Liczbe_Znakow_Opis_Dzialan}
    [Documentation]    Wypełnia danymi pola formularza zadania w HRF.
    Wypelnij Pole Losowym Tekstem    ${NazwaZadania Id}    ${Podaj_Liczbe_Znakow_Nazwa_Zadania}    Biblioteka=Faker
    Wypelnij Pole Losowym Tekstem    ${OpisDzialan Id}    ${Podaj_Liczbe_Znakow_Opis_Dzialan}    Biblioteka=Faker
    #data rozpoczecia zadania
    Click Element    ${DataRozpoczeciaZadania Id}
    Wait Until Element Is Visible    ${DataRozpoczeciaZadania Id}    10s
    Clear Element Text    ${DataRozpoczeciaZadania Id}
    ${z1}    Random Int    min=6    max=14
    ${poczatek}    Get Current Date    local    + ${z1} d    result_format=%Y-%m-%d
    Input Text    ${DataRozpoczeciaZadania Id}    ${poczatek}    #poczatek
    Press Key    ${DataRozpoczeciaZadania Id}    \\13    # symulacja klawisza Enter, ukrywamy kalendarz
    #data zakonczenia zadania
    Click Element    ${DataZakonczeniaZadania Id}
    Wait Until Element Is Visible    ${DataZakonczeniaZadania Id}    10s
    Clear Element Text    ${DataZakonczeniaZadania Id}
    ${z2}    Random Int    min=18    max=30
    ${koniec}    Get Current Date    local    + ${z2} d    result_format=%Y-%m-%d
    Input Text    ${DataZakonczeniaZadania Id}    ${koniec}    #koniec
    Press Key    ${DataZakonczeniaZadania Id}    \\13    # symulacja klawisza Enter, ukrywamy kalendarz

Dodaj wydatek rzeczywiscie ponoszony
    [Arguments]    ${ile_razy}    ${Podaj_Liczbe_Znakow_Nazwa_Kosztu}    ${Podaj_Liczbe_Znakow_Opis_Kosztu}
    [Documentation]    Dodaje wydatek rzeczywiście ponoszony określoną liczbę razy poprzez argument ${ile_razy}. Wypełnia danymi pola formularza wydatku rzeczywiście ponoszonego.
    ${Cyfra}    Set Variable    0
    : FOR    ${hrf_wydatekRzeczywisciePonoszony}    IN RANGE    0    ${ile_razy}
    \    Set Focus To Element    ${HRFdodajWydatekRzeczywisciePonoszony XPath}
    \    Kliknij Przycisk    ${HRFdodajWydatekRzeczywisciePonoszony XPath}
    \    ${Zadanie Id}    ${KategoriaKosztow Id}    ${NazwaKosztu Id}    ${OpisKosztu Id}    ${WydatkiOgolem Id}    ${WydatkiKwalifikowalne Id}
    \    ...    ${WtymVAT Id}    ${Dofinansowanie Id}    Wydatek rzeczywiscie ponoszony lokatory    ${Cyfra}
    \    Wypelnij danymi wydatek rzeczywiscie ponoszony    ${Zadanie Id}    ${KategoriaKosztow Id}    ${NazwaKosztu Id}    ${OpisKosztu Id}    ${WydatkiOgolem Id}
    \    ...    ${WydatkiKwalifikowalne Id}    ${WtymVAT Id}    ${Dofinansowanie Id}    ${Podaj_Liczbe_Znakow_Nazwa_Kosztu}    ${Podaj_Liczbe_Znakow_Opis_Kosztu}
    \    ${Cyfra}    Evaluate    ${Cyfra}+1

Wydatek rzeczywiscie ponoszony lokatory
    [Arguments]    ${Cyfra}
    [Documentation]    Lokatory występujące w formularzu wydatku rzeczywiście ponoszonego. W lokatory przekazywany jest argument ${Cyfra}.
    ${Zadanie Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_zadanie
    ${KategoriaKosztow Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_kategoriaKosztow
    ${NazwaKosztu Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_nazwaKosztu
    ${OpisKosztu Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_opisKosztuWDanejPodkategorii
    ${WydatkiOgolem Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_wartoscOgolem_tbbc_amount
    ${WydatkiKwalifikowalne Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_wydatkiKwalifikowalne_tbbc_amount
    ${WtymVAT Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_wydatkiKwalifikowalneVat_tbbc_amount
    ${Dofinansowanie Id}    Catenate    SEPARATOR=    id=zakres_rzeczowo_finansowy_wydatki_${Cyfra}_wnioskowaneDofinansowanie_tbbc_amount
    [Return]    ${Zadanie Id}    ${KategoriaKosztow Id}    ${NazwaKosztu Id}    ${OpisKosztu Id}    ${WydatkiOgolem Id}    ${WydatkiKwalifikowalne Id}
    ...    ${WtymVAT Id}    ${Dofinansowanie Id}

Wypelnij danymi wydatek rzeczywiscie ponoszony
    [Arguments]    ${Zadanie Id}    ${KategoriaKosztow Id}    ${NazwaKosztu Id}    ${OpisKosztu Id}    ${WydatkiOgolem Id}    ${WydatkiKwalifikowalne Id}
    ...    ${WtymVAT Id}    ${Dofinansowanie Id}    ${Podaj_Liczbe_Znakow_Nazwa_Kosztu}    ${Podaj_Liczbe_Znakow_Opis_Kosztu}
    [Documentation]    Wypełnia danymi formularz wydatku rzeczywiście ponoszonego.
    Wybierz element z listy bez 1 pozycji    ${Zadanie Id}
    Wybierz element z listy bez 1 pozycji    ${KategoriaKosztow Id}
    Wypelnij Pole Losowym Tekstem    ${NazwaKosztu Id}    ${Podaj_Liczbe_Znakow_Nazwa_Kosztu}    Biblioteka=Faker
    Wypelnij Pole Losowym Tekstem    ${OpisKosztu Id}    ${Podaj_Liczbe_Znakow_Opis_Kosztu}    Biblioteka=Faker
    #wydatki ogółem
    ${liczba}    Set Variable    75000000
    Input Text    ${WydatkiOgolem Id}    ${liczba}
    #Wydatki kwalifikowalne
    ${liczba}=    Set Variable    65000000
    Input Text    ${WydatkiKwalifikowalne Id}    ${liczba}
    # wtym vat
    ${liczba}=    Set Variable    0
    Input Text    ${WtymVAT Id}    ${liczba}
    #Dofinansowanie
    ${liczba}=    Set Variable    31500000
    Input Text    ${Dofinansowanie Id}    ${liczba}
