*** Variables ***
${LoginUserName Id}    id=_username    # pole Nazwa użytkownika
${LoginPassword Id}    id=_password    # pole Hasło
${LoginButton Id}    id=zaloguj    # przycisk Zaloguj
${MenuKonkursy Id}    id=konkursy    # menu Konkursy widoczne po zalogowaniu
${WiecejOpcjiButton Id}    id=wiecej_opcji    # przycisk w prawym górnym menu
${LogoutLink Id}    id=logout    # link Wyloguj
${BledneDaneLogowaniaKomunikat}    Niepoprawne dane uwierzytelniania
${BledneDaneLogowaniaAlert CSS}    css=div.alert.alert-danger
${JestWymaganeAlert CSS}    css=div.label.label-danger
${LoginJestWymaganyKomunikat}    Nazwa użytkownika jest wymagana
${HasloJestWymaganeKomunikat}    Hasło jest wymagane
${UkryjCookieInfoButton Id}    id=hide-ue-cookie-info    # Przycisk do ukrywania informacji o cookie
