*** Settings ***
Library           Selenium2Library
Library           String
Library           DatabaseLibrary
Library           Dialogs
Library           DateTime
Library           Collections
Resource          ../Library/LogowanieUI.robot    # Logowanie elementy interface
Resource          ../Library/TrwajaceNaboryUI.robot
Resource          ../Library/WniosekUI.robot
Resource          ../Library/AdministracjaUI.robot
Resource          ../Library/WersjeElementowEdutujUI.robot
Resource          ../Library/Resource.robot
Resource          ../Library/KonfiguracjaWnioskuUI.robot
Resource          ../Library/ElementyWnioskuUI.robot
Resource          ../Library/WalidacjaUI.robot

*** Keywords ***
Edycja Logotyp
    [Documentation]    Krok 1: Sprawdź, czy element istnieje w tabeli
    ...    Krok 2: Zapisz wiersz, w którym istnieje element
    ...    Krok 3: Jezeli element istnieje, uruchom sprawdzenie konfiguracji elementu
    ...    Krok 4: Jeżeli numer wiersza, w którym istnieje element jest większy od 0, to ustaw zmienną CzyJest na TRUE, w przeciwnym razie na FALSE
    ...    Krok 5: Zapisz wartość zmiennej CzyJest
    ...    Krok 6: Ustaw zmienną CzyJest jako dostepną dla całego pakietu
    ${WierszTabeliLogotyp}    Element Istnieje W Tabeli    ${LogotypElemetNazwa}    1    ${TabelaZastosowaneElementy XPath}
    Log    ${WierszTabeliLogotyp}
    Run Keyword If    ${WierszTabeliLogotyp} > 0    Konfiguracja Logotyp    ${WierszTabeliLogotyp}
    ${CzyJestLogotyp}    Set Variable If    ${WierszTabeliLogotyp} > 0    ${TRUE}    ${FALSE}
    Log    ${CzyJestLogotyp}
    Set Suite Variable    ${CzyJestLogotyp}

Konfiguracja Logotyp
    [Arguments]    ${WierszTabeliLogotyp}
    [Documentation]    Krok 1: Otwórz edycję elementu Logotyp
    ...    Krok 2: Pobierz wybraną wartość dla lewego logo
    ...    Krok 3: Zapisz wybraną wartość dla lewego logo
    ...    Krok 4: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 5: Pobierz wybraną wartość dla środkowego logo
    ...    Krok 6: Zapisz wybraną wartość dla środkowego logo
    ...    Krok 7: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 8: Pobierz wybraną wartość dla prawego logo
    ...    Krok 9: Zapisz wybraną wartość dla prawego logo
    ...    Krok 10: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 11: Zamknij edycję elementu Logotyp
    Otworz Edycje Elementu    ${WierszTabeliLogotyp}
    ${LogotypLogoLewe}    Pobierz Wartosc Z Listy    ${LogotypLogoLewe Id}
    Log    ${LogotypLogoLewe}
    Set Suite Variable    ${LogotypLogoLewe}
    ${LogotypLogoSrodkowe}    Pobierz Wartosc Z Listy    ${LogotypLogoSrodkowe Id}
    Log    ${LogotypLogoSrodkowe}
    Set Suite Variable    ${LogotypLogoSrodkowe}
    ${LogotypLogoPrawe}    Pobierz Wartosc Z Listy    ${LogotypLogoPrawe Id}
    Log    ${LogotypLogoPrawe}
    Set Suite Variable    ${LogotypLogoPrawe}
    Zamknij Edycje Elementu

Test Logotyp
    [Documentation]    Krok 1: Jeżeli skonfugurowano logo POIR, sprawdź czy w wnioksu jest logo POIR
    ...    Krok 2: Jeżeli skonfugurowano logo POPW, sprawdź czy w wnioksu jest logo POPW
    ...    Krok 3: Jeżeli skonfugurowano logo PARP, sprawdź czy w wnioksu jest logo PARP
    ...    Krok 4: Jeżeli skonfugurowano logo EFRR, sprawdź czy w wnioksu jest logo EFRR
    Run Keyword If    '${LogotypLogoLewe}'=='Logo POIR'    Page Should Contain Image    ${LogotypLogoPOIR XPath}
    Run Keyword If    '${LogotypLogoLewe}'=='Logo POPW'    Page Should Contain Image    ${LogotypLogoPOPW XPath}
    Run Keyword If    '${LogotypLogoSrodkowe}'=='Logo PARP'    Page Should Contain Image    ${LogotypLogoPARP XPath}
    Run Keyword If    '${LogotypLogoPrawe}'=='Logo Programów Regionalnych'    Page Should Contain Image    ${LogotypLogoEFRR XPath}

Edycja Akcje Wniosku
    [Documentation]    Krok 1: Sprawdź, czy element istnieje w tabeli
    ...    Krok 2: Zapisz wiersz, w którym istnieje element
    ...    Krok 3: Jezeli element istnieje, uruchom sprawdzenie konfiguracji elementu
    ...    Krok 4: Jeżeli numer wiersza, w którym istnieje element jest większy od 0, to ustaw zmienną CzyJest na TRUE, w przeciwnym razie na FALSE
    ...    Krok 5: Zapisz wartość zmiennej CzyJest
    ...    Krok 6: Ustaw zmienną CzyJest jako dostepną dla całego pakietu
    ${WierszTabeliAkcjeWniosku}    Element Istnieje W Tabeli    ${AkcjeWnioskuElemetNazwa}    1    ${TabelaZastosowaneElementy XPath}
    Log    ${WierszTabeliAkcjeWniosku}
    Comment    Run Keyword If    ${WierszTabeliAkcjeWniosku} > 0    Konfiguracja Logotyp    ${WierszTabeliAkcjeWniosku}
    ${CzyJestAkcjeWniosku}    Set Variable If    ${WierszTabeliAkcjeWniosku} > 0    ${TRUE}    ${FALSE}
    Log    ${CzyJestAkcjeWniosku}
    Set Suite Variable    ${CzyJestAkcjeWniosku}

Test Akcje Wniosku
    [Documentation]    Krok 1: Strona powinna zawierać przycisk Sprawdź poprawność
    ...    Krok 2: Pobierz klasę przycisku
    ...    Krok 3: Klasa przycisku powinna zawierać kolorowanie przycisku na niebiesko
    ...    Krok 4: Strona powinna zawierać przycisk Wydruk PDF
    ...    Krok 5: Pobierz klasę przycisku
    ...    Krok 6: Klasa przycisku powinna zawierać kolorowanie przycisku na niebiesko
    ...    Krok 7: Strona powinna zawierać przycisk Zapisz
    ...    Krok 8: Pobierz klasę przycisku
    ...    Krok 9: Klasa przycisku powinna zawierać kolorowanie przycisku na niebiesko
    ...    Krok 10: Strona powinna zawierać przycisk Złóż
    ...    Krok 11: Pobierz klasę przycisku
    ...    Krok 12: Klasa przycisku powinna zawierać kolorowanie przycisku na zielono
    Page Should Contain Element    ${AkcjeWnioskuSprawdzPoprawnosc Id}
    ${Klasa}    Pobierz Klase Elementu    ${AkcjeWnioskuSprawdzPoprawnosc Id}
    Should Contain    ${Klasa}    ${AkcjeWnioskuPrzyciskNiebieskiKlasa}
    Page Should Contain Element    ${AkcjeWnioskuWydrukPDF Id}
    ${Klasa}    Pobierz Klase Elementu    ${AkcjeWnioskuWydrukPDF Id}
    Should Contain    ${Klasa}    ${AkcjeWnioskuPrzyciskNiebieskiKlasa}
    Page Should Contain Element    ${AkcjeWnioskuZapisz Id}
    ${Klasa}    Pobierz Klase Elementu    ${AkcjeWnioskuZapisz Id}
    Should Contain    ${Klasa}    ${AkcjeWnioskuPrzyciskNiebieskiKlasa}
    Page Should Contain Element    ${AkcjeWnioskuZloz Id}
    ${Klasa}    Pobierz Klase Elementu    ${AkcjeWnioskuZloz Id}
    Should Contain    ${Klasa}    ${AkcjeWnioskuPrzyciskZielonyKlasa}

Edycja Ogolne Informacje O Projekcie
    [Documentation]    Krok 1: Sprawdź, czy element istnieje w tabeli
    ...    Krok 2: Zapisz wiersz, w którym istnieje element
    ...    Krok 3: Jezeli element istnieje, uruchom sprawdzenie konfiguracji elementu
    ...    Krok 4: Jeżeli numer wiersza, w którym istnieje element jest większy od 0, to ustaw zmienną CzyJest na TRUE, w przeciwnym razie na FALSE
    ...    Krok 5: Zapisz wartość zmiennej CzyJest
    ...    Krok 6: Ustaw zmienną CzyJest jako dostepną dla całego pakietu
    ${WierszTabeliOgolneInformacje}    Element Istnieje W Tabeli    ${OgolneInformacjeOProjekcieNazwa}    1    ${TabelaZastosowaneElementy XPath}
    Log    ${WierszTabeliOgolneInformacje}
    Run Keyword If    ${WierszTabeliOgolneInformacje} > 0    Konfiguracja Ogolne Informacje O Projekcie    ${WierszTabeliOgolneInformacje}
    ${CzyJestOgolneInformacje}    Set Variable If    ${WierszTabeliOgolneInformacje} > 0    ${TRUE}    ${FALSE}
    Log    ${CzyJestOgolneInformacje}
    Set Suite Variable    ${CzyJestOgolneInformacje}

Konfiguracja Ogolne Informacje O Projekcie
    [Arguments]    ${WierszTabeliOgolneInformacje}
    [Documentation]    Krok 1: Otwórz edycję elementu Ogólne informacje o projekcie
    ...    Krok 2: Pobierz nazwę elementu, która bedzie wyświetlana na wniosku
    ...    Krok 3: Zapisz wartość nazwę elementu, która bedzie wyświetlana na wniosku
    ...    Krok 4: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 5: Pobierz wartość i zaznacz checkbox
    ...    Krok 6: Zapisz wartość wybranego checkboxa
    ...    Krok 7: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 8: Pobierz wartość i zaznacz checkbox
    ...    Krok 9: Zapisz wartość wybranego checkboxa
    ...    Krok 10: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 11: Pobierz wartość i zaznacz checkbox
    ...
    ...    Krok n-1: Sprawdź, czy wszystkie checkbox-y są zaznaczone.
    ...    Krok n: Zamknij edycję elementu Ogólne informacje o projekcie
    Otworz Edycje Elementu    ${WierszTabeliOgolneInformacje}
    ${OIOPLabel}    Get Value    ${ModalNazwa Id}
    Log    ${OIOPLabel}
    Set Suite Variable    ${OIOPLabel}
    ${OIOPNumerWnioskuPoprzedniEtap}    Sprawdz Checkbox I Zaznacz    ${OIOPNumerWnioskuPoprzedniEtap XPath}
    Log    ${OIOPNumerWnioskuPoprzedniEtap}
    Set Suite Variable    ${OIOPNumerWnioskuPoprzedniEtap}
    ${OIOPKodWnioskuPoprzedniEtap}    Sprawdz Checkbox I Zaznacz    ${OIOPKodWnioskuPoprzedniEtap XPath}
    Log    ${OIOPKodWnioskuPoprzedniEtap}
    Set Suite Variable    ${OIOPKodWnioskuPoprzedniEtap}
    ${OIOPUkryjNumerNaboru1Etap}    Sprawdz Checkbox I Zaznacz    ${OIOPUkryjNumerNaboru1Etap XPath}
    Log    ${OIOPUkryjNumerNaboru1Etap}
    Set Suite Variable    ${OIOPUkryjNumerNaboru1Etap}
    ${OIOPTytulProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPTytulProjektu XPath}
    Log    ${OIOPTytulProjektu}
    Set Suite Variable    ${OIOPTytulProjektu}
    ${OIOPDataZlozeniaWNPKoncowaIEtap}    Sprawdz Checkbox I Zaznacz    ${OIOPDataZlozeniaWNPKoncowaIEtap Xpath}
    Log    ${OIOPDataZlozeniaWNPKoncowaIEtap}
    Set Suite Variable    ${OIOPDataZlozeniaWNPKoncowaIEtap}
    ${OIOPKrotkiOpisProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPKrotkiOpisProjektu XPath}
    Log    ${OIOPKrotkiOpisProjektu}
    Set Suite Variable    ${OIOPKrotkiOpisProjektu}
    ${OIOPRozszerzonyOpisProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPRozszerzonyOpisProjektu XPath}
    Log    ${OIOPRozszerzonyOpisProjektu}
    Set Suite Variable    ${OIOPRozszerzonyOpisProjektu}
    ${OIOPCelProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPCelProjektu XPath}
    Log    ${OIOPCelProjektu}
    Set Suite Variable    ${OIOPCelProjektu}
    ${OIOPOpisProcesuProjektowego}    Sprawdz Checkbox I Zaznacz    ${OIOPOpisProcesuProjektowego XPath}
    Log    ${OIOPOpisProcesuProjektowego}
    Set Suite Variable    ${OIOPOpisProcesuProjektowego}
    ${OIOPOpisDzialanWdrozeniowych}    Sprawdz Checkbox I Zaznacz    ${OIOPOpisDzialanWdrozeniowych XPath}
    Log    ${OIOPOpisDzialanWdrozeniowych}
    Set Suite Variable    ${OIOPOpisDzialanWdrozeniowych}
    ${OIOPSlowaKluczowe}    Sprawdz Checkbox I Zaznacz    ${OIOPSlowaKluczowe XPath}
    Log    ${OIOPSlowaKluczowe}
    Set Suite Variable    ${OIOPSlowaKluczowe}
    ${OIOPDziedzinyProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPDziedzinyProjektu XPath}
    Log    ${OIOPDziedzinyProjektu}
    Set Suite Variable    ${OIOPDziedzinyProjektu}
    ${OIOPZnaczenieProjektu}    Sprawdz Checkbox I Zaznacz    ${OIOPZnaczenieProjektu XPath}
    Log    ${OIOPZnaczenieProjektu}
    Set Suite Variable    ${OIOPZnaczenieProjektu}
    ${OIOPOczekiwaneEfekty}    Sprawdz Checkbox I Zaznacz    ${OIOPOczekiwaneEfekty XPath}
    Log    ${OIOPOczekiwaneEfekty}
    Set Suite Variable    ${OIOPOczekiwaneEfekty}
    ${OIOPOczekiwaneEfekty10000}    Sprawdz Checkbox I Zaznacz    ${OIOPOczekiwaneEfekty10000 XPath}
    Log    ${OIOPOczekiwaneEfekty10000}
    Set Suite Variable    ${OIOPOczekiwaneEfekty10000}
    ${OIOPProjektDotyczyInnowcjiProduktowej}    Sprawdz Checkbox I Zaznacz    ${OIOPProjektDotyczyInnowcjiProduktowej XPath}
    ${LoginButton Id}    ${OIOPProjektDotyczyInnowcjiProduktowej}
    Set Suite Variable    ${OIOPProjektDotyczyInnowcjiProduktowej}
    ${OIOPPoziomInnowacyjnosci}    Sprawdz Checkbox I Zaznacz    ${OIOPPoziomInnowacyjnosci XPath}
    Log    ${OIOPPoziomInnowacyjnosci}
    Set Suite Variable    ${OIOPPoziomInnowacyjnosci}
    ${OIOPOpisInnowacji}    Sprawdz Checkbox I Zaznacz    ${OIOPOpisInnowacji XPath}
    Log    ${OIOPOpisInnowacji}
    Set Suite Variable    ${OIOPOpisInnowacji}
    ${OIOPOkresRealizacjiProjektuPoczatek}    Sprawdz Checkbox I Zaznacz    ${OIOPOkresRealizacjiProjektuPoczatek XPath}
    Log    ${OIOPOkresRealizacjiProjektuPoczatek}
    Set Suite Variable    ${OIOPOkresRealizacjiProjektuPoczatek}
    ${OIOPOkresRealizacjiProjektuKoniec}    Sprawdz Checkbox I Zaznacz    ${OIOPOkresRealizacjiProjektuKoniec XPath}
    Log    ${OIOPOkresRealizacjiProjektuKoniec}
    Set Suite Variable    ${OIOPOkresRealizacjiProjektuKoniec}
    ${OIOPInstrumentyFinansowe}    Sprawdz Checkbox I Zaznacz    ${OIOPInstrumentyFinansowe XPath}
    Log    ${OIOPInstrumentyFinansowe}
    Set Suite Variable    ${OIOPInstrumentyFinansowe}
    ${OIOPKategorieDrogi}    Sprawdz Checkbox I Zaznacz    ${OIOPKategorieDrogi XPath}
    Log    ${OIOPKategorieDrogi}
    Set Suite Variable    ${OIOPKategorieDrogi}
    ${OIOPOkresWdrozeniaInnowacjiPoczatek}    Sprawdz Checkbox I Zaznacz    ${OIOPOkresWdrozeniaInnowacjiPoczatek XPath}
    Log    ${OIOPOkresWdrozeniaInnowacjiPoczatek}
    Set Suite Variable    ${OIOPOkresWdrozeniaInnowacjiPoczatek}
    ${OIOPOkresWdrozeniaInnowacjiKoniec}    Sprawdz Checkbox I Zaznacz    ${OIOPOkresWdrozeniaInnowacjiKoniec XPath}
    Log    ${OIOPOkresWdrozeniaInnowacjiKoniec}
    Set Suite Variable    ${OIOPOkresWdrozeniaInnowacjiKoniec}
    Wszystkie Checkboxy Sa Zaznaczone    ${ModalCheckbox XPath}    # Sprawdza, czy żaden checkbox w konfiguracji nie został pominięty
    Zamknij Edycje Elementu

Test Ogolne Informacje O Projekcie
    [Documentation]    Krok 1: Strona powinna zawierać zdefiniowaną nazwę elementu
    ...    Krok 2: Sprawdź, czy są wszystkie pola
    ...    Krok 3: Sprawdź, na pustych polach
    ...    Krok 4: Sprawdź, na wypełnionych polach tekstowych
    ...    Krok 5: Sprawdź, na wypełnionych polach data
    ...    Krok 6: Sprawdź, na wypełnionych polach słowa kluczowe
    Page Should Contain    ${OIOPLabel}
    Test Ogolne Informacje O Projekcie Pola
    Test Ogolne Informacje O Projekcie Puste
    Test Ogolne Informacje O Projekcie Tekstowe
    Test Ogolne Informacje O Projekcie Daty
    Comment    Test Ogolne Informacje O Projekcie Slowa Kluczowe

Test Ogolne Informacje O Projekcie Pola
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdza, czy odpowiednia etykieta jest na stronie
    Run Keyword If    ${OIOPNumerWnioskuPoprzedniEtap}    Page Should Contain    ${OIOPNumerWnioskuPoprzedniEtapLabel}
    Run Keyword If    ${OIOPKodWnioskuPoprzedniEtap}    Page Should Contain    ${OIOPKodWnioskuPoprzedniEtapLabel}
    Run Keyword If    ${OIOPTytulProjektu}    Page Should Contain    ${OIOPTytulProjektuLabel}
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Page Should Contain    ${OIOPKrotkiOpisProjektuLabel}
    Run Keyword If    ${OIOPRozszerzonyOpisProjektu}    Page Should Contain    ${OIOPRozszerzonyOpisProjektuLabel}
    Run Keyword If    ${OIOPCelProjektu}    Page Should Contain    ${OIOPCelProjektuLabel}
    Run Keyword If    ${OIOPOpisProcesuProjektowego}    Page Should Contain    ${OIOPOpisProcesuProjektowegoLabel}
    Run Keyword If    ${OIOPOpisDzialanWdrozeniowych}    Page Should Contain    ${OIOPOpisDzialanWdrozeniowychLabel}
    Run Keyword If    ${OIOPSlowaKluczowe}    Page Should Contain    ${OIOPSlowaKluczoweLabel}
    Run Keyword If    ${OIOPSlowaKluczowe}    Page Should Contain Button    ${OIOPSlowaKluczoweButton XPath}    # Przycisk Dodaj
    Run Keyword If    ${OIOPZnaczenieProjektu}    Page Should Contain    ${OIOPZnaczenieProjektuLabel}
    Run Keyword If    ${OIOPOczekiwaneEfekty}    Page Should Contain    ${OIOPOczekiwaneEfektyLabel}
    Run Keyword If    ${OIOPOczekiwaneEfekty10000}    Page Should Contain    ${OIOPOczekiwaneEfekty10000Label}
    Run Keyword If    ${OIOPProjektDotyczyInnowcjiProduktowej}    Page Should Contain    ${OIOPProjektDotyczyInnowcjiProduktowejLabel}
    Run Keyword If    ${OIOPPoziomInnowacyjnosci}    Page Should Contain    ${OIOPPoziomInnowacyjnosciLabel}
    Run Keyword If    ${OIOPOpisInnowacji}    Page Should Contain    ${OIOPOpisInnowacjiLabel}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Page Should Contain    ${OIOPOkresRealizacjiProjektuPoczatekLabel}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuKoniec}    Page Should Contain    ${OIOPOkresRealizacjiProjektuKoniecLabel}
    Run Keyword If    ${OIOPInstrumentyFinansowe}    Page Should Contain    ${OIOPInstrumentyFinansoweLabel}
    Run Keyword If    ${OIOPKategorieDrogi}    Page Should Contain    ${OIOPKategorieDrogiLabel}
    Run Keyword If    ${OIOPOkresWdrozeniaInnowacjiPoczatek}    Page Should Contain    ${OIOPOkresWdrozeniaInnowacjiPoczatekLabel}
    Run Keyword If    ${OIOPOkresWdrozeniaInnowacjiKoniec}    Page Should Contain    ${OIOPOkresWdrozeniaInnowacjiKoniecLabel}

Test Ogolne Informacje O Projekcie Puste
    [Documentation]    Krok 1: Wprowadź puste ciągi znaków do pól
    ...    Krok 2: Zapisz i przeładuj
    ...    Krok 3: Sprawdź, czy pola są puste
    ...    Krok 4: Otwórz okno walidacji
    ...    Krok 5: Sprawdź, czy wyświetlają się komunikaty o konieczności uzupełnienia pola
    ...    Krok 6: Zamknij okno walidacji
    Test Ogolne Informacje O Projekcie Puste Wprowadz
    Zapisz Dane I Przeladuj
    Test Ogolne Informacje O Projekcie Puste Sprawdz
    Otworz Okno Walidacji
    Test Ogolne Informacje O Projekcie Puste Walidacja
    Zamknij Okno Walidacji

Test Ogolne Informacje O Projekcie Puste Wprowadz
    [Documentation]    Jeżeli pole jest skonfigurowane, to wprowadź do niego pusty ciąg znaków
    Run Keyword If    ${OIOPTytulProjektu}    Input Text    ${OIOPTytulProjektu Id}    ${EMPTY}
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Input Text    ${OIOPKrotkiOpisProjektuId}    ${EMPTY}
    Run Keyword If    ${OIOPCelProjektu}    Input Text    ${OIOPCelProjektuId}    ${EMPTY}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Wyczysc Pole Data    ${OIOPOkresRealizacjiProjektuPoczatekId}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuKoniec}    Wyczysc Pole Data    ${OIOPOkresRealizacjiProjektuKoniecId}

Test Ogolne Informacje O Projekcie Puste Sprawdz
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdź, czy zawiera pusty ciąg znaków
    Run Keyword If    ${OIOPTytulProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPTytulProjektu Id}    ${EMPTY}
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPKrotkiOpisProjektuId}    ${EMPTY}
    Run Keyword If    ${OIOPCelProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPCelProjektuId}    ${EMPTY}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPOkresRealizacjiProjektuPoczatekId}    ${EMPTY}
    Run Keyword If    ${OIOPOkresRealizacjiProjektuKoniec}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPOkresRealizacjiProjektuKoniecId}    ${EMPTY}

Test Ogolne Informacje O Projekcie Puste Walidacja
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdza, czy walidacja wyświetla komunikat o konieczności wypełnienia pola.
    ...    *Dla komunikatu "Cel projektu: To pole jest obowiązkowe mamy jakieś VooDoo".* Nawet poza Selenium nie znajduje XPath = //.[@id='modal']//a[text()='Cel projektu: To pole jest obowiązkowe'], choć wystarczy wpisać dowolny inny komunikat z dowolnymi znakami i znajduje bez problemu. Contains też nie działa.
    ...    W przypadku pól Okres realizacji projektu <od> i Okres realizacji projektu <do> sprawdza również, czy nie są wyświetlane błędne komunikaty - Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja.
    Run Keyword If    ${OIOPTytulProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPTytulProjektuLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPKrotkiOpisProjektuLabel}    ${WalidacjaPoleObowiazkowe}    1
    Comment    Run Keyword If    ${OIOPCelProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPCelProjektuLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja    1    1    0    0
    ...    0    0    0    0

Test Ogolne Informacje O Projekcie Tekstowe
    [Documentation]    Krok 1: Wprowadź losowe ciągi znaków do pól
    ...    Krok 2: Zapisz i przeładuj
    ...    Krok 3: Sprawdź, czy pola zawierają wprowadzone dane
    ...    Krok 4: Otwórz okno walidacji
    ...    Krok 5: Sprawdź, komunikaty walidacji
    ...    Krok 6: Zamknij okno walidacji
    Test Ogolne Informacje O Projekcie Tekstowe Wprowadz
    Zapisz Dane I Przeladuj
    Test Ogolne Informacje O Projekcie Tekstowe Sprawdz
    Otworz Okno Walidacji
    Test Ogolne Informacje O Projekcie Tekstowe Walidacja
    Zamknij Okno Walidacji

Test Ogolne Informacje O Projekcie Tekstowe Wprowadz
    [Documentation]    Jeżeli pole jest skonfigurowane, to wprowadź do niego losowy ciąg znaków
    ${OIOPTytulProjektuTresc}    Run Keyword If    ${OIOPTytulProjektu}    Wypelnij Pole Losowym Tekstem    ${OIOPTytulProjektu Id}    ${OIOPTytulProjektuLen}
    Set Suite Variable    ${OIOPTytulProjektuTresc}
    ${OIOPKrotkiOpisProjektuTresc}    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Wypelnij Pole Losowym Tekstem    ${OIOPKrotkiOpisProjektuId}    ${OIOPKrotkiOpisProjektuLen}
    Set Suite Variable    ${OIOPKrotkiOpisProjektuTresc}
    ${OIOPCelProjektuTresc}    Run Keyword If    ${OIOPCelProjektu}    Wypelnij Pole Losowym Tekstem    ${OIOPCelProjektuId}    ${OIOPCelProjektuLen}
    Set Suite Variable    ${OIOPCelProjektuTresc}

Test Ogolne Informacje O Projekcie Tekstowe Sprawdz
    [Documentation]    Krok 1: Jeżeli pole jest skonfigurowane, to sprawdź, czy zawiera wprowadzny wczesniej ciąg znaków
    ...    Krok 2: Jeżeli pole jest skonfigurowane, to sprawdź, czy wprowadzny ciąg znaków znajduje się w bazie w odpowiedniej tabeli i polu (nie działa dla ciagów znaków zawierających BR - trzeba zakomentować)
    Run Keyword If    ${OIOPTytulProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPTytulProjektu Id}    ${OIOPTytulProjektuTresc}
    Run Keyword If    ${OIOPTytulProjektu}    Pole O Wartosci Jest W Bazie    ${OIOPTytulProjektuBazaTabela}    ${OIOPTytulProjektuBazaPole}    ${OIOPTytulProjektuTresc}
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPKrotkiOpisProjektuId}    ${OIOPKrotkiOpisProjektuTresc}
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Pole O Wartosci Jest W Bazie    ${OIOPTytulProjektuBazaTabela}    ${OIOPKrotkiOpisProjektuPole}    ${OIOPKrotkiOpisProjektuTresc}
    Run Keyword If    ${OIOPCelProjektu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${OIOPCelProjektuId}    ${OIOPCelProjektuTresc}
    Run Keyword If    ${OIOPCelProjektu}    Pole O Wartosci Jest W Bazie    ${OIOPTytulProjektuBazaTabela}    ${OIOPCelProjektuPole}    ${OIOPCelProjektuTresc}

Test Ogolne Informacje O Projekcie Tekstowe Walidacja
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdza, czy walidacja nie wyświetla komunikatu o konieczności wypełnienia pola.
    ...    *Dla komunikatu "Cel projektu: To pole jest obowiązkowe mamy jakieś VooDoo".* Nawet poza Selenium nie znajduje XPath = //.[@id='modal']//a[text()='Cel projektu: To pole jest obowiązkowe'], choć wystarczy wpisać dowolny inny komunikat z dowolnymi znakami i znajduje bez problemu. Contains też nie działa.
    Run Keyword If    ${OIOPTytulProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPTytulProjektuLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${OIOPKrotkiOpisProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPKrotkiOpisProjektuLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${OIOPCelProjektu}    Walidacja Zawiera Label Z Komunikatem    ${OIOPCelProjektuLabel}    ${WalidacjaPoleObowiazkowe}    0

Test Ogolne Informacje O Projekcie Daty
    [Documentation]    Krok 1: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to wprowadź daty minimalne realizacji do pól
    ...    Krok 2: Zapisz dane i odświerz stronę
    ...    Krok 3: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź daty minimalne realizacji w polach
    ...    Krok 4: Otwórz okno walidacji
    ...    Krok 5: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź walidację dat minimalnych realizacji w polach
    ...    Krok 6: Zamknij okno walidacji
    ...    Krok 7: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to wprowadź daty maksymalne realizacji do pól
    ...    Krok 8: Zapisz dane i odświerz stronę
    ...    Krok 9: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź daty maksymalne realizacji w polach
    ...    Krok 10: Otwórz okno walidacji
    ...    Krok 11: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź walidację dat maksymalnych realizacji w polach
    ...    Krok 12: Zamknij okno walidacji
    ...    Krok 13: Pobierz dzisiejszą datę i zwiększ ją o 2 dni
    ...    Krok 14: Pobierz dzisiejszą datę i zwiększ ją o 365 dni
    ...    Krok 15: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to wprowadź uzyskane daty realizacji do pól
    ...    Krok 16: Zapisz dane i odświerz stronę
    ...    Krok 17: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź uzyskane daty realizacji w polach
    ...    Krok 18: Otwórz okno walidacji
    ...    Krok 19: Jeżeli skonfigurowano pole "Okres realizacji projektu <od>", to sprawdź walidację uzyskanych dat realizacji w polach
    ...    Krok 20: Zamknij okno walidacji
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Wprowadz    ${OIOPOkresRealizacjiProjektuPoczatekMin}    ${OIOPOkresRealizacjiProjektuKoniecMin}
    Zapisz Dane I Przeladuj
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Sprawdz    ${OIOPOkresRealizacjiProjektuPoczatekMin}    ${OIOPOkresRealizacjiProjektuKoniecMin}
    Otworz Okno Walidacji
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja    0    0    1    1
    ...    1    0    0    0
    Zamknij Okno Walidacji
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Wprowadz    ${OIOPOkresRealizacjiProjektuPoczatekMax}    ${OIOPOkresRealizacjiProjektuKoniecMax}
    Zapisz Dane I Przeladuj
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Sprawdz    ${OIOPOkresRealizacjiProjektuPoczatekMax}    ${OIOPOkresRealizacjiProjektuKoniecMax}
    Otworz Okno Walidacji
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja    0    0    0    0
    ...    0    1    1    1
    Zamknij Okno Walidacji
    ${OIOPOkresRealizacjiProjektuPoczatekData}    Dodaj Do Dzisiejszej Daty    2 days
    ${OIOPOkresRealizacjiProjektuKoniecData}    Dodaj Do Dzisiejszej Daty    365 days
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Wprowadz    ${OIOPOkresRealizacjiProjektuPoczatekData}    ${OIOPOkresRealizacjiProjektuKoniecData}
    Zapisz Dane I Przeladuj
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Sprawdz    ${OIOPOkresRealizacjiProjektuPoczatekData}    ${OIOPOkresRealizacjiProjektuKoniecData}
    Otworz Okno Walidacji
    Run Keyword If    ${OIOPOkresRealizacjiProjektuPoczatek}    Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja    0    0    0    0
    ...    0    0    0    0
    Zamknij Okno Walidacji

Test Ogolne Informacje O Projekcie Daty Realizacji Wprowadz
    [Arguments]    ${DataRozpoczecia}    ${DataZakonczenia}
    [Documentation]    Wprowadza daty do pól "Okres realizacji projektu <od>" oraz "Okres realizacji projektu <do>". Przyjmuje datę w formacie rrrrmmdd - bez myślników
    ...    Krok 1: Wywołaj błąd, jeżeli obie daty nie zostały skonfigurowane.
    ...    Krok 2: Sprawdź, czy system pilnuje formatu daty dla wskazanego pola
    ...    Krok 3: Sprawdź, czy system pilnuje formatu daty dla wskazanego pola
    ...    Krok 4: Wprawadź datę rozpoczęcia do pola "Okres realizacji projektu <od>"
    ...    Krok 5: Wprowadź datę zakończenia do pola "Okres realizacji projektu <do>"
    Run Keyword If    (${OIOPOkresRealizacjiProjektuPoczatek} and ${OIOPOkresRealizacjiProjektuKoniec}==0) or (${OIOPOkresRealizacjiProjektuPoczatek}==0 and ${OIOPOkresRealizacjiProjektuKoniec})    Fail    Sposób kontroli walidacji dat rozpoczęcia i zakończenia realizacji przewiduje, że obie te daty zostały skonfigurowane
    Pilnuj Formatu Daty    ${OIOPOkresRealizacjiProjektuPoczatekId}
    Pilnuj Formatu Daty    ${OIOPOkresRealizacjiProjektuKoniecId}
    Wprowadz Date    ${OIOPOkresRealizacjiProjektuPoczatekId}    ${DataRozpoczecia}
    Wprowadz Date    ${OIOPOkresRealizacjiProjektuKoniecId}    ${DataZakonczenia}

Test Ogolne Informacje O Projekcie Daty Realizacji Sprawdz
    [Arguments]    ${DataRozpoczecia}    ${DataZakonczenia}
    [Documentation]    Sprawdza, czy daty z pól "Okres realizacji projektu <od>" oraz "Okres realizacji projektu <do>" są równe datom podanym jako argumenty słowa kluczowego. Przyjmuje daty w formacie rrrrmmdd - bez myślników.
    ...    Krok 1: Pobierz datę z pola "Okres realizacji projektu <od>"
    ...    Krok 2: Pobierz datę z pola "Okres realizacji projektu <do>"
    ...    Krok 3: Zapisz pobraną datę
    ...    Krok 4: Zapisz pobraną datę
    ...    Krok 5: Sprawdź, czy data pobrana z pola "Okres realizacji projektu <od>" jest równa podanej dacie rozpoczęcia
    ...    Krok 6: Sprawdź, czy data pobrana z pola "Okres realizacji projektu <do>" jest równa podanej dacie zakończenia
    ${DataRozpoczeciaZPola}    Get Value    ${OIOPOkresRealizacjiProjektuPoczatekId}
    ${DataZakonczeniaZPola}    Get Value    ${OIOPOkresRealizacjiProjektuKoniecId}
    Log    ${DataRozpoczeciaZPola}
    Log    ${DataZakonczeniaZPola}
    Should Be Equal    ${DataRozpoczeciaZPola}    ${DataRozpoczecia}
    Should Be Equal    ${DataZakonczeniaZPola}    ${DataZakonczenia}

Test Ogolne Informacje O Projekcie Daty Realizacji Walidacja
    [Arguments]    @{ListaWalidacji}
    [Documentation]    Testuje walidacje dla pól "Okres realizacji projektu <od>" oraz "Okres realizacji projektu <do>". Sposób sprawdzania walidacji wymaga, aby ww. pola były skonfigurowane. Parametry konfiguracyjne słowa kluczowego podaje się w formie listy.
    ...    1 - komunikat powinien być widoczny
    ...    0 - komunita nie powinien być widoczny
    ...    Krok 1: Wywołaj błąd, jeżeli obie daty nie zostały skonfigurowane.
    ...    Krok 2: Stwórz zmienną Index (ten sposób tworzenia zmiennej nadaje jej typ liczbowy)
    ...    Krok 3: Stwórz zmienną Walidacja
    ...    Krok 4: Dla każdego elementu z listy
    ...    Krok 4.1: Stwórz zmienną WalidacjaIndex o wartości elementu z listy (w ten sposób można utworzyć tylko zmienną test, suite lub global).
    ...    Krok 4.2: Podbij zmienną Index
    ...    Krok 7: Sprawdza komunikaty walidacji
    Run Keyword If    (${OIOPOkresRealizacjiProjektuPoczatek} and ${OIOPOkresRealizacjiProjektuKoniec}==0) or (${OIOPOkresRealizacjiProjektuPoczatek}==0 and ${OIOPOkresRealizacjiProjektuKoniec})    Fail    Sposób kontroli walidacji dat rozpoczęcia i zakończenia realizacji przewiduje, że obie te daty zostały skonfigurowane
    ${Index}    Evaluate    1
    ${Walidacja}    Set Variable    Walidacja
    : FOR    ${i}    IN    @{ListaWalidacji}
    \    Set Test Variable    ${${Walidacja}${Index}}    ${i}
    \    ${Index}    Evaluate    ${Index}+1
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuPoczatekLabel}    ${WalidacjaPoleObowiazkowe}    ${Walidacja1}
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuKoniecLabel}    ${WalidacjaPoleObowiazkowe}    ${Walidacja2}
    Walidacja Zawiera Label Z Komunikatem    \    ${WalidacjaDataOdMniejszaOdJutro}    ${Walidacja3}
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuPoczatekLabel}    ${WalidacjaDataNieMozeBycWczesniejsza}    ${Walidacja4}
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuKoniecLabel}    ${WalidacjaDataNieMozeBycWczesniejsza}    ${Walidacja5}
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuPoczatekLabel}    ${WalidacjaDataNieMozeBycPozniejsza}    ${Walidacja6}
    Walidacja Zawiera Label Z Komunikatem    ${OIOPOkresRealizacjiProjektuKoniecLabel}    ${WalidacjaDataNieMozeBycPozniejsza}    ${Walidacja7}
    Walidacja Zawiera Label Z Komunikatem    \    ${WalidacjaDataOdMniejszaRownaDataDo}    ${Walidacja8}

Test Ogolne Informacje O Projekcie Slowa Kluczowe
    [Documentation]    Krok 1: Jeżeli skonfigurowano element "Słowa kluczowe", to dodaj wskazaną liczbę wypełnionych rekordów
    ...    Krok 2: Zapisz dane i odświerz stronę
    ...    Krok 3: Jeżeli skonfigurowano element "Słowa kluczowe", to sprawdź zawartość i usuń wskazaną liczbę rekordów
    ...    Krok 4: Zapisz dane i odświerz stronę
    ...    Krok 5: Jeżeli skonfigurowano element "Słowa kluczowe", to sprawdź, czy zadana liczba pól została usunięta
    Run Keyword If    ${OIOPSlowaKluczowe}    Test Ogolne Informacje O Projekcie Slowa Kluczowe Wprowadz
    Zapisz Dane I Przeladuj
    Run Keyword If    ${OIOPSlowaKluczowe}    Test Ogolne Informacje O Projekcie Slowa Kluczowe Sprawdz
    Zapisz Dane I Przeladuj
    Run Keyword If    ${OIOPSlowaKluczowe}    Test Ogolne Informacje O Projekcie Slowa Kluczowe Walidacja

Test Ogolne Informacje O Projekcie Slowa Kluczowe Wprowadz
    [Documentation]    Krok 1: Sprawdź, czy liczba pól na słowa i liczba przycisków usuń są równe
    ...    Krok 2: Dodaj podaną liczbę pól na słowa kluczowe za pomocą przycisku dodaj
    Rowna Liczba Dwoch Elementow XPath    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Dodaj Elementy Przyciskiem    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePrzyciskDodaj XPath}    ${OIOPSlowaKluczoweLiczba}    ${OIOPSlowaKluczowePrefix}    ${OIOPSlowaKluczoweLen}

Test Ogolne Informacje O Projekcie Slowa Kluczowe Sprawdz
    [Documentation]    Krok 1: Sprawdź, czy liczba pól na słowa i liczba przycisków usuń są równe
    ...    Krok 2: Sprawdź zawartość i usuń za pomocą przycisku usuń podaną liczbę pól na słowa kluczowe
    Rowna Liczba Dwoch Elementow XPath    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Sprawdz I Usun Elementy Dodane Przyciskiem    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePoleTekstowe Id}    ${OIOPSlowaKluczowePrzyciskUsun XPath}    ${OIOPSlowaKluczoweLiczba}    ${OIOPSlowaKluczowePrefix}

Test Ogolne Informacje O Projekcie Slowa Kluczowe Walidacja
    [Documentation]    Dla tego elementu brak jest zdefiniowanych walidacji
    ...    Krok 1 i Krok 2 Obsługują alert, że cześć danych nie została zapisana. Był moment, że po usuwaniu pól i zapisie, błednie pojawiał się taki komunikat.
    ...    Krok 3 Sprawdź, czy liczba pól na słowa i liczba przycisków usuń są równe
    ...    Krok 4 Sprawdź, czy liczby słów przed dodawaniem i po usunięciu są równe
    Comment    ${Komunikat}    Confirm Action    # Musi być, bo potwierdza odświezenie strony po alercie, który błędnie pojawia się po zapisaniu usunięcia słow kluczowych    # Alert gdzieś zniknął, ale na wszelki wypadek zostawiam w kodzie
    Comment    Log    ${Komunikat}
    Rowna Liczba Dwoch Elementow XPath    ${OIOPSlowaKluczowePoleTekstowe XPath}    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Sprawdz Czy Zmienne Przed I Po Rowne    ${OIOPSlowaKluczowePrefix}

Test Ogolne Informacje O Projekcie Slowa Kluczowe Roboczy
    ${LiczbaSlowKluczowychPrzed}    Get Matching Xpath Count    ${OIOPSlowaKluczowePoleTekstowe XPath}
    ${LiczbaUsunSlowoKluczowePrzed}    Get Matching Xpath Count    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Should Be Equal    ${LiczbaSlowKluczowychPrzed}    ${LiczbaUsunSlowoKluczowePrzed}
    ${Slowo}    Set Variable    Slowo
    ${LiczbaSlowKluczowychPo}    Evaluate    ${LiczbaSlowKluczowychPrzed} + ${OIOPSlowaKluczoweLiczba}
    ${OIOPSlowaKluczowePoleOstatnie XPath}    XPath Ostatni    ${OIOPSlowaKluczowePoleTekstowe XPath}
    ${OIOPSlowaKluczowePrzyciskUsunOstatni XPath}    XPath Ostatni    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    : FOR    ${Index}    IN RANGE    ${LiczbaSlowKluczowychPrzed}    ${LiczbaSlowKluczowychPo}
    \    Click Button    ${OIOPSlowaKluczowePrzyciskDodaj XPath}
    \    ${Tekst}    Wypelnij Pole Losowym Tekstem    ${OIOPSlowaKluczowePoleOstatnie XPath}    255
    \    Set Test Variable    ${${Slowo}${Index}}    ${Tekst}
    \    Execute JavaScript    window.scroll(0,window.pageYOffset - 50)    # przesuwa stronę o 50 px do góry, bo przycisk zapisz jest załonięty przez akcje wniosku
    Zapisz Dane I Przeladuj
    ${LiczbaSlowKluczowychPo}    Get Matching Xpath Count    ${OIOPSlowaKluczowePoleTekstowe XPath}
    ${LiczbaUsunSlowoKluczowePo}    Get Matching Xpath Count    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Should Be Equal    ${LiczbaSlowKluczowychPo}    ${LiczbaUsunSlowoKluczowePo}
    ${LiczbaSlowKluczowychRoznica}    Evaluate    ${LiczbaSlowKluczowychPo} - ${LiczbaSlowKluczowychPrzed}
    ${OIOPSlowaKluczoweLiczba}    Evaluate    ${OIOPSlowaKluczoweLiczba}
    Should Be Equal    ${LiczbaSlowKluczowychRoznica}    ${OIOPSlowaKluczoweLiczba}
    : FOR    ${Index}    IN RANGE    ${LiczbaSlowKluczowychPo}    ${LiczbaSlowKluczowychPrzed}    -1
    \    ${Index}    Convert To String    ${Index-1}
    \    ${PoleTekstowe Id}    Replace String    ${OIOPSlowaKluczowePoleTekstowe Id}    x    ${Index}
    \    ${Tekst}    Get Value    ${PoleTekstowe Id}
    \    Should Be Equal    ${Tekst}    ${${Slowo}${Index}}
    \    Click Button    ${OIOPSlowaKluczowePrzyciskUsunOstatni XPath}
    \    Execute JavaScript    window.scroll(0,window.pageYOffset - 100)    # przesuwa stronę o 50 px do góry, bo przycisk usuń jest załonięty przez akcje wniosku
    Zapisz Dane I Przeladuj
    Comment    ${Komunikat}    Confirm Action    # Musi być, bo potwierdza odświezenie strony po alercie, który błędnie pojawia się po zapisaniu usunięcia słow kluczowych    # Alert gdzieś zniknął, ale na wszelki wypadek zostawiam w kodzie
    Comment    Log    ${Komunikat}
    ${LiczbaSlowKluczowychPo}    Get Matching Xpath Count    ${OIOPSlowaKluczowePoleTekstowe XPath}
    ${LiczbaUsunSlowoKluczowePo}    Get Matching Xpath Count    ${OIOPSlowaKluczowePrzyciskUsun XPath}
    Should Be Equal    ${LiczbaSlowKluczowychPo}    ${LiczbaUsunSlowoKluczowePo}
    Should Be Equal    ${LiczbaSlowKluczowychPo}    ${LiczbaSlowKluczowychPrzed}

Edycja Wnioskodawca Ogolne
    [Documentation]    Krok 1: Sprawdź, czy element istnieje w tabeli
    ...    Krok 2: Zapisz wiersz, w którym istnieje element
    ...    Krok 3: Jezeli element istnieje, uruchom sprawdzenie konfiguracji elementu
    ...    Krok 4: Jeżeli numer wiersza, w którym istnieje element jest większy od 0, to ustaw zmienną CzyJest na TRUE, w przeciwnym razie na FALSE
    ...    Krok 5: Zapisz wartość zmiennej CzyJest
    ...    Krok 6: Ustaw zmienną CzyJest jako dostepną dla całego pakietu
    ${WierszTabeliWnioskodawcaOgolne}    Element Istnieje W Tabeli    ${WnioskodawcaOgolneNazwa}    1    ${TabelaZastosowaneElementy XPath}
    Log    ${WierszTabeliWnioskodawcaOgolne}
    Run Keyword If    ${WierszTabeliWnioskodawcaOgolne} > 0    Konfiguracja Wnioskodawca Ogolne    ${WierszTabeliWnioskodawcaOgolne}
    ${CzyJestWnioskodawcaOgolne}    Set Variable If    ${WierszTabeliWnioskodawcaOgolne} > 0    ${TRUE}    ${FALSE}
    Log    ${CzyJestWnioskodawcaOgolne}
    Set Suite Variable    ${CzyJestWnioskodawcaOgolne}

Konfiguracja Wnioskodawca Ogolne
    [Arguments]    ${WierszTabeliOgolneInformacje}
    [Documentation]    Krok 1: Otwórz edycję elementu Ogólne informacje o projekcie
    ...    Krok 2: Pobierz nazwę elementu, która bedzie wyświetlana na wniosku
    ...    Krok 3: Zapisz wartość nazwę elementu, która bedzie wyświetlana na wniosku
    ...    Krok 4: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 5: Pobierz wartość i zaznacz checkbox
    ...    Krok 6: Zapisz wartość wybranego checkboxa
    ...    Krok 7: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 8: Pobierz wartość i zaznacz checkbox
    ...    Krok 9: Zapisz wartość wybranego checkboxa
    ...    Krok 10: Ustaw zmieną dostepną dla całego pakietu
    ...    Krok 11: Pobierz wartość i zaznacz checkbox
    ...
    ...    Krok n-1: Sprawdź, czy wszystkie checkbox-y są zaznaczone.
    ...    Krok n: Zamknij edycję elementu Ogólne informacje o projekcie
    Otworz Edycje Elementu    ${WierszTabeliOgolneInformacje}
    ${WcaOLabel}    Get Value    ${ModalNazwa Id}
    Log    ${WcaOLabel}
    Set Suite Variable    ${WcaOLabel}
    ${WcaONazwa}    Sprawdz Checkbox I Zaznacz    ${WcaONazwa XPath}
    Log    ${WcaONazwa}
    Set Suite Variable    ${WcaONazwa}
    ${WcaONazwaSkrocona}    Sprawdz Checkbox I Zaznacz    ${WcaONazwaSkrocona XPath}
    Log    ${WcaONazwaSkrocona}
    Set Suite Variable    ${WcaONazwaSkrocona}
    ${WcaOStatus}    Sprawdz Checkbox I Zaznacz    ${WcaOStatus XPath}
    Log    ${WcaOStatus}
    Set Suite Variable    ${WcaOStatus}
    ${WcaOStatusWithDuzy}    Sprawdz Checkbox I Zaznacz    ${WcaOStatusWithDuzy XPath}
    Log    ${WcaOStatusWithDuzy}
    Set Suite Variable    ${WcaOStatusWithDuzy}
    ${WcaOStatusWithNieDotyczy}    Sprawdz Checkbox I Zaznacz    ${WcaOStatusWithNieDotyczy XPath}
    Log    ${WcaOStatusWithNieDotyczy}
    Set Suite Variable    ${WcaOStatusWithNieDotyczy}
    ${WcaOStatusMikroMaly}    Sprawdz Checkbox I Zaznacz    ${WcaOStatusMikroMaly XPath}
    Log    ${WcaOStatusMikroMaly}
    Set Suite Variable    ${WcaOStatusMikroMaly}
    ${WcaODataRozpoczeciaDzialalnosci}    Sprawdz Checkbox I Zaznacz    ${WcaODataRozpoczeciaDzialalnosci XPath}
    Log    ${WcaODataRozpoczeciaDzialalnosci}
    Set Suite Variable    ${WcaODataRozpoczeciaDzialalnosci}
    ${WcaOFormaPrawna}    Sprawdz Checkbox I Zaznacz    ${WcaOFormaPrawna XPath}
    Log    ${WcaOFormaPrawna}
    Set Suite Variable    ${WcaOFormaPrawna}
    ${WcaOFormaWlasnosci}    Sprawdz Checkbox I Zaznacz    ${WcaOFormaWlasnosci XPath}
    Log    ${WcaOFormaWlasnosci}
    Set Suite Variable    ${WcaOFormaWlasnosci}
    ${WcaONip}    Sprawdz Checkbox I Zaznacz    ${WcaONip XPath}
    Log    ${WcaONip}
    Set Suite Variable    ${WcaONip}
    ${WcaORegon}    Sprawdz Checkbox I Zaznacz    ${WcaORegon XPath}
    Log    ${WcaORegon}
    Set Suite Variable    ${WcaORegon}
    ${WcaOKrs}    Sprawdz Checkbox I Zaznacz    ${WcaOKrs XPath}
    Log    ${WcaOKrs}
    Set Suite Variable    ${WcaOKrs}
    ${WcaOPesel}    Sprawdz Checkbox I Zaznacz    ${WcaOPesel XPath}
    Log    ${WcaOPesel}
    Set Suite Variable    ${WcaOPesel}
    ${WcaOInnyRejestr}    Sprawdz Checkbox I Zaznacz    ${WcaOInnyRejestr XPath}
    Log    ${WcaOInnyRejestr}
    Set Suite Variable    ${WcaOInnyRejestr}
    ${WcaOPkd}    Sprawdz Checkbox I Zaznacz    ${WcaOPkd XPath}
    Log    ${WcaOPkd}
    Set Suite Variable    ${WcaOPkd}
    ${WcaOMozliwoscOdzyskaniaVat}    Sprawdz Checkbox I Zaznacz    ${WcaOMozliwoscOdzyskaniaVat XPath}
    Log    ${WcaOMozliwoscOdzyskaniaVat}
    Set Suite Variable    ${WcaOMozliwoscOdzyskaniaVat}
    ${WcaOUzasadnienieBrakuOdzyskaniaVAT}    Sprawdz Checkbox I Zaznacz    ${WcaOUzasadnienieBrakuOdzyskaniaVAT XPath}
    Log    ${WcaOUzasadnienieBrakuOdzyskaniaVAT}
    Set Suite Variable    ${WcaOUzasadnienieBrakuOdzyskaniaVAT}
    ${WcaOUdzialWydatkowNaDzialanoscBR}    Sprawdz Checkbox I Zaznacz    ${WcaOUdzialWydatkowNaDzialanoscBR XPath}
    Log    ${WcaOUdzialWydatkowNaDzialanoscBR}
    Set Suite Variable    ${WcaOUdzialWydatkowNaDzialanoscBR}
    ${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBR}    Sprawdz Checkbox I Zaznacz    ${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBR XPath}
    Log    ${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBR}
    Set Suite Variable    ${WcaOMetodologiaOkreslaniaWydatkowNaDzialalnoscBR}
    ${WcaOIloscPatenty}    Sprawdz Checkbox I Zaznacz    ${WcaOIloscPatenty XPath}
    Log    ${WcaOIloscPatenty}
    Set Suite Variable    ${WcaOIloscPatenty}
    ${WcaOIloscPrawoOchronneNaWzorPrzemyslowy}    Sprawdz Checkbox I Zaznacz    ${WcaOIloscPrawoOchronneNaWzorPrzemyslowy XPath}
    Log    ${WcaOIloscPrawoOchronneNaWzorPrzemyslowy}
    Set Suite Variable    ${WcaOIloscPrawoOchronneNaWzorPrzemyslowy}
    ${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowego}    Sprawdz Checkbox I Zaznacz    ${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowego XPath}
    Log    ${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowego}
    Set Suite Variable    ${WcaOIloscPrawoZRejestracjiWzoruPrzemyslowego}
    ${WcaORodzajOsrodkaInnowacji}    Sprawdz Checkbox I Zaznacz    ${WcaORodzajOsrodkaInnowacji XPath}
    Log    ${WcaORodzajOsrodkaInnowacji}
    Set Suite Variable    ${WcaORodzajOsrodkaInnowacji}
    ${WcaOSiedziba}    Sprawdz Checkbox I Zaznacz    ${WcaOSiedziba XPath}
    Log    ${WcaOSiedziba}
    Set Suite Variable    ${WcaOSiedziba}
    ${WcaOAdresKrajPl}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresKrajPl XPath}
    Log    ${WcaOAdresKrajPl}
    Set Suite Variable    ${WcaOAdresKrajPl}
    ${WcaOAdresKrajSf2}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresKrajSf2 XPath}
    Log    ${WcaOAdresKrajSf2}
    Set Suite Variable    ${WcaOAdresKrajSf2}
    ${WcaOAdresKodPocztowy}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresKodPocztowy XPath}
    Log    ${WcaOAdresKodPocztowy}
    Set Suite Variable    ${WcaOAdresKodPocztowy}
    ${WcaOAdresPoczta}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresPoczta XPath}
    Log    ${WcaOAdresPoczta}
    Set Suite Variable    ${WcaOAdresPoczta}
    ${WcaOAdresMiejscowosc}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresMiejscowosc XPath}
    Log    ${WcaOAdresMiejscowosc}
    Set Suite Variable    ${WcaOAdresMiejscowosc}
    ${WcaOAdresMiejscowoscMirId}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresMiejscowoscMirId XPath}
    Log    ${WcaOAdresMiejscowoscMirId}
    Set Suite Variable    ${WcaOAdresMiejscowoscMirId}
    ${WcaOAdresMiejscowoscZagranica}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresMiejscowoscZagranica XPath}
    Log    ${WcaOAdresMiejscowoscZagranica}
    Set Suite Variable    ${WcaOAdresMiejscowoscZagranica}
    ${WcaOAdresUlica}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresUlica XPath}
    Log    ${WcaOAdresUlica}
    Set Suite Variable    ${WcaOAdresUlica}
    ${WcaOAdresUlicaMirId}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresUlicaMirId XPath}
    Log    ${WcaOAdresUlicaMirId}
    Set Suite Variable    ${WcaOAdresUlicaMirId}
    ${WcaOAdresNrBudynku}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresNrBudynku XPath}
    Log    ${WcaOAdresNrBudynku}
    Set Suite Variable    ${WcaOAdresNrBudynku}
    ${WcaOAdresNrLokalu}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresNrLokalu XPath}
    Log    ${WcaOAdresNrLokalu}
    Set Suite Variable    ${WcaOAdresNrLokalu}
    ${WcaOAdresUlicaZagranica}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresUlicaZagranica XPath}
    Log    ${WcaOAdresUlicaZagranica}
    Set Suite Variable    ${WcaOAdresUlicaZagranica}
    ${WcaOAdresTelefon}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresTelefon XPath}
    Log    ${WcaOAdresTelefon}
    Set Suite Variable    ${WcaOAdresTelefon}
    ${WcaOAdresFaks}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresFaks XPath}
    Log    ${WcaOAdresFaks}
    Set Suite Variable    ${WcaOAdresFaks}
    ${WcaOAdresEmail}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresEmail XPath}
    Log    ${WcaOAdresEmail}
    Set Suite Variable    ${WcaOAdresEmail}
    ${WcaOAdresStronaWww}    Sprawdz Checkbox I Zaznacz    ${WcaOAdresStronaWww XPath}
    Log    ${WcaOAdresStronaWww}
    Set Suite Variable    ${WcaOAdresStronaWww}
    ${WcaOHistoriaWnioskodawcy}    Sprawdz Checkbox I Zaznacz    ${WcaOHistoriaWnioskodawcy XPath}
    Log    ${WcaOHistoriaWnioskodawcy}
    Set Suite Variable    ${WcaOHistoriaWnioskodawcy}
    ${WcaOMiejsceNaRynku}    Sprawdz Checkbox I Zaznacz    ${WcaOMiejsceNaRynku XPath}
    Log    ${WcaOMiejsceNaRynku}
    Set Suite Variable    ${WcaOMiejsceNaRynku}
    ${WcaOCharakterystykaRynku}    Sprawdz Checkbox I Zaznacz    ${WcaOCharakterystykaRynku XPath}
    Log    ${WcaOCharakterystykaRynku}
    Set Suite Variable    ${WcaOCharakterystykaRynku}
    ${WcaOOczekiwaniaPotrzebyKlientow}    Sprawdz Checkbox I Zaznacz    ${WcaOOczekiwaniaPotrzebyKlientow XPath}
    Log    ${WcaOOczekiwaniaPotrzebyKlientow}
    Set Suite Variable    ${WcaOOczekiwaniaPotrzebyKlientow}
    ${WcaOCharakterPopytu}    Sprawdz Checkbox I Zaznacz    ${WcaOCharakterPopytu XPath}
    Log    ${WcaOCharakterPopytu}
    Set Suite Variable    ${WcaOCharakterPopytu}
    ${WcaOWielkoscZatrudnienia}    Sprawdz Checkbox I Zaznacz    ${WcaOWielkoscZatrudnienia XPath}
    Log    ${WcaOWielkoscZatrudnienia}
    Set Suite Variable    ${WcaOWielkoscZatrudnienia}
    ${WcaOPrzychodyZeSprzedazyOstatniRok}    Sprawdz Checkbox I Zaznacz    ${WcaOPrzychodyZeSprzedazyOstatniRok XPath}
    Log    ${WcaOPrzychodyZeSprzedazyOstatniRok}
    Set Suite Variable    ${WcaOPrzychodyZeSprzedazyOstatniRok}
    ${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagraniczna}    Sprawdz Checkbox I Zaznacz    ${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagraniczna XPath}
    Log    ${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagraniczna}
    Set Suite Variable    ${WcaOPrzychodyZeSprzedazyOstatniRokSprzedazZagraniczna}
    ${WcaOProcentZeSprzedazyOstatniRokSprzedazZagraniczna}    Sprawdz Checkbox I Zaznacz    ${WcaOProcentZeSprzedazyOstatniRokSprzedazZagraniczna XPath}
    Log    ${WcaOProcentZeSprzedazyOstatniRokSprzedazZagraniczna}
    Set Suite Variable    ${WcaOProcentZeSprzedazyOstatniRokSprzedazZagraniczna}
    ${WcaOPrzychodyZeSprzedazyPrzedostatniRok}    Sprawdz Checkbox I Zaznacz    ${WcaOPrzychodyZeSprzedazyPrzedostatniRok XPath}
    Log    ${WcaOPrzychodyZeSprzedazyPrzedostatniRok}
    Set Suite Variable    ${WcaOPrzychodyZeSprzedazyPrzedostatniRok}
    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok}    Sprawdz Checkbox I Zaznacz    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok XPath}
    Log    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok}
    Set Suite Variable    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok}
    ${WcaOOpisProwadzonejDzialalnosci}    Sprawdz Checkbox I Zaznacz    ${WcaOOpisProwadzonejDzialalnosci XPath}
    Log    ${WcaOOpisProwadzonejDzialalnosci}
    Set Suite Variable    ${WcaOOpisProwadzonejDzialalnosci}
    ${WcaOOfertaWnioskodawcy}    Sprawdz Checkbox I Zaznacz    ${WcaOOfertaWnioskodawcy XPath}
    Log    ${WcaOOfertaWnioskodawcy}
    Set Suite Variable    ${WcaOOfertaWnioskodawcy}
    ${WcaOOczekiwaniaIPotrzebyOdbiorcow}    Sprawdz Checkbox I Zaznacz    ${WcaOOczekiwaniaIPotrzebyOdbiorcow XPath}
    Log    ${WcaOOczekiwaniaIPotrzebyOdbiorcow}
    Set Suite Variable    ${WcaOOczekiwaniaIPotrzebyOdbiorcow}
    ${WcaODoswiadczenieWeWzornictwie}    Sprawdz Checkbox I Zaznacz    ${WcaODoswiadczenieWeWzornictwie XPath}
    Log    ${WcaODoswiadczenieWeWzornictwie}
    Set Suite Variable    ${WcaODoswiadczenieWeWzornictwie}
    ${WcaOWspolnicy}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicy XPath}
    Log    ${WcaOWspolnicy}
    Set Suite Variable    ${WcaOWspolnicy}
    ${WcaOWspolnicyUlica}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyUlica XPath}
    Log    ${WcaOWspolnicyUlica}
    Set Suite Variable    ${WcaOWspolnicyUlica}
    ${WcaOWspolnicyNrBudynku}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyNrBudynku XPath}
    Log    ${WcaOWspolnicyNrBudynku}
    Set Suite Variable    ${WcaOWspolnicyNrBudynku}
    ${WcaOWspolnicyNrLokalu}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyNrLokalu XPath}
    Log    ${WcaOWspolnicyNrLokalu}
    Set Suite Variable    ${WcaOWspolnicyNrLokalu}
    ${WcaOWspolnicyKodPocztowy}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyKodPocztowy XPath}
    Log    ${WcaOWspolnicyKodPocztowy}
    Set Suite Variable    ${WcaOWspolnicyKodPocztowy}
    ${WcaOWspolnicyPoczta}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyPoczta XPath}
    Log    ${WcaOWspolnicyPoczta}
    Set Suite Variable    ${WcaOWspolnicyPoczta}
    ${WcaOWspolnicyMiejscowosc}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyMiejscowosc XPath}
    Log    ${WcaOWspolnicyMiejscowosc}
    Set Suite Variable    ${WcaOWspolnicyMiejscowosc}
    ${WcaOWspolnicyTelefon}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyTelefon XPath}
    Log    ${WcaOWspolnicyTelefon}
    Set Suite Variable    ${WcaOWspolnicyTelefon}
    ${WcaOWspolnicyFaks}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyFaks XPath}
    Log    ${WcaOWspolnicyFaks}
    Set Suite Variable    ${WcaOWspolnicyFaks}
    ${WcaOWspolnicyEmail}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyEmail XPath}
    Log    ${WcaOWspolnicyEmail}
    Set Suite Variable    ${WcaOWspolnicyEmail}
    ${WcaOWspolnicyStronaWww}    Sprawdz Checkbox I Zaznacz    ${WcaOWspolnicyStronaWww XPath}
    Log    ${WcaOWspolnicyStronaWww}
    Set Suite Variable    ${WcaOWspolnicyStronaWww}
    ${WcaOProduktyPodlegajaceInternacjonalizacji}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacji XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacji}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacji}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiNazwa}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiNazwa XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiNazwa}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiNazwa}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnCzyDotyczy}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnCzyDotyczy XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnCzyDotyczy}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCnCzyDotyczy}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCn}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCn XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCn}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodCn}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiuCzyDotyczy}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiuCzyDotyczy XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiuCzyDotyczy}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiuCzyDotyczy}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiu}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiu XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiu}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiKodPkwiu}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiInformacje}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiInformacje XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiInformacje}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiInformacje}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynki}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynki XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynki}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynki}
    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiDocelowe}    Sprawdz Checkbox I Zaznacz    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiDocelowe XPath}
    Log    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiDocelowe}
    Set Suite Variable    ${WcaOProduktyPodlegajaceInternacjonalizacjiRynkiDocelowe}
    ${WcaORynkiZagraniczne}    Sprawdz Checkbox I Zaznacz    ${WcaORynkiZagraniczne XPath}
    Log    ${WcaORynkiZagraniczne}
    Set Suite Variable    ${WcaORynkiZagraniczne}
    ${WcaOCzyDotyczyRynkiZagraniczne}    Sprawdz Checkbox I Zaznacz    ${WcaOCzyDotyczyRynkiZagraniczne XPath}
    Log    ${WcaOCzyDotyczyRynkiZagraniczne}
    Set Suite Variable    ${WcaOCzyDotyczyRynkiZagraniczne}
    Wszystkie Checkboxy Sa Zaznaczone    ${ModalCheckbox XPath}    # Sprawdza, czy żaden checkbox w konfiguracji nie został pominięty
    Zamknij Edycje Elementu

Test Wnioskodawca Ogolne
    [Documentation]    Krok 1: Strona powinna zawierać zdefiniowaną nazwę elementu
    ...    Krok 2: Sprawdź, czy są wszystkie pola
    ...    Krok 3: Sprawdź, na pustych polach
    ...    Krok 4: Sprawdź, na wypełnionych polach tekstowych
    ...    Krok 5: Sprawdź, na wypełnionych polach data
    ...    Krok 6: Sprawdź, na wypełnionych polach słowa kluczowe
    Page Should Contain    ${WcaOLabel}
    Test Wnioskodawca Ogolne Pola
    Test Wnioskodawca Ogolne Puste
    Test Wnioskodawca Ogolne Adres

Test Wnioskodawca Ogolne Pola
    [Documentation]    *Uwaga! To słowo kluczowe nie obejmuje wszystkich pól konfigurowanych w tym elemencie.* Jeżeli pole jest skonfigurowane, to sprawdza, czy odpowiednia etykieta jest na stronie
    Run Keyword If    ${WcaONazwa}    Page Should Contain    ${WcaONazwaLabel}
    Run Keyword If    ${WcaOStatus}    Page Should Contain    ${WcaOStatusLabel}
    Run Keyword If    ${WcaODataRozpoczeciaDzialalnosci}    Page Should Contain    ${WcaODataRozpoczeciaDzialalnosciLabel}
    Run Keyword If    ${WcaOFormaPrawna}    Page Should Contain    ${WcaOFormaPrawnaLabel}
    Run Keyword If    ${WcaOFormaWlasnosci}    Page Should Contain    ${WcaOFormaWlasnosciLabel}
    Run Keyword If    ${WcaONip}    Page Should Contain    ${WcaONipLabel}
    Run Keyword If    ${WcaORegon}    Page Should Contain    ${WcaORegonLabel}
    Run Keyword If    ${WcaOKrs}    Page Should Contain    ${WcaOKrsLabel}
    Run Keyword If    ${WcaOPesel}    Page Should Contain    ${WcaOPeselLabel}
    Run Keyword If    ${WcaOPkd}    Page Should Contain    ${WcaOPkdLabel}
    Run Keyword If    ${WcaOMozliwoscOdzyskaniaVat}    Page Should Contain    ${WcaOMozliwoscOdzyskaniaVatLabel}
    Run Keyword If    ${WcaOUzasadnienieBrakuOdzyskaniaVAT}    Page Should Contain    ${WcaOUzasadnienieBrakuOdzyskaniaVATLabel}
    Run Keyword If    ${WcaOSiedziba}    Page Should Contain    ${WcaOSiedzibaLabel}
    Run Keyword If    ${WcaOAdresKrajPl}    Page Should Contain    ${WcaOAdresKrajPlLabel}
    Run Keyword If    ${WcaOAdresKodPocztowy}    Page Should Contain    ${WcaOAdresKodPocztowyLabel}
    Run Keyword If    ${WcaOAdresPoczta}    Page Should Contain    ${WcaOAdresPocztaLabel}
    Run Keyword If    ${WcaOAdresMiejscowoscMirId}    Page Should Contain    ${WcaOAdresMiejscowoscMirIdLabel}
    Run Keyword If    ${WcaOAdresUlicaMirId}    Page Should Contain    ${WcaOAdresUlicaMirIdLabel}
    Run Keyword If    ${WcaOAdresNrBudynku}    Page Should Contain    ${WcaOAdresNrBudynkuLabel}
    Run Keyword If    ${WcaOAdresNrLokalu}    Page Should Contain    ${WcaOAdresNrLokaluLabel}
    Run Keyword If    ${WcaOAdresTelefon}    Page Should Contain    ${WcaOAdresTelefonLabel}
    Run Keyword If    ${WcaOAdresFaks}    Page Should Contain    ${WcaOAdresFaksLabel}
    Run Keyword If    ${WcaOAdresEmail}    Page Should Contain    ${WcaOAdresEmailLabel}
    Run Keyword If    ${WcaOAdresStronaWww}    Page Should Contain    ${WcaOAdresStronaWwwLabel}
    Run Keyword If    ${WcaOPrzychodyZeSprzedazyOstatniRok}    Page Should Contain    ${WcaOPrzychodyZeSprzedazyOstatniRokLabel}
    Run Keyword If    ${WcaOPrzychodyZeSprzedazyPrzedostatniRok}    Page Should Contain    ${WcaOPrzychodyZeSprzedazyPrzedostatniRokLabel}
    Run Keyword If    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRok}    Page Should Contain    ${WcaOPrzychodyZeSprzedazyPoprzedazjacyPrzedostatniRokLabel}

Test Wnioskodawca Ogolne Puste
    [Documentation]    Krok 1: Wprowadź puste ciągi znaków do pól
    ...    Krok 2: Zapisz i przeładuj
    ...    Krok 3: Sprawdź, czy pola są puste
    ...    Krok 4: Otwórz okno walidacji
    ...    Krok 5: Sprawdź, czy wyświetlają się komunikaty o konieczności uzupełnienia pola
    ...    Krok 6: Zamknij okno walidacji
    Test Wnioskodawca Ogolne Puste Wprowadz
    Zapisz Dane I Przeladuj
    Test Wnioskodawca Ogolne Puste Sprawdz
    Otworz Okno Walidacji
    Test Wnioskodawca Ogolne Puste Walidacja
    Zamknij Okno Walidacji

Test Wnioskodawca Ogolne Puste Wprowadz
    [Documentation]    Jeżeli pole jest skonfigurowane, to wprowadź do niego pusty ciąg znaków
    Run Keyword If    ${WcaOAdresKrajPl}    Select From List By Value    ${WcaOAdresKrajPl Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresNrBudynku}    Input text    ${WcaOAdresNrBudynku Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresNrLokalu}    Input text    ${WcaOAdresNrLokalu Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresKodPocztowy}    Input text    ${WcaOAdresKodPocztowy Id}    a    # Żeby usunąć wcześniej wpisane wartości - nie działa ${EMPTY}
    Run Keyword If    ${WcaOAdresPoczta}    Input text    ${WcaOAdresPoczta Id}    ${EMPTY}

Test Wnioskodawca Ogolne Puste Sprawdz
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdź, czy zawiera pusty ciąg znaków
    Run Keyword If    ${WcaOAdresKrajPl}    Sprawdz Czy Wartosc Listy Jest Rowna    ${WcaOAdresKrajPl Id}    ${KrajPolskaLabelPusty}
    Run Keyword If    ${WcaOAdresNrBudynku}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresNrBudynku Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresNrLokalu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresNrLokalu Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresKodPocztowy}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresKodPocztowy Id}    ${EMPTY}
    Run Keyword If    ${WcaOAdresPoczta}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresPoczta Id}    ${EMPTY}

Test Wnioskodawca Ogolne Puste Walidacja
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdza, czy walidacja wyświetla komunikat o konieczności wypełnienia pola.*Dziwne, ale na pole Adres siedziby wnioskodawcy - Poczta nie ma walidacji
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresKrajPlLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresWojewodztwoLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresPowiatLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresGminaLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${WcaOAdresNrBudynku}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresNrBudynkuLabel}    ${WalidacjaPoleObowiazkowe}    1
    Run Keyword If    ${WcaOAdresKodPocztowy}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresKodPocztowyLabel}    ${WalidacjaPoleObowiazkowe}    1
    Comment    Run Keyword If    ${WcaOAdresPoczta}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresPocztaLabel}    ${WalidacjaPoleObowiazkowe}
    ...    1

Test Wnioskodawca Ogolne Adres
    [Documentation]    Krok 1-7: Wprowadź dane do pól bloku adres
    ...    Krok 8: Zapisz i przeładuj
    Run Keyword If    ${WcaOAdresKrajPl}    Test Wnioskodawca Ogolne Adres Wprowadz Kraj Gmina
    Run Keyword If    ${WcaOAdresMiejscowoscMirId}    Test Wnioskodawca Ogolne Adres Wprowadz Miejscowosc
    Run Keyword If    ${WcaOAdresUlicaMirId}    Test Wnioskodawca Ogolne Adres Wprowadz Ulica
    Run Keyword If    ${WcaOAdresNrBudynku}    Test Wnioskodawca Ogolne Adres Wprowadz Nr Budynku
    Run Keyword If    ${WcaOAdresNrLokalu}    Test Wnioskodawca Ogolne Adres Wprowadz Nr Lokalu
    Run Keyword If    ${WcaOAdresKodPocztowy}    Test Wnioskodawca Ogolne Adres Wprowadz Kod Pocztowy
    Run Keyword If    ${WcaOAdresPoczta}    Test Wnioskodawca Ogolne Adres Wprowadz Poczta
    Zapisz Dane I Przeladuj
    Test Wnioskodawca Ogolne Adres Sprawdz
    Otworz Okno Walidacji
    Test Wnioskodawca Ogolne Adres Walidacja
    Zamknij Okno Walidacji

Test Wnioskodawca Ogolne Adres Wprowadz Kraj Gmina
    [Documentation]    Krok 1: Wybierz Polskę z listy krajów
    ...    Krok 2: Wylosuj województwo z listy
    ...    Krok 3: Zbuduj zapytanie dla listy powiatów dla wylosowanego województwa
    ...    Krok 4: Wylosuj powiat z listy
    ...    Krok 5: Zbuduj zapytanie dla listy gmin dla wylosowanego województwa
    ...    Krok 6: Zbuduj zapytanie dla listy gmin dla wylosowanego powiatu
    ...    Krok 7: Wylosuj gminę z listy
    ...    Krok 8: Ustaw zmienną z wylosowanym województwem dla całego pakietu
    ...    Krok 9: Ustaw zmienną z wylosowanym powiatem dla całego pakietu
    ...    Krok 10: Ustaw zmienną z wylosową gminą dla całego pakietu
    Select From List By Label    ${WcaOAdresKrajPl Id}    ${KrajPolskaLabel}
    ${WcaOAdresWojewodztwo}    Adres Wprowadz Z Listy    ${WcaOAdresWojewodztwo Id}    ${ZapytanieWojewodztwo}
    ${ZapytaniePowiat}    Replace String    ${ZapytaniePowiat}    {Wojewodztwo}    ${WcaOAdresWojewodztwo}
    ${WcaOAdresPowiat}    Adres Wprowadz Z Listy    ${WcaOAdresPowiat Id}    ${ZapytaniePowiat}
    ${ZapytanieGmina}    Replace String    ${ZapytanieGmina}    {Wojewodztwo}    ${WcaOAdresWojewodztwo}
    ${ZapytanieGmina}    Replace String    ${ZapytanieGmina}    {Powiat}    ${WcaOAdresPowiat}
    ${WcaOAdresGmina}    Adres Wprowadz Z Listy    ${WcaOAdresGmina Id}    ${ZapytanieGmina}
    Set Suite Variable    ${WcaOAdresWojewodztwo}
    Set Suite Variable    ${WcaOAdresPowiat}
    Set Suite Variable    ${WcaOAdresGmina}

Test Wnioskodawca Ogolne Adres Wprowadz Miejscowosc
    [Documentation]    Krok 1: Wyświetl błąd, jeżeli w konfiguratorze wniosku nie zaznaczono Adres siedziby Kraj - Polska
    ...    Krok 2: Zbuduj zapytanie dla listy powiatów dla wylosowanego województwa
    ...    Krok 3: Zbuduj zapytanie dla listy powiatów dla wylosowanego powiatu
    ...    Krok 4: Zbuduj zapytanie dla listy powiatów dla wylosowanej gminy
    ...    Krok 5: Wylosuj miejscowość z listy
    ...    Krok 6: Ustaw zmienną z wylosową miejscowością dla całego pakietu
    Run Keyword If    ${WcaOAdresKrajPl}==${FALSE}    Fail    Dla elementu Adres siedziby Miejscowość - lista rozwijalna musi zostać skonfigurowany element Adres siedziby Kraj - Polska
    ${ZapytanieMiejscowosc}    Replace String    ${ZapytanieMiejscowosc}    {Wojewodztwo}    ${WcaOAdresWojewodztwo}
    ${ZapytanieMiejscowosc}    Replace String    ${ZapytanieMiejscowosc}    {Powiat}    ${WcaOAdresPowiat}
    ${ZapytanieMiejscowosc}    Replace String    ${ZapytanieMiejscowosc}    {Gmina}    ${WcaOAdresGmina}
    ${WcaOAdresMiejscowosc}    Adres Wprowadz Z Listy    ${WcaOAdresMiejscowoscMirId Id}    ${ZapytanieMiejscowosc}
    Set Suite Variable    ${WcaOAdresMiejscowosc}

Test Wnioskodawca Ogolne Adres Wprowadz Ulica
    [Documentation]    Krok 1: Wyświetl błąd, jeżeli w konfiguratorze wniosku nie zaznaczono Adres siedziby Kraj - Polska
    ...    Krok 2: Wyświetl błąd, jeżeli w konfiguratorze wniosku nie zaznaczono Adres siedziby Miejscowość - lista rozwijalna
    ...    Krok 3: Zbuduj zapytanie dla listy powiatów dla wylosowanego województwa
    ...    Krok 4: Zbuduj zapytanie dla listy powiatów dla wylosowanego powiatu
    ...    Krok 5: Zbuduj zapytanie dla listy powiatów dla wylosowanej gminy
    ...    Krok 6: Wylosuj miejscowość z listy
    ...    Krok 7: Ustaw zmienną z wylosową miejscowością dla całego pakietu
    Run Keyword If    ${WcaOAdresKrajPl}==${FALSE}    Fail    Dla elementu Adres siedziby Ulica - lista rozwijalna musi zostać skonfigurowany element Adres siedziby Kraj - Polska
    Run Keyword If    ${WcaOAdresMiejscowoscMirId}==${FALSE}    Fail    Dla elementu Adres siedziby Ulica - lista rozwijalna musi zostać skonfigurowany element Adres siedziby Miejscowość - lista rozwijalna
    ${ZapytanieUlica}    Replace String    ${ZapytanieUlica}    {Wojewodztwo}    ${WcaOAdresWojewodztwo}
    ${ZapytanieUlica}    Replace String    ${ZapytanieUlica}    {Powiat}    ${WcaOAdresPowiat}
    ${ZapytanieUlica}    Replace String    ${ZapytanieUlica}    {Gmina}    ${WcaOAdresGmina}
    ${ZapytanieUlica}    Replace String    ${ZapytanieUlica}    {Miejscowosc}    ${WcaOAdresMiejscowosc}
    ${WcaOAdresUlica}    Adres Wprowadz Z Listy    ${WcaOAdresUlicaMirId Id}    ${ZapytanieUlica}
    Set Suite Variable    ${WcaOAdresUlica}

Test Wnioskodawca Ogolne Adres Wprowadz Nr Budynku
    [Documentation]    Wprowadź ciąg znaków do pola
    ${WcaOAdresNrBudynkuTresc}    Wypelnij Pole Losowym Tekstem    ${WcaOAdresNrBudynku Id}    ${WcaOAdresNrBudynkuLen}
    Set Suite Variable    ${WcaOAdresNrBudynkuTresc}

Test Wnioskodawca Ogolne Adres Wprowadz Nr Lokalu
    [Documentation]    Wprowadź ciąg znaków do pola
    ${WcaOAdresNrLokaluTresc}    Wypelnij Pole Losowym Tekstem    ${WcaOAdresNrLokalu Id}    ${WcaOAdresNrLokaluLen}
    Set Suite Variable    ${WcaOAdresNrLokaluTresc}

Test Wnioskodawca Ogolne Adres Wprowadz Kod Pocztowy
    [Documentation]    Krok 1: Sprawdź, czy pole pilnuje formatu kodu pocztowego
    ...    Krok 2: Wprowadź ciąg znaków do pola
    Pilnuj Formatu Kodu Pocztowego    ${WcaOAdresKodPocztowy Id}
    ${WcaOAdresKodPocztowyTresc}    Wprowadz Kod Pocztowy    ${WcaOAdresKodPocztowy Id}    ${WcaOAdresKodPocztowyLen}
    Set Suite Variable    ${WcaOAdresKodPocztowyTresc}

Test Wnioskodawca Ogolne Adres Wprowadz Poczta
    [Documentation]    Wprowadź ciąg znaków do pola
    ${WcaOAdresPocztaTresc}    Wypelnij Pole Losowym Tekstem    ${WcaOAdresPoczta Id}    ${WcaOAdresPocztaLen}
    Set Suite Variable    ${WcaOAdresPocztaTresc}

Test Wnioskodawca Ogolne Adres Sprawdz
    [Documentation]    Jeżeli pole jest skonfigurowane, to wprowadź do niego losowy ciąg znaków lub wybierz wartość z listy
    Run Keyword If    ${WcaOAdresKrajPl}    Sprawdz Czy Wartosc Listy Jest Rowna    ${WcaOAdresKrajPl Id}    ${KrajPolskaLabel}
    Run Keyword If    ${WcaOAdresKrajPl}    Sprawdz Czy Wartosc Select2 Jest Rowna    ${WcaOAdresWojewodztwo Id}    ${WcaOAdresWojewodztwo}
    Run Keyword If    ${WcaOAdresKrajPl}    Sprawdz Czy Wartosc Select2 Jest Rowna    ${WcaOAdresPowiat Id}    ${WcaOAdresPowiat}
    Run Keyword If    ${WcaOAdresKrajPl}    Sprawdz Czy Wartosc Select2 Jest Rowna    ${WcaOAdresGmina Id}    ${WcaOAdresGmina}
    Run Keyword If    ${WcaOAdresMiejscowoscMirId}    Sprawdz Czy Wartosc Select2 Jest Rowna    ${WcaOAdresMiejscowoscMirId Id}    ${WcaOAdresMiejscowosc}
    Run Keyword If    ${WcaOAdresUlicaMirId}    Sprawdz Czy Wartosc Select2 Jest Rowna    ${WcaOAdresUlicaMirId Id}    ${WcaOAdresUlica}
    Run Keyword If    ${WcaOAdresNrBudynku}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresNrBudynku Id}    ${WcaOAdresNrBudynkuTresc}
    Run Keyword If    ${WcaOAdresNrLokalu}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresNrLokalu Id}    ${WcaOAdresNrLokaluTresc}
    Run Keyword If    ${WcaOAdresKodPocztowy}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresKodPocztowy Id}    ${WcaOAdresKodPocztowyTresc}
    Run Keyword If    ${WcaOAdresPoczta}    Sprawdz Czy Wartosc Pola Jest Rowna    ${WcaOAdresPoczta Id}    ${WcaOAdresPocztaTresc}

Test Wnioskodawca Ogolne Adres Walidacja
    [Documentation]    Jeżeli pole jest skonfigurowane, to sprawdza, czy walidacja wyświetla komunikat o konieczności wypełnienia pola.*Dziwne, ale na pole Adres siedziby wnioskodawcy - Poczta nie ma walidacji
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresKrajPlLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresWojewodztwoLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresPowiatLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${WcaOAdresKrajPl}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresGminaLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${WcaOAdresNrBudynku}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresNrBudynkuLabel}    ${WalidacjaPoleObowiazkowe}    0
    Run Keyword If    ${WcaOAdresKodPocztowy}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresKodPocztowyLabel}    ${WalidacjaPoleObowiazkowe}    0
    Comment    Run Keyword If    ${WcaOAdresPoczta}    Walidacja Zawiera Label Z Prefixem I Komunikatem    ${WalidacjaPrefixAdresSiedzibyWnioskodawcy}    ${WcaOAdresPocztaLabel}    ${WalidacjaPoleObowiazkowe}
    ...    0

Dodaj czlonka realizujacego pomysl
    [Documentation]  Przycisk dodaj w sekcji Zespol realizujacy pomysl
    Set Focus To Element  ${WcaDodajCzlonkaRealPomysl}
    Click Button  ${WcaDodajCzlonkaRealPomysl}