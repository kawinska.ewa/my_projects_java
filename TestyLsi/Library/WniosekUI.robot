*** Variables ***
${MenuNawigacja Id}    id=navigator_menu    # Boczne menu z elementami wniosku (MN)
${MNOstatniLink XPath}    //ul[@id='navigator_menu']/li[last()]/a    # Ostatnia pozycja z bocznego menu
${MNPierwszyLink XPath}    //ul[@id='navigator_menu']/li[1]/a    # Pierwsza pozycja z bocznego menu
${TytulProjektu Id}    id=ogolne_projekt_tytulProjektu    # Pole Tytuł projektu
${ZapiszWniosek Id}    id=akcja_zapisz_wniosek    # Przycisk Zapisz wniosek
${SprawdzPoprawnosc Id}    //*[@id="akcja_waliduj_wniosek"]    # Przycisk Sprawdź poprawność
${ZlozWniosek Id}    id=akcja_zloz_wniosek    # Przycisk Złóż wniosek
${ElementListyWalidacja Id}    css=.jumpToError
${ModalWalidacja Id}    id=modal
${ModalWalidacjaLink XPath}    //*[@id='modal']//a    # Link w oknie walidacji
${ModalZlozWniosek Id}    id=modal
${ModalZlozWniosekH4 CSS}    css=.modal h4    # Styl nagłówka modala Złóż wniosek
${ModalZlozWniosekH4Tresc}    Czy na pewno chcesz złożyć ten wniosek?    # Treść nagłówka modala Złóż wniosek
${ModalZlozWniosekPrzycisk XPath}    //div[@id='modal']//a[contains(text(),'Złóż wniosek')]    # Przycisk Złóż wniosek
${AlertZlozono}    Wniosek został zarejestrowany    # Treść alertu po złożeniu wniosku
${AnkietaPoRejestracjiWrocPrzycisk XPath}    //form[@name='parp_ankietabundle_porejestracjiwniosku_ankieta']//a[contains(text(),'Powrót do listy wniosków')]    # Przycisk "Powrót do listy wniosków" na ankiecie po rejestracji wniosku
${AnkietaPoRejestracjiNaglowekTresc}    Wniosek został zarejestrowany    # Treść nagłówka ankiety po rejestracji
