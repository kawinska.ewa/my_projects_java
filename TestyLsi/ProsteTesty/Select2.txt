*** Settings ***
Documentation     *W związku z koniecznością ukrycia hasła uzytkownika PARP, pakiet należy uruchamiać z prarametrami --outputdir TestResult --timestamp --removekeywords name:Resource.Zaloguj --removekeywords name:Resource.PobierzDaneDoLogowania*
Test Teardown     Close Browser
Library           Selenium2Library
Library           Dialogs
Library           String
Library           DateTime
Resource          ../Library/LogowanieUI.txt    # Logowanie elementy interface
Resource          ../Library/Dane_do_logowania.txt    # Dane do poprawnego logowania
Resource          ../Library/Resource.txt    # User keywords
Resource          ../Library/AdministracjaUI.txt
Resource          ../Library/WersjeElementowEdutujUI.txt
Resource          ../Library/ElementyWniosku.txt
Resource          ../Library/ElementyWnioskuUI.txt

*** Test Cases ***
Select2 w Adres Wnioskodawcy
    [Documentation]    Krok 1-7: Definicja zmiennych, które przy testach ElementyWniosku są definiowane przez Robot
    ...    Krok 8: Otwórz przeglądarkę na stronie logowania
    ...    Krok 9: Zaloguj z podaną Nazwą użytkownika i Hasłem
    ...    Krok 10: Sprawdź czy logowanie się powiodło
    ...    Krok 11: Wejdź na stronę edycji wniosku o wskazanym id
    ...    Krok 12: Testuj adres wnioskodawcy
    Set Test Variable    ${WcaOAdresKrajPl}    ${TRUE}
    Set Test Variable    ${WcaOAdresMiejscowoscMirId}    ${TRUE}
    Set Test Variable    ${WcaOAdresUlicaMirId}    ${TRUE}
    Set Test Variable    ${WcaOAdresNrBudynku}    ${TRUE}
    Set Test Variable    ${WcaOAdresNrLokalu}    ${TRUE}
    Set Test Variable    ${WcaOAdresKodPocztowy}    ${TRUE}
    Set Test Variable    ${WcaOAdresPoczta}    ${TRUE}
    Otworz Przegladarke Na Stronie Logowania
    Zaloguj    ${Login}    ${Password}
    Logowanie Powiodlo Sie
    ${IdWniosku}    Get Value From User    Podaj Id wniosku, na którym chcesz przeprowadzić testy    3593
    Edytuj Wniosek    ${IdWniosku}
    Test Wnioskodawca Ogolne Adres Wprowadz Kraj Gmina
    Test Wnioskodawca Ogolne Adres Wprowadz Miejscowosc
    Test Wnioskodawca Ogolne Adres Wprowadz Ulica
    Test Wnioskodawca Ogolne Adres Wprowadz Nr Budynku
    Test Wnioskodawca Ogolne Adres Wprowadz Nr Lokalu
    Test Wnioskodawca Ogolne Adres Wprowadz Kod Pocztowy
    Test Wnioskodawca Ogolne Adres Wprowadz Poczta
    Zapisz Dane I Przeladuj
    Test Wnioskodawca Ogolne Adres Sprawdz
    Otworz Okno Walidacji
    Test Wnioskodawca Ogolne Adres Walidacja
    Zamknij Okno Walidacji
