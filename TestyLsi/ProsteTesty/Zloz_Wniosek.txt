*** Settings ***
Test Teardown     Close Browser
Library           Selenium2Library    #Test Teardown    Close Browser
Library           String
Library           DatabaseLibrary
Resource          ../Library/LogowanieUI.txt    # Logowanie elementy interface
Resource          ../Library/Dane_do_logowania.txt    # Dane do poprawnego logowania
Resource          ../Library/Resource.txt    # User keywords
Resource          ../Library/TrwajaceNaboryUI.txt

*** Test Cases ***
Zloz Wniosek
    [Documentation]    Krok 0: Pobierz zmienną ${IdWnioskuDoZlozenia}
    ...    Krok 1: Sprawdź czy zdefiniowano zmienną ${IdWnioskuDoZlozenia}
    ...    Krok 2: Jeżeli nie zdefiniowano zmiennej ${IdWnioskuDoZlozenia}, to utwórz zmienną @{IdWnioskowDoZlozenia} korzystając z Wybierz Zlozony Wniosek Z Otwartego Naboru, w przeciwnym razie utwórz nową listę @{IdWnioskowDoZlozenia} ze zmiennej ${IdWnioskuDoZlozenia}
    ...    Krok 3: Ustaw zmienną dla testu @{BlendyPrzySkladaniuWniosku} o wartości empty
    ...    Krok 4: Otwórz przeglądarkę na stronie logowania
    ...    Krok 5: Zaloguj z podaną Nazwą użytkownika i Hasłem
    ...    Krok 6: Sprawdź czy logowanie się powiodło
    ...    Krok 8: Usuń informacje o złożeniu wniosku o wskazanym Id
    ...    Krok 9: Wejdź na stronę edycji wniosku o wskazanym id
    ...    Krok 10: Uruchom keyword Złóż wniosek i zwróć informację, czy nie było błedów
    ...    Krok 11: Jeżeli w poprzednim kroku nie było błędów to sprawdź, czy wniosek został złożony, w przeciwnym wypadku wykonaj zrób screenshot walidacji
    ...    Krok 12: Jeżeli przy składaniu wniosków wystąpiły błędy (lista @{BlendyPrzySkladaniuWniosku} nie jest pusta), to wywołaj błąd
    ${IdWnioskuDoZlozenia}    Get Value From User    Podaj Id wniosku, na którym chcesz przeprowadzić testy złożenia wniosku    2351
    ${CzyZdefiniowanoWniosek}    Run Keyword And Return Status    Variable Should Exist    ${IdWnioskuDoZlozenia}
    @{IdWnioskowDoZlozenia}    Run Keyword If    '${CzyZdefiniowanoWniosek}'=='${FALSE}'    Wybierz Zlozony Wniosek Z Otwartego Naboru
    ...    ELSE    Create List    ${IdWnioskuDoZlozenia}
    Set Test Variable    @{BlendyPrzySkladaniuWniosku}    @{EMPTY}
    Otworz Przegladarke Na Stronie Logowania
    Zaloguj    ${Login}    ${Password}
    Logowanie Powiodlo Sie
    : FOR    ${IdWnioskuDoZlozenia}    IN    @{IdWnioskowDoZlozenia}
    \    Usun Informacje O Zlozeniu Wniosku    ${IdWnioskuDoZlozenia}
    \    Edytuj Wniosek    ${IdWnioskuDoZlozenia}
    \    Comment    ${BrakBledowPrzySkladaniu}    Run Keyword And Return Status    Zloz Wniosek    ${IdWnioskuDoZlozenia}    # pierwotna kolejnosc
    \    ${BrakBledowPrzySkladaniu}    Run Keyword And Return Status    Zloz Wniosek    ${IdWnioskuDoZlozenia}
    \    Run Keyword If    ${BrakBledowPrzySkladaniu}    Sprawdz Czy Wniosek Zlozony    ${IdWnioskuDoZlozenia}
    \    ...    ELSE    Blad Przy Skladaniu Wniosku    ${IdWnioskuDoZlozenia}
    Run Keyword If    @{BlendyPrzySkladaniuWniosku}!=@{EMPTY}    Fail    Część wniosków nie została prawidłowo złożona. Sprawdź screenshot-y z walidacją w 'FOR/IdWniosku/Run Keyword If/Blad Przy Skladaniu Wniosku' dla @{BlendyPrzySkladaniuWniosku}.
