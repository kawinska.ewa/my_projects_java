package app.moje;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class OdczytZplikuTekstowego {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String s;
		FileReader fr = new FileReader("dane.txt");
		Scanner plik = new Scanner(fr);
		// w tym wypadku petla sie wykonuje dopoki trafia na dowolna wartosc
		while(plik.hasNext()){
			s=plik.next();
			System.out.print(s + " ");
		}
	}

}
