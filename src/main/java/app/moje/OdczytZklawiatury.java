package app.moje;

import java.util.Scanner;

public class OdczytZklawiatury {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Deklarujemy nasz skaner, odczytujemy dane ze standardowego wejscia,
		// domyslnie jest to klawiatura
		Scanner sc = new Scanner(System.in);
		// Ponizej pobieramy zmienna z klawiatury,
		// program oczekuje w tym wypadku na wpisanie w konsoli wartosci typu
		// Integer
		int a = sc.nextInt();
		System.out.println(a);

		/*
		 * public static String getUserInput(){ return sc.nextLine(); }
		 * System.out.print("Podaj imie:"); String
		 * imieWczytaneOdUzytkownika=getUserInput();
		 */

	}

}
