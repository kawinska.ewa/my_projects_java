package app.main.definition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverInitialize {
 
	public static WebDriver driver;	

	//System.out.println(driver.type);
	
public static WebDriver browserChoose(){
	
	String przegladarka=System.getProperty("driver.type");
	switch(przegladarka) {
	case "Firefox":
		return new FirefoxDriver();
	
	case "Chrome":
		return new ChromeDriver();
		
	case "IE":
		return new InternetExplorerDriver();
		
	default:
			throw new IllegalStateException("nieznana przeglądarka");	
	}	
}	
	

public static WebDriver test_run(){
	driver=browserChoose();
	return driver;
}


	
/*	
	
	public void setUpFirefox() throws Exception{
		driver = new FirefoxDriver();
	}

	public void setUpChrome() throws Exception{
		driver = new ChromeDriver();
	}

	public void setUpIE() throws Exception{
		driver = new InternetExplorerDriver();
	}
	
	public void tearDown() throws Exception{
		driver.quit();
	}
	
*/

	public WebDriverInitialize (WebDriver driver){
		this.driver=driver;
	}
	
	//BrowserType browser;
	
	//public void setUpBrowsers(BrowserType browser){ //metoda wspolna dla wszystkich przegladarek
		//switch(browser.getBrowserName()){
		
	//}
	/*
	public static WebDriver webDriver(DesiredCapabilities desiredCapabilities) { 

		switch () {
		case BrowserType.FIREFOX:
		return new FirefoxDriver(desiredCapabilities);
		case BrowserType.CHROME:
		return new ChromeDriver(desiredCapabilities);
		case BrowserType.IE:
		return new InternetExplorerDriver(desiredCapabilities);
		default:
		throw new IllegalStateException
		("unknown browser " + desiredCapabilities.getBrowserName());
		}
		}
*/
	
}



