package javafortesters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Checking {
	
	@Test
	public void checkWebTitle(){
		
		FirefoxDriver driver = new FirefoxDriver();
		driver.get("http://www.google.pl");
		
		assertEquals("Tytul powinien pasowac","Google", driver.getTitle());
	}

}
