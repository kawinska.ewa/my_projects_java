-- zapytanie używane przez keyword "Wybierz Otwarte Nabory"
-- Wybiera z bazy i zwraca Id 'aktywnych' i 'testowalnych' naborów.

SELECT wk.id -- , pw.kod, wk.nabor
FROM wnioski.konkurs wk
	JOIN slowniki.poziom_wdrazania pw ON pw.id = wk.id_poziomu_wdrazania
WHERE 1=1
 	AND wk.data_czas_rozpoczecia < NOW() 
 	AND wk.data_czas_zakonczenia - INTERVAL '1 hour' >= NOW() 
 	AND pw.kod NOT LIKE 'POWR%' 
 	AND wk.id NOT IN (35, 53, 54, 65, 97, 132, 124) -- Uwaga! Nie uwzglednia naboru o id 35, 53, 54, 65 (POIR 2.3.4/POIR 2.3.1/staroć, bo tam trzeba wybrać rodzaj wniosku oraz naboru); 97 (zepsuty wniosek obejmujacy wszystkie pola w generatorze); 132 (PAMI, bo tam nie ma tytułu projektu wg którego prowadzony jest test zapisu); POPR.01.01.00 Pożyczka na rozwój, bo tam nie ma tytułu projektu wg którego prowadzony jest test zapisu)
-- 	AND wk.id IN (13, 133) -- pomocniczy atrybut testu do podmiany, aby wykonać test jedynie dla wniosków z naborów POPW.01.01.01 i POIR.02.04.01