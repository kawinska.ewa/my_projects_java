DELETE
FROM dziennik.historia_statusow
WHERE 1=1
	AND id >= (SELECT MIN(id) id FROM dziennik.historia_statusow
					WHERE wniosek_id = 2351 
					AND zdarzenie = 'Rozpoczęcie rejestracji') 
	AND wniosek_id = 2351;

UPDATE dziennik.historia_statusow 
SET status_id = 2
WHERE wniosek_id = 2351 AND status_id = 3 AND komunikat = 'Skopiowano status z oryginału';

UPDATE wnioski.wnioski 
SET numer_rejestracyjny_wniosku = NULL, numer_kolejny_wniosku = NULL
WHERE id = 2351;